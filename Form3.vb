Public Class Form3
    Inherits System.Windows.Forms.Form
    Dim intCount As Integer = 0

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboStopYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboStopDay As System.Windows.Forms.ComboBox
    Friend WithEvents cboStopMonth As System.Windows.Forms.ComboBox
    Friend WithEvents cboStartYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboStartDay As System.Windows.Forms.ComboBox
    Friend WithEvents cboStartMonth As System.Windows.Forms.ComboBox
    Friend WithEvents lblStop As System.Windows.Forms.Label
    Friend WithEvents lblStart As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button5 = New System.Windows.Forms.Button
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.cboStopYear = New System.Windows.Forms.ComboBox
        Me.cboStopDay = New System.Windows.Forms.ComboBox
        Me.cboStopMonth = New System.Windows.Forms.ComboBox
        Me.cboStartYear = New System.Windows.Forms.ComboBox
        Me.cboStartDay = New System.Windows.Forms.ComboBox
        Me.cboStartMonth = New System.Windows.Forms.ComboBox
        Me.lblStop = New System.Windows.Forms.Label
        Me.lblStart = New System.Windows.Forms.Label
        Me.btnSearch = New System.Windows.Forms.Button
        Me.DataGrid1 = New System.Windows.Forms.DataGrid
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(360, 72)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(120, 32)
        Me.Button5.TabIndex = 47
        Me.Button5.Text = "Update production date"
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(256, 32)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.TabIndex = 46
        Me.DateTimePicker1.Visible = False
        '
        'cboStopYear
        '
        Me.cboStopYear.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009"})
        Me.cboStopYear.Location = New System.Drawing.Point(144, 96)
        Me.cboStopYear.Name = "cboStopYear"
        Me.cboStopYear.Size = New System.Drawing.Size(64, 21)
        Me.cboStopYear.TabIndex = 35
        '
        'cboStopDay
        '
        Me.cboStopDay.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})
        Me.cboStopDay.Location = New System.Drawing.Point(88, 96)
        Me.cboStopDay.Name = "cboStopDay"
        Me.cboStopDay.Size = New System.Drawing.Size(40, 21)
        Me.cboStopDay.TabIndex = 34
        '
        'cboStopMonth
        '
        Me.cboStopMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cboStopMonth.Location = New System.Drawing.Point(16, 96)
        Me.cboStopMonth.Name = "cboStopMonth"
        Me.cboStopMonth.Size = New System.Drawing.Size(56, 21)
        Me.cboStopMonth.TabIndex = 33
        '
        'cboStartYear
        '
        Me.cboStartYear.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009"})
        Me.cboStartYear.Location = New System.Drawing.Point(144, 32)
        Me.cboStartYear.Name = "cboStartYear"
        Me.cboStartYear.Size = New System.Drawing.Size(64, 21)
        Me.cboStartYear.TabIndex = 32
        '
        'cboStartDay
        '
        Me.cboStartDay.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})
        Me.cboStartDay.Location = New System.Drawing.Point(88, 32)
        Me.cboStartDay.Name = "cboStartDay"
        Me.cboStartDay.Size = New System.Drawing.Size(40, 21)
        Me.cboStartDay.TabIndex = 31
        '
        'cboStartMonth
        '
        Me.cboStartMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cboStartMonth.Location = New System.Drawing.Point(16, 32)
        Me.cboStartMonth.Name = "cboStartMonth"
        Me.cboStartMonth.Size = New System.Drawing.Size(56, 21)
        Me.cboStartMonth.TabIndex = 30
        '
        'lblStop
        '
        Me.lblStop.Location = New System.Drawing.Point(24, 72)
        Me.lblStop.Name = "lblStop"
        Me.lblStop.TabIndex = 29
        Me.lblStop.Text = "Stop:"
        '
        'lblStart
        '
        Me.lblStart.Location = New System.Drawing.Point(24, 8)
        Me.lblStart.Name = "lblStart"
        Me.lblStart.TabIndex = 28
        Me.lblStart.Text = "Start:"
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(232, 96)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(104, 23)
        Me.btnSearch.TabIndex = 26
        Me.btnSearch.Text = "Search Records"
        '
        'DataGrid1
        '
        Me.DataGrid1.AlternatingBackColor = System.Drawing.Color.Lavender
        Me.DataGrid1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.DataGrid1.BackgroundColor = System.Drawing.Color.White
        Me.DataGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataGrid1.CaptionBackColor = System.Drawing.Color.Khaki
        Me.DataGrid1.CaptionForeColor = System.Drawing.Color.MidnightBlue
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.FlatMode = True
        Me.DataGrid1.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.DataGrid1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.DataGrid1.GridLineColor = System.Drawing.Color.Gainsboro
        Me.DataGrid1.GridLineStyle = System.Windows.Forms.DataGridLineStyle.None
        Me.DataGrid1.HeaderBackColor = System.Drawing.Color.MidnightBlue
        Me.DataGrid1.HeaderFont = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.DataGrid1.HeaderForeColor = System.Drawing.Color.WhiteSmoke
        Me.DataGrid1.LinkColor = System.Drawing.Color.Teal
        Me.DataGrid1.Location = New System.Drawing.Point(24, 136)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.ParentRowsBackColor = System.Drawing.Color.Gainsboro
        Me.DataGrid1.ParentRowsForeColor = System.Drawing.Color.MidnightBlue
        Me.DataGrid1.PreferredColumnWidth = 95
        Me.DataGrid1.ReadOnly = True
        Me.DataGrid1.RowHeaderWidth = 95
        Me.DataGrid1.SelectionBackColor = System.Drawing.Color.CadetBlue
        Me.DataGrid1.SelectionForeColor = System.Drawing.Color.WhiteSmoke
        Me.DataGrid1.Size = New System.Drawing.Size(980, 525)
        Me.DataGrid1.TabIndex = 48
        Me.DataGrid1.TabStop = False
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem10, Me.MenuItem8})
        Me.MenuItem1.Text = "Tools"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Route summary"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "Production summary"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 2
        Me.MenuItem4.Text = "Route details"
        '
        'MenuItem5
        '
        Me.MenuItem5.Enabled = False
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "Production details"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 4
        Me.MenuItem6.Text = "Weekly report"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 5
        Me.MenuItem7.Text = "Law tags"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 6
        Me.MenuItem10.Text = "-"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 7
        Me.MenuItem8.Text = "Exit application"
        '
        'Form3
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1048, 430)
        Me.Controls.Add(Me.DataGrid1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.cboStopYear)
        Me.Controls.Add(Me.cboStopDay)
        Me.Controls.Add(Me.cboStopMonth)
        Me.Controls.Add(Me.cboStartYear)
        Me.Controls.Add(Me.cboStartDay)
        Me.Controls.Add(Me.cboStartMonth)
        Me.Controls.Add(Me.lblStop)
        Me.Controls.Add(Me.lblStart)
        Me.Controls.Add(Me.btnSearch)
        Me.Menu = Me.MainMenu1
        Me.Name = "Form3"
        Me.Text = "Production Details"
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal
        Me.intCount = 0

        Me.cboStartYear.DataSource = Module1.DropBoxYears
        Me.cboStopYear.DataSource = Module1.DropBoxYears

        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text
        If cboStartMonth.Text = "" Then
            Me.cboStartMonth.Text = Date.Today.Month.ToString
            Me.cboStartDay.Text = Date.Today.Day.ToString
            Me.cboStartYear.Text = Date.Today.Year.ToString
            Me.cboStopMonth.Text = Date.Today.Month.ToString
            Me.cboStopDay.Text = Date.Today.Day.ToString
            Me.cboStopYear.Text = Date.Today.Year.ToString
            Return
        End If
        'Dim ReviewTest As New clsOrderHeader("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "'")
        ''Dim rvstr As String = ""
        ''If ReviewTest.IsNew Then
        ''    rvstr = " and Reviewed='0'"
        ''End If

        Dim o As New clsOrderProd("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "'")

        For Each r As DataRow In o.m_dt.Rows
            Me.intCount += 1
            Dim ol As New clsOrder(" OrderNum = '" & r.Item("HeaderNum") & "'")
            For Each r2 As DataRow In ol.m_dt.Rows
                If Not IsDBNull(r2.Item("QtyToShip")) Then

                    Dim bl As New clsBOMLine(" [BOM No_] = '" & r2.Item("Item") & "'")
                    'r2.Item("QtyToShip") = 2

                    r2.Item("ManHours") = CDec(r2.Item("QtyToShip")) * CDec(bl.GetManHours)
                    decTotal += CDec(r2.Item("ManHours"))

                End If
            Next
            r.Item("Cubes") = Decimal.Round(CDec(r.Item("Cubes")), 2)
            r.Item("Qty") = Decimal.Round(CDec(r.Item("Qty")), 0)
            r.Item("LaborCost") = Decimal.Round(decTotal, 2)
            decTotal = 0
        Next

        Dim d As New DateTime(cboStartYear.Text, cboStartMonth.Text, cboStartDay.Text)


        Dim aGridTableStyle As New DataGridTableStyle
        aGridTableStyle.MappingName = "PView2"
        '
        ' Create GridColumnStyle objects for the grid columns 
        '
        aGridTableStyle.AlternatingBackColor = Color.Cornsilk
        aGridTableStyle.HeaderBackColor() = Color.Beige
        aGridTableStyle.AllowSorting = True


        Dim arl As New ArrayList
        Dim colhide As New Hashtable
        'colhide.Add("HeaderID", 1)
        'colhide.Add("Length", 1)
        'colhide.Add("Width", 1)
        'colhide.Add("Height", 1)
        'colhide.Add("Compress", 1)
        Dim colformat As New Hashtable
        'colformat.Add("QtyToShip", "#0")
        'colformat.Add("QtyRemaining", "#0")
        'colformat.Add("Compress", "#0.00")

        For Each c As DataColumn In o.m_dt.Columns
            Dim aCol1 As New DataGridTextBoxColumn

            aCol1.MappingName = c.ColumnName
            aCol1.HeaderText = c.ColumnName
            'aCol1.Format = "#0.00"
            aCol1.Width = 90
            If colhide.Contains(c.ColumnName) Then
                aCol1.Width = 0
            End If
            If colformat.Contains(c.ColumnName) Then
                aCol1.Format = colformat(c.ColumnName)
            End If
            arl.Add(aCol1)
        Next
        For Each acol As DataGridTextBoxColumn In arl
            aGridTableStyle.GridColumnStyles.Add(acol)
        Next

        Me.DataGrid1.TableStyles.Clear()
        Me.DataGrid1.TableStyles.Add(aGridTableStyle)

        'Dim o As New clsOrder("[Sell-to Customer No_] = '" & Me.ListBox1.SelectedValue & "'")

        'If Not IsDBNull(o.m_row.Item("QtyToShip")) Then

        If Not o.IsNew Then
            For Each r2 As DataRow In o.m_dt.Rows
                'If r2.Item("Reviewed") = "0" Then
                '    r2.Item("Reviewed") = "No"
                'Else
                '    r2.Item("Reviewed") = "Yes"
                'End If
                'If IsDBNull(r2.Item("Compress")) Then
                '    r2.Item("Compress") = 0
                'End If
                'If IsDBNull(r2.Item("Width")) Then
                '    r2.Item("Width") = 0
                'End If
                'If IsDBNull(r2.Item("Length")) Then
                '    r2.Item("Length") = 0
                'End If
                'If IsDBNull(r2.Item("Height")) Then
                '    r2.Item("Height") = 0
                'End If
                'If r2.Item("Compress") = 0 Then
                '    r2.Item("Compress") = 1
                'End If
                'If r2.Item("Compress") = 0 Then
                '    r2.Item("Compress") = 1
                'End If

                'Dim bl As New clsBOMLine(" [BOM No_] = '" & r2.Item("Item") & "'")

                'r2.Item("ManHours") = Decimal.Round(CDec(bl.GetManHours * CDec(r2.Item("QtyToShip"))), 2)
                'r2.Item("Cubes") = Math.Round(CDec(r2.Item("QtyToShip")) * ((CDec(r2.Item("Length")) / 12) * (CDec(r2.Item("Width")) / 12) * (CDec(r2.Item("Height") * CDec(r2.Item("Compress"))) / 12)), 1)
                ''r2.Item("Route") = "B"
                ''r2.Item("ProdDate") = "5/15/2005"
                ''r2.Item("Reviewed") = "Yes"
            Next

            Me.DataGrid1.DataSource = o.m_dt
        End If




    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim s2 As String = ""
        'Me.DataGrid1.columns(1).visible = False
        'Dim dw As New DOW

        'dw.ShowDialog()
        'Dim newdow As String = dw.newdow

        'dw.Close()
        Dim d As Date
        Dim f As New frmCalendar
        f.ShowDialog()
        d = f.d
        f.Close()
        For x As Integer = 0 To (Me.intCount - 1)
            If Me.DataGrid1.IsSelected(x) Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                'Dim l As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim crdt As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim sh As New clsSalesHeader("No_ = '" & s & "'")

                Dim con As SqlClient.SqlConnection = sh.m_con
                con.Open()
                Dim str As String = "update [" & Module1.CompanyName & "$Sales Header] set [Production Date] = '" & d.ToShortDateString & "' where [No_] = '" & s & "'"

                Dim cmd As New SqlClient.SqlCommand(str, con)
                cmd.ExecuteNonQuery()


                con.Close()
                s2 = s2 & s & " "
            End If
        Next
        MsgBox("Document #s " & s2 & " production date has been changed to " & d.ToShortDateString)
        'Button1_Click(sender, e)


    End Sub

    Private Sub Form3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If Me.l Then
        Me.btnSearch.PerformClick()
        Me.DateTimePicker1.Focus()

    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        Dim f As New frmHoursSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim f As New RouteSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        Dim f As New Form1
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem6.Click
        Dim f As New frmProdSched
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem7.Click
        Dim f As New Form2
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim f As New frmAddRoute
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem8.Click
        Application.Exit()
    End Sub
End Class
