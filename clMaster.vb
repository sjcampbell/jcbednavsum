

Public MustInherit Class clMaster

    Private m_tablename As String
    Private m_tablekey As String
    Private m_con As New OleDb.OleDbConnection
    Private m_da As OleDb.OleDbDataAdapter
    Private m_ds As DataSet
    Private m_dt As DataTable
    Private m_table As String
    Private m_row As DataRow
    Private m_cmd As OleDb.OleDbCommandBuilder
    Private m_bmb As BindingManagerBase

    'Public m_ado As clssqldb

    Private hash As New Hashtable

    Private m_Exists As Boolean = False

    Event ShowIt(ByVal actionName As String)

    Sub New(ByVal keyval As String, ByVal table As String, ByVal keyphrase As String)
        Dim colValues As System.Collections.Specialized.NameValueCollection
        colValues = System.Configuration.ConfigurationSettings.AppSettings()
        'Console.WriteLine("YOU ARE HERE")

        m_con.ConnectionString = colValues.Get("connstr")

        'Dim cn As New SqlConnection(colValues.Get("connstr"))

        ' will want to use the ini class for this
        m_tablekey = keyphrase
        'Create all the ado objects
        'm_con.ConnectionString = "Provider=SQLOLEDB;workstation id=TWDLAPTOP;packet size=4096;integrated security=SSPI;data source=;persist security info=False;initial catalog=Atmos"
        '  m_con.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" _
        '    & "Data Source=c:\Atmos.xls;Extended Properties=Excel 8.0;"

        'pStrConnection
        m_con.Open()
        m_table = table
        m_da = New OleDb.OleDbDataAdapter("Select * from " & table & " where " & keyphrase & "='" & keyval & "'", m_con)

        ' Dim da As New OleDb.OleDbDataAdapter("Select * from M_Customer", m_con)
        m_ds = New DataSet

        m_da.Fill(m_ds, table)
        m_dt = New DataTable
        m_dt = m_ds.Tables(m_table)
        'The name of the unique id field and it's value
        Dim phrase As String = keyphrase & "='" & keyval & "'"
        Dim drows() As DataRow = m_dt.Select(phrase)
        m_cmd = New OleDb.OleDbCommandBuilder(m_da)

        'Does it exist
        If m_dt.Rows.Count > 0 Then
            m_Exists = True
            m_row = drows(0)
        Else
            'Code to add new one 
            m_row = m_dt.NewRow()
            m_dt.Rows.Add(m_row)
        End If

        'Build hash of values, empty if new row
        For Each s As String In Fieldnames()
            If m_dt.Rows.Count > 0 Then
                hash(s) = m_row.Item(s)
            Else
                hash(s) = ""
            End If

        Next
        m_con.Close()

    End Sub



    Overloads Function FetchHash(ByVal keyphrase As String) As ArrayList
        'returns an arraylist of hashs of all the rows returned using the keyphrase

        m_con.Open()
        Dim arl As New ArrayList
        Dim da As New OleDb.OleDbDataAdapter("Select * from " & m_table & " where " & keyphrase, m_con)

        Dim ds As New DataSet

        da.Fill(ds, m_table)
        Dim dt As New DataTable
        dt = ds.Tables(m_table)


        Dim drows() As DataRow = dt.Select(keyphrase)
        For Each r As DataRow In drows
            Dim h As New Hashtable
            For Each s As String In (hash.Keys)
                h.Add(s, r.Item(s))
            Next
            arl.Add(h)
        Next

        m_con.Close()
        Return arl
    End Function




    Overloads Function FetchHash(ByVal keyphrase As String, ByVal ncol As String, ByVal vcol As String, ByVal orderphrase As String) As ArrayList
        'Overloaded version return an arraylist of clClassify objects 
        'ncol and vcol describe the fields witl the Name and Value properties

        m_con.Open()
        Dim arl As New ArrayList
        Dim da As New OleDb.OleDbDataAdapter("Select * from " & m_table & " where " & keyphrase, m_con)

        Dim ds As New DataSet

        da.Fill(ds, m_table)
        Dim dt As New DataTable
        dt = ds.Tables(m_table)

        'Dim phrase As String = keyphrase & "='" & keyval & "'"
        Dim drows() As DataRow = dt.Select(keyphrase, orderphrase)

        For Each r As DataRow In drows
            Dim h As New Hashtable
            For Each s As String In (hash.Keys)
                h.Add(s, r.Item(s))
            Next
            Dim c As New clClassify(h(ncol), h(vcol))
            arl.Add(c)
        Next

        m_con.Close()
        Return arl
    End Function




    Function Fieldnames() As ArrayList
        'returns an arraylist of strings containing the field names 
        Dim arl As New ArrayList

        For Each c As DataColumn In m_ds.Tables(m_table).Columns
            arl.Add(c.ColumnName)
        Next

        Return arl
    End Function



    Public Function ReturnAll() As ArrayList
        Dim employees As ArrayList = New ArrayList
        'May not use it
        Return employees
    End Function



    Public Property GetVal(ByVal v As String) As String
        'GetVal and SetVal use the appropriate hash element 

        Get
            If IsDBNull(hash(v)) Then
                Return ""
            End If
            Return hash(v)
        End Get
        Set(ByVal Value As String)

        End Set
    End Property


    Public Property SetVal(ByVal v As String) As String
        Get

        End Get
        Set(ByVal Value As String)
            hash(v) = Value
        End Set
    End Property



    Public Property FullHash() As Hashtable
        'returns the full hash
        Get
            Return hash
        End Get
        Set(ByVal Value As Hashtable)

        End Set
    End Property

    Sub BindClass(ByRef o As Form)
        Dim objControl As New Control
        'loops through the forms controls and looks for names(minus the 1st 3 chars) that match
        'field names and binds them.  Just does text boxes now.
        For Each objControl In o.Controls
            If objControl.GetType.Name = "TextBox" Then
                Dim name As String
                name = objControl.Name
                name = name.Substring(3)
                If hash.Contains(name) Then
                    objControl.DataBindings.Clear()
                    objControl.DataBindings.Add("Text", m_ds, m_table & "." & name)
                End If
            ElseIf objControl.GetType.Name = "ComboBox" Then
                Dim name As String
                name = objControl.Name
                name = name.Substring(3)
                If hash.Contains(name) Then
                    objControl.DataBindings.Clear()
                    objControl.DataBindings.Add("SelectedValue", m_ds, m_table & "." & name)
                End If
            End If
        Next

        m_bmb = o.BindingContext(m_ds, m_tablename)

    End Sub

    Sub LoadLBX(ByRef o As Object, ByVal keyphrase As String, ByVal ncol As String, ByVal vcol As String, ByVal orderphrase As String)
        'populates the passed lbx given search phrase, value column, and name column
        o.DisplayMember = "Name"
        o.ValueMember = "Value"
        o.DataSource = Me.FetchHash(keyphrase, ncol, vcol, orderphrase)
    End Sub

    Sub update()
        m_con.Open()

        m_row.BeginEdit()
        m_row.EndEdit()
        If m_row.HasErrors Then
            m_row.RejectChanges()
            m_ds.RejectChanges()
            m_row.ClearErrors()
        Else
            Try
                m_da.Update(m_dt)
            Catch
                m_row.RejectChanges()
                m_ds.RejectChanges()
                m_row.ClearErrors()
                m_con.Close()
                Return
            End Try

            m_row.AcceptChanges()
            m_ds.AcceptChanges()
        End If
        m_con.Close()
    End Sub

    Sub Sync(ByVal fields() As String)
        For Each fld As String In fields
            m_row.Item(fld) = Me.GetVal(fld)
        Next

    End Sub


    Sub delete()
        m_con.Open()
        m_row.Delete()
        Try
            m_da.Update(m_dt)
        Catch
            MsgBox("Unable to delete")
            m_ds.RejectChanges()
            m_con.Close()
            Return
        End Try
        m_ds.AcceptChanges()
        m_con.Close()
    End Sub



    '########################################################


    Public Class clClassify
        Private m_name As String
        Private m_value As String

        'seems silly to have to do this but I need it to turn the objects hash items into objects
        'to populate lists.


        Sub New(ByVal ncol As String, ByVal vcol As String)
            Name = ncol
            Value = vcol
        End Sub


        Public Property Name() As String
            Get
                Return m_name
            End Get
            Set(ByVal Value As String)
                m_name = Value
            End Set
        End Property


        Public Property Value() As String
            Get
                Return m_value
            End Get
            Set(ByVal Value As String)
                m_value = Value
            End Set
        End Property

    End Class

End Class
