Imports CRHelper
Public Class frmDynamicReports
    Inherits System.Windows.Forms.Form

    Public report As String
    Public route As String
    Public OrderList As String = ""

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents DateTimePicker2 As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkStraightToPrinter As System.Windows.Forms.CheckBox
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents CheckBox3 As System.Windows.Forms.CheckBox
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button2 = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker
        Me.chkStraightToPrinter = New System.Windows.Forms.CheckBox
        Me.DataGrid1 = New System.Windows.Forms.DataGrid
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Button1 = New System.Windows.Forms.Button
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.ListBox2 = New System.Windows.Forms.ListBox
        Me.CheckBox3 = New System.Windows.Forms.CheckBox
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(504, 56)
        Me.Button2.Name = "Button2"
        Me.Button2.TabIndex = 26
        Me.Button2.Text = "Print all"
        Me.Button2.Visible = False
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(870, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(40, 23)
        Me.Label2.TabIndex = 25
        Me.Label2.Text = "To"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(608, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 23)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "From"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Location = New System.Drawing.Point(918, 56)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.TabIndex = 23
        '
        'chkStraightToPrinter
        '
        Me.chkStraightToPrinter.Location = New System.Drawing.Point(376, 56)
        Me.chkStraightToPrinter.Name = "chkStraightToPrinter"
        Me.chkStraightToPrinter.Size = New System.Drawing.Size(112, 32)
        Me.chkStraightToPrinter.TabIndex = 21
        Me.chkStraightToPrinter.Text = "Send straight to printer."
        '
        'DataGrid1
        '
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(56, 112)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.Size = New System.Drawing.Size(1024, 264)
        Me.DataGrid1.TabIndex = 19
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(656, 56)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.TabIndex = 22
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(288, 56)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(80, 23)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "View results"
        '
        'ListBox1
        '
        Me.ListBox1.Location = New System.Drawing.Point(16, 8)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(120, 95)
        Me.ListBox1.TabIndex = 27
        '
        'CheckBox1
        '
        Me.CheckBox1.Location = New System.Drawing.Point(288, 16)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(144, 24)
        Me.CheckBox1.TabIndex = 28
        Me.CheckBox1.Text = "Show only unsheduled"
        '
        'CheckBox2
        '
        Me.CheckBox2.Location = New System.Drawing.Point(472, 16)
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.Size = New System.Drawing.Size(184, 24)
        Me.CheckBox2.TabIndex = 29
        Me.CheckBox2.Text = "Show all"
        '
        'ListBox2
        '
        Me.ListBox2.Location = New System.Drawing.Point(144, 8)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(120, 95)
        Me.ListBox2.TabIndex = 30
        '
        'CheckBox3
        '
        Me.CheckBox3.Location = New System.Drawing.Point(712, 16)
        Me.CheckBox3.Name = "CheckBox3"
        Me.CheckBox3.Size = New System.Drawing.Size(128, 24)
        Me.CheckBox3.TabIndex = 31
        Me.CheckBox3.Text = "Select Order Set"
        Me.CheckBox3.Visible = False
        '
        'frmDynamicReports
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1136, 414)
        Me.Controls.Add(Me.CheckBox3)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.CheckBox2)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DateTimePicker2)
        Me.Controls.Add(Me.chkStraightToPrinter)
        Me.Controls.Add(Me.DataGrid1)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button2)
        Me.Name = "frmDynamicReports"
        Me.Text = "frmDynamicReports"
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmDynamicReports_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim rl As New clsReportList("ReportName > ''")
        Dim dt2 As DataTable = rl.distinct("ReportName")
        Me.ListBox1.ValueMember = "ReportName"
        Me.ListBox1.DisplayMember = "ReportName"
        Me.ListBox1.DataSource = dt2
        Dim pg2 As New clsRoute("")
        Dim dt4 As New DataTable
        dt4 = pg2.distinct("[Route Code]")
        Me.ListBox2.DisplayMember = "Route Code"
        Me.ListBox2.ValueMember = "Route Code"

        Dim r As DataRow = dt4.NewRow

        r.Item("Route Code") = "ALL"
        dt4.Rows.InsertAt(r, 0)
        Me.ListBox2.DataSource = dt4
        Me.Button1.PerformClick()
        If Module1.testmode = 0 Then
            Me.Close()
        End If
    End Sub



    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        'If Me.CheckBox3.Checked Then
        '    Dim f As New Form2
        '    f.cboStartDay.Text = Me.DateTimePicker1.Value.Day
        '    f.cboStartMonth.Text = Me.DateTimePicker1.Value.Month
        '    f.cboStartYear.Text = Me.DateTimePicker1.Value.Year
        '    f.cboStopDay.Text = Me.DateTimePicker2.Value.Day
        '    f.cboStopMonth.Text = Me.DateTimePicker2.Value.Month
        '    f.cboStopYear.Text = Me.DateTimePicker2.Value.Year

        '    f.ShowDialog()
        '    If f.OrderList.Length > 0 Then
        '        OrderList = " and  dbo.[" & Module1.companynamestr & "$BOM Explosion].[Document No_] in" & f.OrderList
        '    End If
        '    f.Close()
        'End If
        Dim sper As String = Me.ListBox1.SelectedValue
        'sper = "ALL"
        Dim rp As New clsReportDefs(" Report = '" & report & "' and Company='" & Module1.companynamestr & "'")
        Dim selected(rp.m_dt.Rows.Count - 1) As String
        Dim count As Integer = 0
        For Each r As DataRow In rp.m_dt.Rows
            selected(count) = "'" & r.Item("Item") & "'"
            count += 1
        Next
        Dim instr As String = String.Join(",", selected)
        Dim startdate As String = Me.DateTimePicker1.Value.ToShortDateString
        Dim enddate As String = Me.DateTimePicker2.Value.ToShortDateString
        If Me.CheckBox1.Checked Then
            startdate = "1/1/1753"
            enddate = "1/1/2001"
        End If
        If Me.CheckBox2.Checked Then
            startdate = "1/1/1753"
            enddate = "1/1/2100"
        End If
        Dim sl As Object
        If report = "Insulator" Then
            If route = "All" Then
                sl = New clsDynamicInsulator2(companynamestr, " [Item No_] in(" & instr & ") ", startdate, enddate, OrderList)
            Else
                sl = New clsDynamicInsulator2(companynamestr, " [Item No_] in(" & instr & ") and [Ship-to Route Code] ='" & route & "'", startdate, enddate, OrderList)

            End If

        ElseIf report = "Encasement" Then
            If route = "All" Then
                sl = New clsDynamicReport2(companynamestr, " [Item No_] in(" & instr & ") ", startdate, enddate, OrderList)
            Else
                sl = New clsDynamicReport2(companynamestr, " [Item No_] in(" & instr & ") and [Ship-to Route Code] ='" & route & "'", startdate, enddate, OrderList)

            End If

        ElseIf Me.report = "Production" Then
            If Me.route = "All" Then
                sl = New clsDynamicProduction(companynamestr, "", startdate, enddate, OrderList)
            Else
                sl = New clsDynamicProduction(companynamestr, "  [Ship-to Route Code] ='" & Me.route & "'", startdate, enddate, OrderList)

            End If
            Dim ht As New Hashtable
            ht.Add(0, "None")
            ht.Add(1, "SP")
            ht.Add(2, "FE")
            ht.Add(3, "IS")

            For Each r As DataRow In sl.m_dt.Rows
                r.Item("FEI") = ht(r.Item("Foam Encased_Innerspring"))
            Next
        ElseIf Me.report = "Manpower" Then
            If Me.route = "All" Then
                sl = New clsDynamicLabor(companynamestr, "", startdate, enddate, OrderList)
            Else
                sl = New clsDynamicLabor(companynamestr, "  [Ship-to Route Code] ='" & Me.route & "'", startdate, enddate, OrderList)

            End If
        ElseIf Me.report = "ManpowerNR" Then
            If Me.route = "All" Then
                sl = New clsDynamicLaborNR(companynamestr, "", startdate, enddate, OrderList)
            Else
                sl = New clsDynamicLaborNR(companynamestr, "  [Ship-to Route Code] ='" & Me.route & "'", startdate, enddate, OrderList)

            End If

        ElseIf report = "AssemblyFE" Then
            If route = "All" Then
                sl = New clsDynamicAssemblyFE(companynamestr, " [Item No_] in(" & instr & ") ", startdate, enddate, OrderList)
            Else
                sl = New clsDynamicAssemblyFE(companynamestr, " [Item No_] in(" & instr & ") and [Ship-to Route Code] ='" & route & "'", startdate, enddate, OrderList)

            End If

        ElseIf report = "AssemblyIS" Then
            If route = "All" Then
                sl = New clsDynamicAssemblyIS(companynamestr, " [Item No_] in(" & instr & ") ", startdate, enddate, OrderList)
            Else
                sl = New clsDynamicAssemblyIS(companynamestr, " [Item No_] in(" & instr & ") and [Ship-to Route Code] ='" & route & "'", startdate, enddate, OrderList)

            End If

        ElseIf report = "AssemblySP" Then
            If route = "All" Then
                sl = New clsDynamicAssemblySP(companynamestr, " [Item No_] in(" & instr & ") ", startdate, enddate, OrderList)
            Else
                sl = New clsDynamicAssemblySP(companynamestr, " [Item No_] in(" & instr & ") and [Ship-to Route Code] ='" & route & "'", startdate, enddate, OrderList)

            End If

        ElseIf Me.report = "PickList" Then
            If Me.route = "All" Then
                sl = New clsDynamicPickList(companynamestr, "", startdate, enddate, OrderList)
            Else
                sl = New clsDynamicPickList(companynamestr, "  [Ship-to Route Code] ='" & Me.route & "'", startdate, enddate, OrderList)

            End If

        ElseIf report = "ZMachine" Then
            If Me.route = "All" Then
                sl = New clsQuiltingZ(companynamestr, "", startdate, enddate, OrderList)
            Else
                sl = New clsQuiltingZ(companynamestr, "  [Ship-to Route Code] ='" & Me.route & "'", startdate, enddate, OrderList)

            End If

        Else
            If route = "All" Then
                sl = New clsQuiltingPattern(companynamestr, " [Item No_] in(" & instr & ") ", startdate, enddate, OrderList)
                If Not sl.IsNew Then
                    For Each slr As DataRow In sl.m_dt.Rows
                        Dim qd As New clsQuiltdesc("No_ = '" & slr.Item("Quilting Pattern") & "'")
                        slr.Item("quiltdesc") = qd.m_row.Item("Description")
                    Next
                End If
            Else
                sl = New clsQuiltingPattern(companynamestr, " [Item No_] in(" & instr & ") and [Ship-to Route Code] ='" & route & "'", startdate, enddate, OrderList)
                If Not sl.IsNew Then
                    For Each slr As DataRow In sl.m_dt.Rows
                        Dim qd As New clsQuiltdesc("No_ = '" & slr.Item("Quilting Pattern") & "'")
                        slr.Item("quiltdesc") = qd.m_row.Item("Description")
                    Next
                End If
            End If
        End If
        'sl = New clsDynamicReport(companynamestr, " [Item No_] in(" & instr & ") and [Ship-to Route Code] ='" & route & "'", startdate, enddate)
        '   Dim sl As New clsAdvertising(companynamestr, sper, Me.DateTimePicker1.Value.ToShortDateString, Me.DateTimePicker2.Value.ToShortDateString)
        If Not sl.IsNew Then
            'For Each r As DataRow In sl.m_dt.Rows
            '    ' If ((Not IsDBNull(r.Item("Allocated"))) And (Not IsDBNull(r.Item("Allocated")))) Then
            '    r.Item("Remaining") = CDec(r.Item("Allocated")) - CDec(r.Item("Advert"))
            '    If CDec(r.Item("Amount")) > 0 Then
            '        r.Item("Per") = (CDec(r.Item("Advert")) / CDec(r.Item("Amount"))) * 100.0
            '    End If
            '    Dim sp2 As New clsSalesperson(r.Item("Person"))
            '    r.Item("Person") = sp2.GetVal("Name")
            '    'End If
            'Next
        End If
        Me.DataGrid1.DataSource = sl.m_dt
        'sl.m_ds.WriteXmlSchema("Dynamic4.xml")
        'MsgBox("Done!")
        'Return

        Dim targetform As New frmDynamicCrpt
        'targetform.filename = "advertising.pdf"
        If report = "Insulator" Then
            targetform.crpt = New crptDynamicInsulator
        ElseIf report = "Encasement" Then
            targetform.crpt = New crptDynamicReport
        ElseIf report = "Production" Then
            targetform.crpt = New crptDynamicProduction
        ElseIf report = "Manpower" Then
            targetform.crpt = New crptDynamicLabor
        ElseIf report = "ManpowerNR" Then
            targetform.crpt = New crptDynamicLaborNR
        ElseIf report = "Manpower" Then
            targetform.crpt = New crptDynamicLabor
        ElseIf report = "ManpowerNR" Then
            targetform.crpt = New crptDynamicLaborNR
        ElseIf report = "AssemblyFE" Then
            targetform.crpt = New crptDynamicAssemblyFE
        ElseIf report = "AssemblyIS" Then
            targetform.crpt = New crptDynamicAssemblyFE
        ElseIf report = "AssemblySP" Then
            targetform.crpt = New crptDynamicAssemblyFE
        ElseIf report = "PickList" Then
            targetform.crpt = New crptDynamicPickList
        ElseIf report = "ZMachine" Then
            targetform.crpt = New crptQuiltingPattern
        Else
            targetform.crpt = New crptQuiltingPattern
        End If

        Dim cr As New CRH(targetform.crpt)
        'Dim sp2 As New clsSalesperson(Me.ListBox1.SelectedValue)
        cr.SetText("Text4", Me.report)
        cr.SetText("Text5", "Production date " & Me.DateTimePicker1.Value.ToShortDateString & " - " & Me.DateTimePicker2.Value.ToShortDateString)
        If Me.CheckBox1.Checked Then
            cr.SetText("Text5", "Unscheduled ")

        End If
        If Me.CheckBox2.Checked Then
            cr.SetText("Text5", " All ")

        End If
        targetform.ds = sl.m_ds
        If Me.chkStraightToPrinter.Checked Then
            ' Dim pd As New PrintDialog
            'If Me.report = "ZMachine" Then
            clsStraightToPrinterDR.Print(targetform.crpt, sl.m_ds, Module1.pdtherest, Module1.dresult2)
            'End If
        Else
            targetform.ShowDialog()
        End If

    End Sub
End Class
