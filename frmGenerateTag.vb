Imports CRHelper

Public Class frmGenerateTag
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents Button3 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button2 = New System.Windows.Forms.Button
        Me.DataGrid1 = New System.Windows.Forms.DataGrid
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.Button3 = New System.Windows.Forms.Button
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(552, 32)
        Me.Button2.Name = "Button2"
        Me.Button2.TabIndex = 21
        Me.Button2.Text = "Print all"
        '
        'DataGrid1
        '
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(232, 72)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.Size = New System.Drawing.Size(848, 264)
        Me.DataGrid1.TabIndex = 18
        '
        'ListBox1
        '
        Me.ListBox1.Location = New System.Drawing.Point(8, 40)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(200, 186)
        Me.ListBox1.TabIndex = 17
        '
        'CheckBox1
        '
        Me.CheckBox1.Checked = True
        Me.CheckBox1.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox1.Location = New System.Drawing.Point(352, 32)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(160, 24)
        Me.CheckBox1.TabIndex = 20
        Me.CheckBox1.Text = "Send straight to printer"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(232, 32)
        Me.Button1.Name = "Button1"
        Me.Button1.TabIndex = 19
        Me.Button1.Text = "View results"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(752, 24)
        Me.Button3.Name = "Button3"
        Me.Button3.TabIndex = 22
        Me.Button3.Text = "Button3"
        '
        'frmGenerateTag
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1000, 414)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.DataGrid1)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button2)
        Me.Name = "frmGenerateTag"
        Me.Text = "frmGenerateTag"
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        'Dim pd As New PrintDialog
        'pd.Document = New System.Drawing.Printing.PrintDocument


        'Dim result As DialogResult = pd.ShowDialog()

        'Dim gt As New clsGenerateTag("SQ10000", "10000", pd, result)
        'gt.PrintIt()
        'Return
        Dim lw As New clsLawTag("[Document No_] ='SQ10000' and [Line No_]='10000'")
        If lw.IsNew Then
            lw.m_cmd.QuotePrefix = "["
            lw.m_cmd.QuoteSuffix = "]"
            lw.m_row.Item("Tag Number") = lw.NextId("Tag Number")
            lw.m_row.Item("Document No_") = "SQ10000"
            lw.m_row.Item("Line No_") = "10000"
            '''lw.update()
        End If
        ''''''''''Dim sl3 As New clsTearOff2(" OrderNum='SQ10000' and LineNum='10000'")
        Dim sl3 As New clsTearOff2("  [" & Module1.CompanyName & "$Sales Line].[Document No_]='SQ10000' and [" & Module1.CompanyName & "$Sales Line].[Line No_]='10000'")

        Dim sl As New clsTagView(" OrderNum='SQ10000' and LineNum='10000'")
        ''''''''''''' Dim sl2 As New clsTearOff(" OrderNum='SQ10000' and LineNum='10000'")
        Dim sl2 As New clsTearOff(" [" & Module1.CompanyName & "$Sales Line].[Document No_]='SQ10000' and [" & Module1.CompanyName & "$Sales Line].[Line No_]='10000'")

        Dim arl As New ArrayList
        If Not sl2.IsNew Then
            FindSubAssemblies(0, sl2.m_row.Item("Item"), arl)
        End If
        Dim holder As DataRow = sl2.m_row

        sl2.m_dt.Rows.Clear()

        For Each ds As clsBOMLineSelect In arl

            For Each r As DataRow In ds.m_dt.Rows
                Dim drow2 As DataRow = sl2.m_dt.NewRow
                For Each col As DataColumn In sl2.m_dt.Columns
                    drow2.Item(col.ColumnName) = sl3.m_row.Item(col.ColumnName)
                Next
                drow2.Item("Labor Cost") = r.Item("Labor Cost")
                drow2.Item("BDescription") = r.Item("Description")
                drow2.Item("Item") = r.Item("BOM No_")
                sl2.m_dt.Rows.Add(drow2)
                Dim junk As String = "*" & lw.m_row.Item("Tag Number") & "_" & r.Item("BOM No_") & "_" & r.Item("Line No_") & "*"
                drow2.Item("ScanField") = "*" & lw.m_row.Item("Tag Number") & "_" & r.Item("BOM No_") & "_" & r.Item("Line No_") & "*"
                'Write row to TearOffTag table
                Dim eto As New clsEmployeeTearOff("BOMNo ='" & r.Item("BOM No_") & "' and LineNum='" & r.Item("Line No_") & "' and LawTagNum='" & lw.m_row.Item("Tag Number") & "'", CompanyName)
                If eto.IsNew Then
                    eto.m_row.Item("LawTagNum") = lw.m_row.Item("Tag Number")
                    eto.m_row.Item("BOMNo") = r.Item("BOM No_")
                    eto.m_row.Item("LineNum") = r.Item("Line No_")
                    eto.m_row.Item("Amount") = r.Item("Labor Cost")
                    eto.m_cmd.QuotePrefix = "["
                    eto.m_cmd.QuoteSuffix = "]"
                    'eto.update()
                End If

            Next
        Next
        sl.m_row.Item("LawTag") = lawtag(lw.m_row)
        For Each r As DataRow In sl2.m_dt.Rows
            r.Item("LawTag") = lawtag(lw.m_row)
        Next
        sl.m_ds.WriteXmlSchema("TagView.xml")
        sl.m_ds.WriteXmlSchema("TagView2.xml")



        Dim crpt As New crptTicket
        'Dim subrpt As CrystalDecisions.CrystalReports.Engine.ReportDocument = crpt.OpenSubreport("TearOffs")


        Dim targetform As New frrmCRVProdAnDealer
        targetform.crpt = crpt
        'subrpt.SetDataSource(sl2.m_ds)

        Dim bl As New clsBOMLine(" [BOM No_] = '" & sl.m_row.Item("Item") & "' and Type = '1' ")
        Dim s As String = ""

        'For Each r As DataRow In bl.m_dt.Rows
        '    Dim q As Decimal = fixnulldec(r.Item("Quantity"))
        '    q = q.Round(q, 2)
        '    s = s & CStr(q) & " " & r.Item("Description") & ControlChars.CrLf
        'Next


        Dim cnt As New clsContents("M847974") 'sl.m_row.Item("Item"))
        'For Each r As DataRow In bl.m_dt.Rows
        '    Dim q As Decimal = fixnulldec(r.Item("Quantity"))
        '    q = q.Round(q, 2)
        '    s = s & CStr(q) & " " & r.Item("Description") & ControlChars.CrLf
        'Next

        For Each r As DataRow In cnt.m_dt.Rows
            'Dim q As Decimal = fixnulldec(r.Item("Quantity"))
            'q = q.Round(q, 2)
            s = s & r.Item("Description") & " " & Decimal.Round(CDec(r.Item("Percent of Total")), 1) & "% " & ControlChars.CrLf
            's = s & r.Item("Description") & r.Item("Percent of Total") & "  " & ControlChars.CrLf
        Next



        Dim cr As New CRH(targetform.crpt)

        ''Dim sp2 As New clsSalesperson(Me.ListBox1.SelectedValue)
        cr.CRLF("txtBOM", s)
        Dim ci As New clsCompanyInfo
        cr.SetText("txtAddr1", ci.m_row.Item("Name") & " " & ci.m_row.Item("Name 2"))
        cr.SetText("txtAddr2", ci.m_row.Item("Address") & " " & ci.m_row.Item("Address 2"))
        cr.SetText("txtAddr3", ci.m_row.Item("City") & ", " & ci.m_row.Item("County") & " " & ci.m_row.Item("Post Code"))
        Dim warranty As New clsWarranty(sl.m_row.Item("Item"))
        cr.SetText("txtWarranty", "Warranty: " & warranty.m_row.Item("Description"))


        targetform.ds = sl.m_ds
        Dim llll As String = sl.m_row.Item("LawTag")
        sl.m_dt.Rows(0).Item("LawTag") = "TEST"
        Me.DataGrid1.DataSource = sl.m_dt

        targetform.ds2 = sl2.m_dt.DataSet
        'If Me.CheckBox1.Checked Then
        '    clsStraightToPrinter.Print(targetform.crpt, sl.m_ds)
        'Else
        targetform.ShowDialog()
        'End If
    End Sub

    Function lawtag(ByRef r As DataRow) As String
        Return r.Item("Tag Number")
    End Function

    Function fixnulldec(ByVal o As Object) As Decimal
        If IsDBNull(o) Then
            Return 0
        End If
        Return o
    End Function

    Function FindSubAssemblies(ByVal counter As Integer, ByVal item As String, ByRef arl As ArrayList) As ArrayList
        'WARNING: Recursion below
        'Returns an array list of clsBOMLine objects
        If counter > 5 Then
            'Sanity check
            MsgBox("Too many levels of sub assemblies.")
            Return arl
        End If
        Dim s As String = Module1.CompanyName()
        Dim sl2 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '2' ")
        If Not sl2.IsNew Then
            For Each r As DataRow In sl2.m_dt.Rows
                FindSubAssemblies(counter + 1, sl2.m_row.Item("No_"), arl)
            Next
        End If
        Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'  and Item.[Piece Rate Ticket] = '0'")
        'Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'")
        If Not sl3.IsNew Then
            arl.Add(sl3)
        End If
    End Function

    Private Sub frmGenerateTag_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim crpt As New crptVoucher
        Dim targetform As New frrmCRVProdAnDealer
        targetform.crpt = crpt
        Dim ds As New DataSet



        'Dim cr As New CRH(targetform.crpt)

        '''Dim sp2 As New clsSalesperson(Me.ListBox1.SelectedValue)
        'cr.CRLF("txtBOM", s)
        'Dim ci As New clsCompanyInfo
        'cr.SetText("txtAddr1", ci.m_row.Item("Name") & " " & ci.m_row.Item("Name 2"))
        'cr.SetText("txtAddr2", ci.m_row.Item("Address") & " " & ci.m_row.Item("Address 2"))
        'cr.SetText("txtAddr3", ci.m_row.Item("City") & ", " & ci.m_row.Item("County") & " " & ci.m_row.Item("Post Code"))
        'Dim warranty As New clsWarranty(sl.m_row.Item("Item"))
        'cr.SetText("txtWarranty", "Warranty: " & warranty.m_row.Item("Description"))


        targetform.ds = ds
        'Dim llll As String = sl.m_row.Item("LawTag")
        'sl.m_dt.Rows(0).Item("LawTag") = "TEST"
        'Me.DataGrid1.DataSource = sl.m_dt

        'targetform.ds2 = sl2.m_dt.DataSet
        'If Me.CheckBox1.Checked Then
        '    clsStraightToPrinter.Print(targetform.crpt, sl.m_ds)
        'Else
        targetform.ShowDialog()
        'End If
    End Sub
End Class
