Imports DbMaster

Public Class clsDynamicPickList
    Inherits clSqlMaster

    Sub New(ByVal tablename As String, ByVal arg As String, ByVal startdate As String, ByVal enddate As String, ByVal orderlist As String)
        'Passes the table name, and key field to the parent class

        MyBase.New()
        'Me.m_con.Open()
        '''Dim command As SqlClient.SqlCommand = _
        '''     New SqlClient.SqlCommand("spSalespersonDealer", Me.m_con)
        '''command.Parameters.Add("@tablename", tablename)
        '''command.Parameters.Add("@salesperson", salesperson)

        '''command.Parameters.Add("@startdate", startdate)
        '''command.Parameters.Add("@enddate", enddate)


        '''command.CommandType = CommandType.StoredProcedure
        ' Me.StoredProcedure(command)
        Dim Str As String = ""
        Str = Str & " SELECT sum(dbo.[" & tablename & "$BOM Explosion].[Quantity Required]) as Qty, Item.[Size Description] as Size,"

        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Item No_], "
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Description],"
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Item Category],"
        'Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Ship-to Route Code],"
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Unit of Measure] as UOM"
       
        Str = Str & " "
        Str = Str & " FROM dbo.[" & tablename & "$BOM Explosion]"
        Str = Str & " left outer join Item on Item.No_ = dbo.[" & tablename & "$BOM Explosion].[Item No_] "

        Str = Str & "  "
        Str = Str & " where (dbo.[" & tablename & "$BOM Explosion].[Production Date] >="
        Str = Str & "'" & startdate & "' and dbo.[" & tablename & "$BOM Explosion].[Production Date] <= "
        Str = Str & "'" & enddate & "') "
        Str = Str & "and  ((dbo.[" & tablename & "$BOM Explosion].[Item Category] = 'BUNKIE') "
        Str = Str & " or  (dbo.[" & tablename & "$BOM Explosion].[Item Category] = 'FOUNDATION')) "

        If Not (arg = "") Then
            Str = Str & " and " & arg
        End If
        Str = Str & " " & orderlist
        Str = Str & " group by dbo.[" & tablename & "$BOM Explosion].[Item Category],"
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Item No_],"
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Description],"
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Unit of Measure], Item.[Size Description]"
        Str = Str & "   order by  dbo.[Holding Company, LLC$BOM Explosion].[Item No_]"
        Str = Str & " "
        'If Module1.testmode = "3" Then
        'MsgBox(Str, , "Pick List")
        'End If
        Me.SelectPhrase(Str, "spPickList", "")


    End Sub
End Class
