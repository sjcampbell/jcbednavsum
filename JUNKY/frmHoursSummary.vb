Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Drawing.Text

Public Class frmHoursSummary
    Inherits System.Windows.Forms.Form

    Public route As String = ""
    Public month As String
    Public day As String
    Public year As String
    Public g As Graphics
    Public intCount As Integer = 0
    Dim c As Integer = 1
    Public arl As New ArrayList
    Public max As Decimal = 0
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cboEndYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboEndDay As System.Windows.Forms.ComboBox
    Friend WithEvents cboEndMonth As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents cboStartYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboStartDay As System.Windows.Forms.ComboBox
    Friend WithEvents cboStartMonth As System.Windows.Forms.ComboBox
    Friend WithEvents lblStart As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.cboEndYear = New System.Windows.Forms.ComboBox
        Me.cboEndDay = New System.Windows.Forms.ComboBox
        Me.cboEndMonth = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Button1 = New System.Windows.Forms.Button
        Me.cboStartYear = New System.Windows.Forms.ComboBox
        Me.cboStartDay = New System.Windows.Forms.ComboBox
        Me.cboStartMonth = New System.Windows.Forms.ComboBox
        Me.lblStart = New System.Windows.Forms.Label
        Me.btnSearch = New System.Windows.Forms.Button
        Me.DataGrid1 = New System.Windows.Forms.DataGrid
        Me.Button4 = New System.Windows.Forms.Button
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cboEndYear
        '
        Me.cboEndYear.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009", "2010"})
        Me.cboEndYear.Location = New System.Drawing.Point(164, 113)
        Me.cboEndYear.Name = "cboEndYear"
        Me.cboEndYear.Size = New System.Drawing.Size(64, 21)
        Me.cboEndYear.TabIndex = 42
        '
        'cboEndDay
        '
        Me.cboEndDay.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})
        Me.cboEndDay.Location = New System.Drawing.Point(108, 113)
        Me.cboEndDay.Name = "cboEndDay"
        Me.cboEndDay.Size = New System.Drawing.Size(40, 21)
        Me.cboEndDay.TabIndex = 41
        '
        'cboEndMonth
        '
        Me.cboEndMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cboEndMonth.Location = New System.Drawing.Point(36, 113)
        Me.cboEndMonth.Name = "cboEndMonth"
        Me.cboEndMonth.Size = New System.Drawing.Size(56, 21)
        Me.cboEndMonth.TabIndex = 40
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(44, 89)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 17)
        Me.Label1.TabIndex = 39
        Me.Label1.Text = "Select end date:"
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Location = New System.Drawing.Point(688, 16)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(368, 448)
        Me.Panel1.TabIndex = 38
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(252, 17)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(104, 23)
        Me.Button1.TabIndex = 37
        Me.Button1.Text = "Details"
        '
        'cboStartYear
        '
        Me.cboStartYear.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009", "2010"})
        Me.cboStartYear.Location = New System.Drawing.Point(164, 56)
        Me.cboStartYear.Name = "cboStartYear"
        Me.cboStartYear.Size = New System.Drawing.Size(64, 21)
        Me.cboStartYear.TabIndex = 36
        '
        'cboStartDay
        '
        Me.cboStartDay.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})
        Me.cboStartDay.Location = New System.Drawing.Point(108, 56)
        Me.cboStartDay.Name = "cboStartDay"
        Me.cboStartDay.Size = New System.Drawing.Size(40, 21)
        Me.cboStartDay.TabIndex = 35
        '
        'cboStartMonth
        '
        Me.cboStartMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cboStartMonth.Location = New System.Drawing.Point(36, 56)
        Me.cboStartMonth.Name = "cboStartMonth"
        Me.cboStartMonth.Size = New System.Drawing.Size(56, 21)
        Me.cboStartMonth.TabIndex = 34
        '
        'lblStart
        '
        Me.lblStart.Location = New System.Drawing.Point(44, 32)
        Me.lblStart.Name = "lblStart"
        Me.lblStart.Size = New System.Drawing.Size(100, 17)
        Me.lblStart.TabIndex = 33
        Me.lblStart.Text = "Select start date:"
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(252, 57)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(104, 23)
        Me.btnSearch.TabIndex = 32
        Me.btnSearch.Text = "Search Records"
        '
        'DataGrid1
        '
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(16, 176)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.PreferredColumnWidth = 105
        Me.DataGrid1.RowHeaderWidth = 95
        Me.DataGrid1.Size = New System.Drawing.Size(656, 296)
        Me.DataGrid1.TabIndex = 31
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(256, 104)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(128, 40)
        Me.Button4.TabIndex = 43
        Me.Button4.Text = "Weekly Prod Sched Report"
        Me.Button4.Visible = False
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem10, Me.MenuItem8})
        Me.MenuItem1.Text = "Tools"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Route summary"
        '
        'MenuItem3
        '
        Me.MenuItem3.Enabled = False
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "Production summary"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 2
        Me.MenuItem4.Text = "Route details"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "Production details"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 4
        Me.MenuItem6.Text = "Weekly report"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 5
        Me.MenuItem7.Text = "Law tags"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 6
        Me.MenuItem10.Text = "-"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 7
        Me.MenuItem8.Text = "Exit application"
        '
        'frmHoursSummary
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1072, 482)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.cboEndYear)
        Me.Controls.Add(Me.cboEndDay)
        Me.Controls.Add(Me.cboEndMonth)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.cboStartYear)
        Me.Controls.Add(Me.cboStartDay)
        Me.Controls.Add(Me.cboStartMonth)
        Me.Controls.Add(Me.lblStart)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.DataGrid1)
        Me.Menu = Me.MainMenu1
        Me.Name = "frmHoursSummary"
        Me.Text = "Production Summary"
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmHoursSummary_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Button1.Enabled = False
        Me.cboStartDay.Text = Date.Now.Day
        Me.cboStartMonth.Text = Date.Now.Month
        Me.cboStartYear.Text = Date.Now.Year
        Me.cboEndDay.Text = Date.Now.Day
        Me.cboEndMonth.Text = Date.Now.Month
        Me.cboEndYear.Text = Date.Now.Year
        'Me.btnSearch.PerformClick()
    End Sub



    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.Button1.Enabled = True
        Dim strStartDate As String
        Dim strEndDate As String
       
        Dim strNumRows As String
        c = 1
        intCount = 0
        arl.Clear()
        max = 0
        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboEndMonth.Text & "/" & cboEndDay.Text & "/" & cboEndYear.Text
        Dim d1s() As String = strStartDate.Split("/")
        Dim dts As New Date(d1s(2), d1s(0), d1s(1))
        Dim d2s() As String = strEndDate.Split("/")
        Dim dte As New Date(d2s(2), d2s(0), d2s(1))
        Dim dtc As New Date(d1s(2), d1s(0), d1s(1))


        Dim ds As New DataSet
        Dim dt As New DataTable("Routes")
        'dt.Columns.Add("Route", GetType(String))
        dt.Columns.Add("LaborCost", GetType(Decimal))
        dt.Columns.Add("Pcs", GetType(Decimal))
        dt.Columns.Add("Production date", GetType(DateTime))
        ds.Tables.Add(dt)



        'Dim sr As DataRow = dt.NewRow
        'sr.Item("Route") = "One"
        'sr.Item("Cubes") = 12.5
        'dt.Rows.Add(sr)
        'While (dtc <= dte)
        Dim od As New clsHrOrder("([Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "')")
        For Each dayrow As DataRow In od.m_dt.Rows
            ' Dim strCurrentDate As String = dtc.Month & "/" & dtc.Day & "/" & dtc.Year
            Dim strCurrentDate As String = dayrow.Item("Production Date")

            Dim decCount As Decimal = 0
            Dim decTotal As Decimal = 0

            'Dim o As New clsOrder("[ProdDate] = '" & strCurrentDate & "'")
            Dim o As New clsOrder("[ProdDate] = '" & dayrow.Item("Production Date") & "'")
            'Dim o As New clsOrder("[Sell-to Customer No_] = '" & Me.ListBox1.SelectedValue & "'")

            For Each r2 As DataRow In o.m_dt.Rows
                If Not IsDBNull(o.m_row.Item("QtyToShip")) Then

                    Dim bl As New clsBOMLine(" [BOM No_] = '" & r2.Item("Item") & "'")
                    'r2.Item("QtyToShip") = 2

                    r2.Item("ManHours") = CDec(r2.Item("QtyToShip")) * CDec(bl.GetManHours)
                    decTotal += CDec(r2.Item("ManHours"))
                    If CDec(r2.Item("ManHours")) > max Then
                        max = CDec(r2.Item("ManHours"))
                    End If

                End If
                decCount += CDec(r2.Item("QtyToShip"))

            Next

            Dim sr As DataRow = dt.NewRow
            '   sr.Item("Route") = "zz"
            'Dim z As String = r.Item("Ship-to Route Code")
            sr.Item("LaborCost") = Math.Round(decTotal, 1)
            sr.Item("Pcs") = Math.Round(decCount, 0)
            sr.Item("Production Date") = strCurrentDate
            If CDec(sr.Item("LaborCost")) > 0 Then
                dt.Rows.Add(sr)
                Dim s As String() = {strCurrentDate, CStr(decTotal)}
                arl.Add(s)
                intCount += 1
            End If

            dtc = dtc.AddDays(1)
            'End While
        Next

        If 1 = 1 Then

            Dim strCurrentDate As String = "1/1/2001"

            Dim decTotal As Decimal = 0
            Dim decCount As Decimal = 0

            Dim o As New clsOrder("[ProdDate] <= '" & strCurrentDate & "'")
            'Dim o As New clsOrder("[Sell-to Customer No_] = '" & Me.ListBox1.SelectedValue & "'")

            For Each r2 As DataRow In o.m_dt.Rows
                If Not IsDBNull(o.m_row.Item("QtyToShip")) Then

                    Dim bl As New clsBOMLine(" [BOM No_] = '" & r2.Item("Item") & "'")
                    'r2.Item("QtyToShip") = 2

                    r2.Item("ManHours") = CDec(r2.Item("QtyToShip")) * CDec(bl.GetManHours)
                    decTotal += CDec(r2.Item("ManHours"))
                    If CDec(r2.Item("ManHours")) > max Then
                        max = CDec(r2.Item("ManHours"))
                    End If
                    decCount += CDec(r2.Item("QtyToShip"))
                End If

            Next

            Dim sr As DataRow = dt.NewRow
            '   sr.Item("Route") = "zz"
            'Dim z As String = r.Item("Ship-to Route Code")
            sr.Item("LaborCost") = Math.Round(decTotal, 1)
            sr.Item("Production Date") = strCurrentDate
            If CDec(sr.Item("LaborCost")) > 0 Then
                dt.Rows.Add(sr)
                Dim s As String() = {strCurrentDate, CStr(decTotal)}
                arl.Add(s)
                intCount += 1
            End If
        End If











        Me.DataGrid1.DataSource = dt
        c += 1
        Me.Panel1.Invalidate()
        '   Me.Panel1_Paint(sender, e)
    End Sub

    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

        Dim font1 As New Font("Arial", 8)
        g = e.Graphics
        g.Clear(System.Drawing.Color.Beige)
        If max > 0 Then
            Dim b As New SolidBrush(Color.FromArgb(128, 128, 128))
            Dim y As Integer = 10
            Dim mult As Decimal = Me.Panel1.Width / max

            For Each s As String() In arl
                Dim w As Integer = CInt(CInt(s(1)) * mult)
                g.FillRectangle(b, 0, y, w, 10)
                g.DrawString(s(0), font1, Brushes.Orange, 0, y - 2)
                y += 18
            Next
        End If
        g.Dispose()

    End Sub


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        month = Me.cboStartMonth.Text
        day = Me.cboStartDay.Text
        year = Me.cboStartYear.Text
        For x As Integer = 0 To (Me.intCount - 1)
            'Dim i As Integer = Me.DataGrid1.CurrentRowIndex()

            If x = 0 Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                Me.route = s
            End If
            If Me.DataGrid1.IsSelected(x) Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                Me.route = s
                Dim d As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim da() As String = d.Split("/")
                month = da(0)
                day = da(1)
                year = da(2)

            End If
        Next
        gendday = Me.cboEndDay.Text
        gendmonth = Me.cboEndMonth.Text
        gendyear = Me.cboEndYear.Text
        gstartday = Me.cboStartDay.Text
        gstartmonth = Me.cboStartMonth.Text
        gstartyear = Me.cboStartYear.Text
        Dim f As New Form3
        f.cboStartMonth.Text = month
        f.cboStartDay.Text = day
        f.cboStartYear.Text = year
        f.cboStopMonth.Text = month
        f.cboStopDay.Text = day
        f.cboStopYear.Text = year
        Me.Hide()
        f.ShowDialog()
        Me.Show()

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim f As New frmProdSched
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim f As New RouteSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        Dim f As New Form1
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem5.Click
        Dim f As New Form3
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem6.Click
        Dim f As New frmProdSched
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem7.Click
        Dim f As New Form2
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim f As New frmAddRoute
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem8.Click
        Application.Exit()

    End Sub
End Class
