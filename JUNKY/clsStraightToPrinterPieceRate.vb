Public Class clsStraightToPrinterPieceRate

    Shared Sub Print(ByRef crpt As Object, ByVal ds As DataSet, ByVal ds2 As DataSet, ByVal ds3 As DataSet, ByVal pd As PrintDialog, ByVal result As DialogResult)

        'Big subreport on left hols 10 labor tags
        Dim subrpt As CrystalDecisions.CrystalReports.Engine.ReportDocument = crpt.OpenSubreport("TearOffs")
        subrpt.SetDataSource(ds2)

        'Smaller subreport to hold labor tags 11-13
        Dim subrpt2 As CrystalDecisions.CrystalReports.Engine.ReportDocument = crpt.OpenSubreport("sub2")
        subrpt2.SetDataSource(ds3)


        crpt.SetDataSource(ds)


        Dim Printer As String = pd.Document.PrinterSettings.PrinterName
        Dim Copies As String = pd.Document.PrinterSettings.Copies

        Dim FromPage As Integer = 1 'pd.Document.PrinterSettings.FromPage
        Dim ToPage As Integer = 1 'pd.Document.PrinterSettings.ToPage




        If (result = DialogResult.OK) Then
            crpt.PrintOptions.PrinterName = Printer
            'crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Manual
            'The case statement below should not make any difference, but I left it in case we need it sometime
            Select Case Module1.lawtagtray
                Case "Manual"
                    ' MsgBox("Manual")
                    crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Manual
                Case "LargeCapacity"
                    'MsgBox("LargeCapacity")
                    crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.LargeCapacity
                Case "Lower"
                    'MsgBox("Lower")
                    crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Lower
                Case "Middle"
                    'MsgBox("Middle")
                    crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Middle
                Case "Upper"
                    'MsgBox("Upper")
                    crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Upper
                Case "Auto"
                    'MsgBox("Auto")
                    crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Auto
            End Select

            If Module1.testmode = 0 Then
                crpt.PrintToPrinter(Copies, False, FromPage, ToPage)

            End If
        End If
    End Sub

End Class
