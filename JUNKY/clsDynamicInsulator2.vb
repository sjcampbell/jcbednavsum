Imports DbMaster
Public Class clsDynamicInsulator2

    Inherits clSqlMaster

    Sub New(ByVal tablename As String, ByVal arg As String, ByVal startdate As String, ByVal enddate As String, ByVal orderlist As String)
        'Passes the table name, and key field to the parent class

        MyBase.New()
        'Me.m_con.Open()
        '''Dim command As SqlClient.SqlCommand = _
        '''     New SqlClient.SqlCommand("spSalespersonDealer", Me.m_con)
        '''command.Parameters.Add("@tablename", tablename)
        '''command.Parameters.Add("@salesperson", salesperson)

        '''command.Parameters.Add("@startdate", startdate)
        '''command.Parameters.Add("@enddate", enddate)


        '''command.CommandType = CommandType.StoredProcedure
        ' Me.StoredProcedure(command)
        Dim Str As String = ""
        Str = Str & " SELECT sum(dbo.[" & tablename & "$BOM Explosion].[Quantity Required]) as Qty,"
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Parent No_],"
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Item No_], "
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Description],"
        'Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Document No_],"
        'Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Line No_],"
        Str = Str & " '' as [Document No_],"
        Str = Str & " '' as [Line No_],"
        Str = Str & "  dbo.[" & tablename & "$BOM Explosion].[Ship-to Route Code],"
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Top Level Item No_]"
        Str = Str & " "
        Str = Str & " FROM dbo.[" & tablename & "$BOM Explosion]"
        Str = Str & "  "
        Str = Str & " where (dbo.[" & tablename & "$BOM Explosion].[Production Date] >= '"
        Str = Str & startdate & "' and dbo.[" & tablename & "$BOM Explosion].[Production Date] <= '"
        Str = Str & enddate & "') and"
        Str = Str & " "
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Top Level Item Cat_] = 'MATTRESS' "
        Str = Str & " and dbo.[" & tablename & "$BOM Explosion].[BOM Level] = '1'"
        If Not (arg = "") Then
            Str = Str & " and " & arg
        End If
        Str = Str & " " & orderlist
        Str = Str & " group by dbo.[" & tablename & "$BOM Explosion].[Top Level Item No_],"
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Item No_],"
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Parent No_],dbo.[" & tablename & "$BOM Explosion].[Item No_],dbo.[" & tablename & "$BOM Explosion].[Description],"
        'Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Document No_],"
        'Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Line No_],"
        Str = Str & "  dbo.[" & tablename & "$BOM Explosion].[Ship-to Route Code]"
        Str = Str & " "

        Me.SelectPhrase(Str, "INS", "")


    End Sub
End Class
