Public Class clsPrintOther

    Shared Sub Print(ByRef crpt As Object, ByVal ds As DataSet, ByVal pd As PrintDialog, ByVal result As DialogResult)
      
        crpt.SetDataSource(ds)
        Dim dt As DataTable = ds.Tables(0)

        Dim f As New frmJunk
        f.DataGrid1.DataSource = dt
        'f.ShowDialog()
        Dim Printer As String = pd.Document.PrinterSettings.PrinterName
        Dim Copies As String = pd.Document.PrinterSettings.Copies
        Dim FromPage As Integer = 1 'pd.Document.PrinterSettings.FromPage
        Dim ToPage As Integer = 3 'pd.Document.PrinterSettings.ToPage

        If (result = DialogResult.OK) Then
            crpt.PrintOptions.PrinterName = Printer
            If Module1.testmode = 0 Then
                crpt.PrintToPrinter(Copies, False, FromPage, ToPage)
            End If
        End If
    End Sub

End Class
