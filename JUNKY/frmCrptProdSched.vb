
Imports CrystalDecisions.Shared

Imports CRHelper

Public Class frmCrptProdSched
    Inherits System.Windows.Forms.Form
    Public crpt As Object
    Public ds As DataSet
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents CrystalReportViewer2 As CrystalDecisions.Windows.Forms.CrystalReportViewer
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.CrystalReportViewer2 = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.SuspendLayout()
        '
        'CrystalReportViewer2
        '
        Me.CrystalReportViewer2.ActiveViewIndex = -1
        Me.CrystalReportViewer2.Location = New System.Drawing.Point(16, 24)
        Me.CrystalReportViewer2.Name = "CrystalReportViewer2"
        Me.CrystalReportViewer2.ReportSource = Nothing
        Me.CrystalReportViewer2.Size = New System.Drawing.Size(1124, 656)
        Me.CrystalReportViewer2.TabIndex = 1
        '
        'frmCrptProdSched
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1176, 438)
        Me.Controls.Add(Me.CrystalReportViewer2)
        Me.Name = "frmCrptProdSched"
        Me.Text = "frmCrptProdSched"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmCrptProdSched_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
     
        crpt.SetDataSource(ds)
        Me.CrystalReportViewer2.ReportSource = crpt

    End Sub
End Class
