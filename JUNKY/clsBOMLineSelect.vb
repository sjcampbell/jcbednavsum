Imports DbMaster

Public Class clsBOMLineSelect

    Inherits clSqlMaster

    Sub New(ByVal arg As String)
        'Passes the table name, and key field to the parent class
        MyBase.New()
        Dim selectphrase As String
        selectphrase = "SELECT [" & CompanyName() & "$Item].[Piece Rate Ticket], dbo.[" & CompanyName() & "$Item].[Product Group Code] AS ProdGroup,dbo.[" & CompanyName() & "$Item].[No_] AS Inumber,[" & CompanyName() & "$BOM Line].*"
        selectphrase = selectphrase & "FROM [" & CompanyName() & "$BOM Line] INNER JOIN [" & CompanyName() & "$Item] ON "
        selectphrase = selectphrase & "[" & CompanyName() & "$BOM Line].[No_] = [" & CompanyName() & "$Item].[No_] where " & arg
        Me.SelectPhrase(selectphrase, "BOMLine", arg)
    End Sub


    Public Function GetManHours() As Decimal
        Dim val As Decimal = 0.0
        For Each r As DataRow In Me.m_dt.Rows
            If Not IsDBNull(r.Item("Labor Cost")) Then
                val += CDec(r.Item("Labor Cost"))
            End If
        Next
        Return val
    End Function


End Class

