Public Class frmByLines
    Inherits System.Windows.Forms.Form

    Public ordernum As String
    Public rowcount As Integer = 0

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnMakeNew As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents btnReprint As System.Windows.Forms.Button
    Friend WithEvents TxtLawTag As System.Windows.Forms.TextBox
    Friend WithEvents cmdLawtag As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.DataGrid1 = New System.Windows.Forms.DataGrid
        Me.btnMakeNew = New System.Windows.Forms.Button
        Me.btnReprint = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.TxtLawTag = New System.Windows.Forms.TextBox
        Me.cmdLawtag = New System.Windows.Forms.Button
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGrid1
        '
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(29, 102)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.Size = New System.Drawing.Size(1017, 396)
        Me.DataGrid1.TabIndex = 0
        '
        'btnMakeNew
        '
        Me.btnMakeNew.Location = New System.Drawing.Point(376, 28)
        Me.btnMakeNew.Name = "btnMakeNew"
        Me.btnMakeNew.Size = New System.Drawing.Size(90, 26)
        Me.btnMakeNew.TabIndex = 1
        Me.btnMakeNew.Text = "Make new"
        '
        'btnReprint
        '
        Me.btnReprint.Location = New System.Drawing.Point(496, 28)
        Me.btnReprint.Name = "btnReprint"
        Me.btnReprint.Size = New System.Drawing.Size(90, 26)
        Me.btnReprint.TabIndex = 2
        Me.btnReprint.Text = "Reprint"
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(605, 28)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(90, 26)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Close form"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(806, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(173, 26)
        Me.Label1.TabIndex = 4
        '
        'TxtLawTag
        '
        Me.TxtLawTag.Location = New System.Drawing.Point(64, 16)
        Me.TxtLawTag.Name = "TxtLawTag"
        Me.TxtLawTag.Size = New System.Drawing.Size(160, 22)
        Me.TxtLawTag.TabIndex = 5
        Me.TxtLawTag.Text = ""
        '
        'cmdLawtag
        '
        Me.cmdLawtag.Location = New System.Drawing.Point(48, 48)
        Me.cmdLawtag.Name = "cmdLawtag"
        Me.cmdLawtag.Size = New System.Drawing.Size(192, 23)
        Me.cmdLawtag.TabIndex = 6
        Me.cmdLawtag.Text = "Assign Law Tag to New Order"
        '
        'frmByLines
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 15)
        Me.ClientSize = New System.Drawing.Size(1075, 514)
        Me.Controls.Add(Me.cmdLawtag)
        Me.Controls.Add(Me.TxtLawTag)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnReprint)
        Me.Controls.Add(Me.btnMakeNew)
        Me.Controls.Add(Me.DataGrid1)
        Me.Name = "frmByLines"
        Me.Text = "frmByLines"
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmByLines_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Label1.Text = "Order #: " & Me.ordernum

        Dim ord As New clsByLineOrder(Me.ordernum)
        'Dim ord As New clsSalesLine(" [Document No_] = '" & Me.ordernum & "'")
        Me.DataGrid1.DataSource = ord.m_dt
        Me.rowcount = ord.m_dt.Rows.Count
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnReprint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReprint.Click

        Dim pd As PrintDialog
        Dim result As DialogResult
        pd = New PrintDialog
        pd.Document = New System.Drawing.Printing.PrintDocument
        pd.Document.DocumentName = "Freedom - Laser Law Label"

        result = pd.ShowDialog()

        Dim printed As Integer = 0

        For x As Integer = 0 To (Me.rowcount - 1)

            If Me.DataGrid1.IsSelected(x) Then
                Dim line As String = CStr(Me.DataGrid1.Item(x, 0))
                Dim quantity As Integer = CStr(Me.DataGrid1.Item(x, 2))
                Dim ItemLine As New clsItem(Me.DataGrid1.Item(x, 3))
                For y As Integer = 1 To quantity
                    'If (Mid$(ItemLine.m_row.Item("No_"), 1, 2) = "01") Or (Mid$(ItemLine.m_row.Item("No_"), 1, 2) = "02") Or (Mid$(ItemLine.m_row.Item("No_"), 1, 2) = "03") Or (Mid$(ItemLine.m_row.Item("No_"), 1, 2) = "05") Or (Mid$(ItemLine.m_row.Item("No_"), 1, 2) = "14") Then


                    'If ItemLine.m_row.Item("FR Lawtag") = 0 Then
                    'Dim gt As New clsGenerateTag(Me.ordernum, line, CStr(y), pd, result, False)
                    'printed += gt.PrintIt()
                    'Else
                    Dim itcat As New clsItemCat(ItemLine.m_row.Item("Item Category Code"))
                    If Not itcat.IsNew Then
                        Dim sl As New clsSalesLine("[Document No_] = '" & Me.ordernum & "' AND [Line No_] = " & line)
                        If sl.m_row.Item("Sell-to Customer No_") = "9083" Then
                            Dim gt As New clsGenerateTagFRstylebedinabox(Me.ordernum, line, CStr(y), pd, result, False)
                            printed += gt.PrintIt()
                        Else
                            If itcat.m_row.Item("Law Tag Style") = 1 Then
                                Dim gt As New clsGenerateTagFRstyleBox(Me.ordernum, line, CStr(y), pd, result, False)
                                printed += gt.PrintIt()
                            End If
                            If itcat.m_row.Item("Law Tag Style") = 0 Then
                                Dim gt As New clsGenerateTagFRstyle(Me.ordernum, line, CStr(y), pd, result, False)
                                printed += gt.PrintIt()
                            End If
                        End If
                    End If
                        'End If
                Next
            End If

        Next
        MsgBox(printed & " Items printed")

    End Sub

    Private Sub btnMakeNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMakeNew.Click

        Dim pd As PrintDialog
        Dim result As DialogResult
        pd = New PrintDialog
        pd.Document = New System.Drawing.Printing.PrintDocument
        pd.Document.DocumentName = "Freedom - Laser Law Label"

        result = pd.ShowDialog()

        Dim printed As Integer = 0

        For x As Integer = 0 To (Me.rowcount - 1)

            If Me.DataGrid1.IsSelected(x) Then
                Dim line As String = CStr(Me.DataGrid1.Item(x, 0))
                Dim quantity As Integer = CStr(Me.DataGrid1.Item(x, 2))
                Dim ItemLine As New clsItem(Me.DataGrid1.Item(x, 3))
                For y As Integer = 1 To quantity
                    'If (Mid$(ItemLine.m_row.Item("No_"), 1, 2) = "01") Or (Mid$(ItemLine.m_row.Item("No_"), 1, 2) = "02") Or (Mid$(ItemLine.m_row.Item("No_"), 1, 2) = "03") Or (Mid$(ItemLine.m_row.Item("No_"), 1, 2) = "05") Or (Mid$(ItemLine.m_row.Item("No_"), 1, 2) = "14") Then


                    'If ItemLine.m_row.Item("FR Lawtag") = 0 Then
                    'Dim gt As New clsGenerateAdditionalTag(Me.ordernum, line, CStr(y), pd, result, False)
                    'printed += gt.PrintIt()
                    'Else
                    Dim itcat As New clsItemCat(ItemLine.m_row.Item("Item Category Code"))
                    If Not itcat.IsNew Then
                        Dim sl As New clsSalesLine("[Document No_] = '" & Me.ordernum & "' AND [Line No_] = " & line)
                        If sl.m_row.Item("Sell-to Customer No_") = "9083" Then
                            Dim gt As New clsGenerateTagFRstylebedinabox(Me.ordernum, line, CStr(y), pd, result, False)
                            printed += gt.PrintIt()
                        Else
                            If itcat.m_row.Item("Law Tag Style") = 1 Then
                                Dim gt As New clsGenerateAdditionalTagFRBox(Me.ordernum, line, CStr(y), pd, result, False)
                                printed += gt.PrintIt()
                            End If
                            If itcat.m_row.Item("Law Tag Style") = 0 Then
                                Dim gt As New clsGenerateAdditionalTagFR(Me.ordernum, line, CStr(y), pd, result, False)
                                printed += gt.PrintIt()
                            End If
                        End If
                    End If
                    'End If
                Next
            End If

        Next
        MsgBox(printed & " Items printed")

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLawtag.Click
        Dim lt As New clsLawTag("[Tag Number] = " & Me.TxtLawTag.Text)
        If lt.IsNew Then
            MsgBox("This Law Tag number does not exist!")
            Return
        End If


        For x As Integer = 0 To (Me.rowcount - 1)
            If Me.DataGrid1.IsSelected(x) Then
                Dim lt2 As New clsLawTag("[Document No_] = '" & Me.ordernum & "' AND [Line No_] = " & Me.DataGrid1.Item(x, 0) & " AND [Changed by Doc No_]<> '" & Me.ordernum & "'")
                If lt2.IsNew Then
                    MsgBox("No more law tags for this order available to be replaced!")
                    Return
                End If
                Dim inst As New Integer
                Dim tagno As New Integer
                inst = 0
                If Not lt2.IsNew Then
                    For Each l As DataRow In lt2.m_dt.Rows
                        If l.Item("Packed") <> 1 Then
                            inst = l.Item("Instance")
                            tagno = l.Item("Tag Number")
                            Exit For
                        End If
                    Next
                End If
                Dim line As String = CStr(Me.DataGrid1.Item(x, 0))
                Dim quantity As Integer = CStr(Me.DataGrid1.Item(x, 2))
                Dim ItemLine As New clsItem(Me.DataGrid1.Item(x, 3))
                lt.m_cmd.QuotePrefix = "["
                lt.m_cmd.QuoteSuffix = "]"
                lt.m_row.Item("Document No_") = Me.ordernum
                lt.m_row.Item("Line No_") = line
                lt.m_row.Item("Instance") = inst
                lt.m_row.Item("Changed by Doc No_") = Me.ordernum
                lt.update()
                Dim lt3 As New clsLawTag("[Tag Number] = " & tagno)
                If Not lt3.IsNew Then
                    lt3.m_cmd.QuotePrefix = "["
                    lt3.m_cmd.QuoteSuffix = "]"
                    lt3.m_row.Item("Document No_") = "REPLACED"
                    lt3.update()
                End If
            End If
        Next
        MsgBox("Re-assignment complete!")
    End Sub
End Class
