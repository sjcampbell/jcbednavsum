Imports CRHelper
Imports System.IO

Public Class clsGeneratePieceRateTicketsbkp
    Public ordernum As String
    Public linenum As String
    Public instance As String
    Public pd As PrintDialog
    Public result As DialogResult
    Public typeother As Boolean

    Sub New(ByVal OrderNumber As String, ByVal LineNumber As String, ByVal instancenum As String, ByVal pdialog As PrintDialog, ByVal presult As DialogResult, ByVal other As Boolean)
        ordernum = OrderNumber
        linenum = LineNumber
        instance = instancenum
        pd = pdialog
        result = presult
        typeother = other
    End Sub

    Public Function PrintIt() As Integer
        'Only use type 2

        'OTHER Tags are created and inserted into the Tear Off table but not printed
        'LawTags are created AND printed



        Dim line As New clsSalesLine("[Document No_] ='" & ordernum & "' and [Line No_]='" & linenum & "'")
        If Not (line.m_row.Item("Type") = "2") Then
            Return 0
        End If
        Module1.ticketcounter += 1 'Uses global so we can print the page number for the whole print job
        'Look up law tag and create new one if not present
        Dim lw As New clsLawTag("[Document No_] ='" & ordernum & "' and [Line No_]='" & linenum & "' and [Instance]='" & instance & "'")
        If lw.IsNew Then
            lw.m_cmd.QuotePrefix = "["
            lw.m_cmd.QuoteSuffix = "]"
            lw.m_row.Item("Tag Number") = lw.NextId("Tag Number")
            lw.m_row.Item("Document No_") = ordernum
            lw.m_row.Item("Line No_") = linenum
            lw.m_row.Item("Instance") = instance
            lw.m_row.Item("PM Redemption Date") = "1/1/1753"
            lw.m_row.Item("PM Date") = "1/1/1753"
            lw.m_row.Item("Pack Date") = "1/1/1753"
            lw.m_row.Item("Piece Date") = "1/1/1753"
            lw.m_row.Item("Packed By") = "NOT PACKED"
            lw.m_row.Item("Packed") = 0
            lw.m_row.Item("Inventory Updated") = 0
            lw.m_row.Item("Changed by Doc No_") = " "



            lw.update()
        End If


        Dim sl3 As New clsTearOff2(" [" & Module1.CompanyName & "$Sales Line].[Document No_]='" & ordernum & "' and [" & Module1.CompanyName & "$Sales Line].[Line No_]='" & linenum & "'")
        'Look up for order # and line # from Sales Line joined to BOM info
        Dim sl As New clsTagView(" OrderNum='" & ordernum & "' and LineNum='" & linenum & "'")
        Dim sl2 As New clsTearOff("  [" & Module1.CompanyName & "$Sales Line].[Document No_]='" & ordernum & "' and [" & Module1.CompanyName & "$Sales Line].[Line No_]='" & linenum & "'")
        'Look up Sales Line to see if the BOM Item No_ is different from the regulat item #
        Dim templine As New clsSalesLine(" [Document No_] ='" & ordernum & "' and [Line No_]='" & linenum & "'")
        Dim sub2 As New clsSR2("  [" & Module1.CompanyName & "$Sales Line].[Document No_]='" & ordernum & "' and [" & Module1.CompanyName & "$Sales Line].[Line No_]='" & linenum & "'")

        If Not templine.m_row.Item("BOM Item No_") = "" Then
            sl2.m_row.Item("Item") = templine.m_row.Item("BOM Item No_")
        End If


        Dim arl As New ArrayList
        'clsTearOff has an inner join so we also have to handle hits on the BOM Item No
        If (Not sl2.IsNew) Or (Not templine.m_row.Item("BOM Item No_") = "") Then
            FindSubAssemblies(0, sl2.m_row.Item("Item"), arl) ' Call sub assembly iterator
        Else
            ' MsgBox("Is new")
        End If
        Dim holder As DataRow = sl2.m_row

        'Set up blank tables to be filled back in
        sl2.m_dt.Rows.Clear()
        sub2.m_dt.Rows.Clear()
        Dim gitem As String = templine.m_row.Item("BOM Item No_")
        Dim counter As Integer = 0
        For Each ds As clsBOMLineSelect In arl
            'arl holds an arraylist of clsBOMLineSelect objects, each of which wraps a dataset

            For Each r As DataRow In ds.m_dt.Rows
                'Loop through the data rows in the clsBOMLine select object

                'we are bulding a new dataset to combine the seperate datasets returned fro FindSubAssemblies
                'essentially converting a clsBOMLine select to a clsTearOff
                'sl2 (drow2) contains data for the big tear off sub report on the left
                'and sub2 (drow3) contains data for the little 3 item tear off sub report
                Dim drow2 As DataRow = sl2.m_dt.NewRow
                Dim drow3 As DataRow = sub2.m_dt.NewRow

                For Each col As DataColumn In sl2.m_dt.Columns
                    drow2.Item(col.ColumnName) = sl3.m_row.Item(col.ColumnName)
                Next

                For Each col As DataColumn In sub2.m_dt.Columns
                    drow3.Item(col.ColumnName) = sl3.m_row.Item(col.ColumnName)
                Next
                Dim descr As String
                Dim opercode As String
                Dim rate As Decimal
                Dim qty As Decimal
                Dim opscode As Boolean = True
                Dim Itemdesc As New clsItem(r.Item("BOM No_"))
                If r.Item("Operation Code") <> "" Then
                    Dim Ops As New clsOpCode("OpCode = '" & LCase$(r.Item("Operation Code")) & "'")
                    drow2.Item("Description") = Ops.m_row.Item("Description")
                    drow2.Item("Labor Cost") = Ops.m_row.Item("Piece Rate")
                    drow3.Item("Description") = Ops.m_row.Item("Description")
                    drow3.Item("Labor Cost") = Ops.m_row.Item("Piece Rate")
                    descr = Ops.m_row.Item("Description")
                    opercode = Ops.m_row.Item("OpCode")
                    rate = Ops.m_row.Item("Piece Rate")
                    qty = 0
                Else
                    opscode = False
                End If
                If r.Item("Operation Code2") <> "" Then
                    Dim Spec As New clsOpCode("OpCode = '" & LCase$(r.Item("Operation Code2")) & "'")
                    drow2.Item("BDescription") = Spec.m_row.Item("Description")
                    drow3.Item("BDescription") = Spec.m_row.Item("Description")
                    If opscode = False Then
                        drow2.Item("Labor Cost") = Spec.m_row.Item("Piece Rate")
                        drow2.Item("Labor Cost") = Spec.m_row.Item("Piece Rate")
                        descr = Spec.m_row.Item("Description")
                        opercode = Spec.m_row.Item("OpCode")
                        rate = Spec.m_row.Item("Piece Rate")
                        qty = 0
                    End If
                End If

                'drow2.Item("Labor Cost") = r.Item("Quantity")
                'MsgBox(r.Item("Description") & " - " & r.Item("No_"))
                'drow2.Item("BDescription") = r.Item("Description") & " - " & r.Item("No_")
                drow2.Item("Item") = r.Item("BOM No_")
                drow2.Item("ProdGroup") = r.Item("Operation Code")
                drow2.Item("LawTag") = lw.m_row.Item("Tag Number")
                drow2.Item("Name") = Itemdesc.m_row.Item("Description")
                drow2.Item("Route") = r.Item("Operation Code2")

                'drow3.Item("Labor Cost") = r.Item("Quantity")
                'MsgBox(r.Item("Description") & " - " & r.Item("No_"))
                'drow3.Item("BDescription") = r.Item("Description") & " - " & r.Item("No_")
                drow3.Item("Item") = r.Item("BOM No_")
                drow3.Item("ProdGroup") = r.Item("Operation Code")
                drow3.Item("LawTag") = lw.m_row.Item("Tag Number")
                drow3.Item("Name") = Itemdesc.m_row.Item("Description")
                drow3.Item("Route") = r.Item("Operation Code2")




                If Not r.Item("Description") = "Fringe & Overhead" Then

                    If r.Item("Print On Law Label") = 1 Then ' 3 = Labor
                        If r.Item("Operation Code") <> "" Then
                            counter += 1
                            'add the row we jus built to the sl2 data set
                            sl2.m_dt.Rows.Add(drow2)
                            'If there are more than 10 tesr offs, use the second sub report
                            If counter >= 11 And counter <= 13 Then
                                sub2.m_dt.Rows.Add(drow3)
                            End If
                        End If
                    End If
                    Dim junk As String = "*" & lw.m_row.Item("Tag Number") & "_" & r.Item("BOM No_") & "_" & r.Item("Line No_") & "*"


                    drow3.Item("ScanField") = "*" & lw.m_row.Item("Tag Number") & "_" & r.Item("BOM No_") & "_" & r.Item("Line No_") & "*"

                    drow2.Item("ScanField") = "*" & lw.m_row.Item("Tag Number") & "_" & r.Item("BOM No_") & "_" & r.Item("Line No_") & "*"
                    'Write row to TearOffTag table
                    Dim eto As New clsEmployeeTearOff("BOMNo ='" & r.Item("BOM No_") & "' and LineNum='" & r.Item("Line No_") & "' and LawTagNum='" & lw.m_row.Item("Tag Number") & "'", CompanyName)
                    If eto.IsNew Then
                        eto.m_row.Item("LawTagNum") = lw.m_row.Item("Tag Number")
                        eto.m_row.Item("BOMNo") = r.Item("BOM No_")
                        eto.m_row.Item("LineNum") = r.Item("Line No_")
                        eto.m_row.Item("Amount") = r.Item("Labor Cost")
                        eto.m_row.Item("Amount") = drow3.Item("Labor Cost")
                        Dim d As Decimal = CDec(r.Item("Quantity"))
                        d = d.Round(d, 4)
                        eto.m_row.Item("Minutes") = CStr(d) 'r.Item("Quantity")

                        eto.m_row.Item("ItemType") = r.Item("Piece Rate Ticket")
                        eto.m_row.Item("ItemNo") = r.Item("No_")
                        eto.m_row.Item("Description") = descr
                        eto.m_row.Item("OpCode") = opercode
                        eto.m_row.Item("Rate") = rate
                        eto.m_row.Item("Qty") = qty
                        ' MsgBox(r.Item("Piece Rate Ticket") & " - " & r.Item("No_"))
                        eto.m_cmd.QuotePrefix = "["
                        eto.m_cmd.QuoteSuffix = "]"
                        eto.update()
                    End If

                    'Get the damn id for what we just created
                    Dim eto2 As New clsEmployeeTearOff("BOMNo ='" & r.Item("BOM No_") & "' and LineNum='" & r.Item("Line No_") & "' and LawTagNum='" & lw.m_row.Item("Tag Number") & "'", CompanyName)
                    drow2.Item("ScanField") = "*" & eto2.m_row.Item("id") & "*"
                    drow3.Item("ScanField") = "*" & eto2.m_row.Item("id") & "*"
                End If


            Next
        Next
        For Each r As DataRow In sl.m_dt.Rows
            r.Item("LawTag") = "*" & lawtag(lw.m_row) & "*"
        Next
        For Each r As DataRow In sl2.m_dt.Rows
            r.Item("LawTag") = "*" & lawtag(lw.m_row) & "*"
        Next



        Dim crpt As New crptPieceRateTicket


        Dim targetform As New frrmCRVProdAnDealer

        targetform.crpt = crpt

        'Did the sales line have a BOM No_
        If gitem = "" Then
            gitem = sl.m_row.Item("Item")
        End If
        Dim it As New clsItem(gitem)



        Dim MatArl As New ArrayList
        Me.FindMatSubAssemblies(0, gitem, MatArl)

        'we have to look up some additional stuff like material % , company info and warranty

        Dim bl As New clsBOMLine(" [BOM No_] = '" & it.m_row.Item("BOM No_") & "' and Type = '1' and [Print On Law Label] = 1")
        Dim s As String = ""
        Dim smaterials As String = ""

        For Each r As DataRow In bl.m_dt.Rows
            Dim q As Decimal = fixnulldec(r.Item("Quantity"))
            q = q.Round(q, 2)
            smaterials = smaterials & CStr(q) & " " & r.Item("Description") & ControlChars.CrLf
        Next



        If smaterials = "" Then
            smaterials = "No BOM Items"
        End If


        Dim cnt As New clsContents(sl.m_row.Item("Item"))

        For Each r As DataRow In cnt.m_dt.Rows

            If Not IsDBNull(r.Item("Percent of Total")) Then
                s = s & r.Item("Description") & " " & Decimal.Round(CDec(r.Item("Percent of Total")), 1) & "%" & ControlChars.CrLf
            End If
        Next

        Dim cr As New CRH(crpt)
        Dim test As Boolean = False
        If Module1.testmode = "1" Then
            test = True
        End If
        'set some CR text fields using the CRHelper class
        cr.CRLF("txtBOM", s)
        cr.CRLF("txtMats", smaterials)
        Dim ci As New clsCompanyInfo
        cr.SetText("txtAddr1", ci.m_row.Item("Name") & " " & ci.m_row.Item("Name 2"))
        cr.SetText("txtAddr2", ci.m_row.Item("Address") & " " & ci.m_row.Item("Address 2"))
        cr.SetText("txtAddr3", ci.m_row.Item("City") & ", " & ci.m_row.Item("County") & " " & ci.m_row.Item("Post Code"))
        cr.SetText("txtCount", CStr(Module1.ticketcounter))
        cr.SetText("TxtDesc2", it.m_row.Item("Description 2"))
        'cr.SetText("txtCount2", CStr(Module1.ticketcounter))

        'If Not lw.IsNew Then
        'cr.SetText("txtReprint", "")
        'End If
        targetform.ds = sl.m_ds

        'the following is used to set the print job file name so it goes to the correct tray
        'done so Houstons printer can print upside down!
        Dim fp As String = crpt.FilePath

        File.Copy(fp, "Lawtag", True)


        Dim crpt2 As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        crpt2.Load("Lawtag")

        Dim cr2 As New CRH(crpt2)

        ''Dim sp2 As New clsSalesperson(Me.ListBox1.SelectedValue)
        cr2.CRLF("txtBOM", s)
        cr2.CRLF("txtMats", smaterials)
        Dim ci2 As New clsCompanyInfo
        cr2.SetText("txtAddr1", ci.m_row.Item("Name") & " " & ci.m_row.Item("Name 2") & "X" & CStr(Module1.ticketcounter))
        cr2.SetText("txtAddr2", ci.m_row.Item("Address") & " " & ci.m_row.Item("Address 2"))
        cr2.SetText("txtAddr3", ci.m_row.Item("City") & ", " & ci.m_row.Item("County") & " " & ci.m_row.Item("Post Code"))
        cr2.SetText("txtCount", CStr(Module1.ticketcounter))
        cr2.SetText("TxtDesc2", it.m_row.Item("Description 2"))
        'cr2.SetText("txtCount2", CStr(Module1.ticketcounter))

        Dim warranty As New clsWarranty(it.m_row.Item("BOM No_"))
        cr2.SetText("txtWarranty", "Warranty: " & warranty.m_row.Item("Description"))
        targetform.ds2 = sl2.m_dt.DataSet


        'If Not lw.IsNew Then
        'cr2.SetText("txtReprint", "")
        'End If
        crpt2.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.EnvManual

        If Not test Then
            clsStraightToPrinterPieceRate.Print(crpt2, sl.m_dt.DataSet, sl2.m_dt.DataSet, sub2.m_dt.DataSet, pd, result)
            'Dim GenFRTag As New clsGenerateTagFRstyle(ordernum, linenum, instance, pd, result, False)
            'GenFRTag.PrintIt()
        Else
            targetform.ShowDialog()
        End If
        crpt.Close()
        targetform.Close()
        crpt2.Close()
        crpt.Dispose()
        targetform.Dispose()
        crpt2.Dispose()
        GC.Collect()

        Return 1


    End Function

    Function lawtag(ByRef r As DataRow) As String
        Return CStr(r.Item("Tag Number"))
    End Function

    Function fixnulldec(ByVal o As Object) As Decimal
        If IsDBNull(o) Then
            Return 0
        End If
        Return o
    End Function

    Function FindSubAssemblies(ByVal counter As Integer, ByVal item As String, ByRef arl As ArrayList) As ArrayList
        'WARNING: Recursion below
        'Returns an array list of clsBOMLine objects
        Dim pre As String = item
        Dim it As New clsItem(item)
        item = it.m_row.Item("BOM No_")

        If counter > 5 Then
            'Sanity check
            ' MsgBox("Too many levels of sub assemblies.")
            Return arl
        End If

        Dim s As String = Module1.CompanyName()
        Dim sl2 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '2' ")
        If Not sl2.IsNew Then
            For Each r As DataRow In sl2.m_dt.Rows
                FindSubAssemblies(counter + 1, r.Item("No_"), arl)
            Next
        End If
        If Not typeother Then
            'Type 4 = Labor
            'Piece rate ticket 2 = labor
            'Piece rate ticket 3 = other

            'Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'  and Item.[Piece Rate Ticket] = '3'")
            Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and [" & s & "$BOM Line].[Print On Law Label] = '1' and [" & s & "$BOM Line].[Operation Code] <> ''")

            If (Not sl3.IsNew) Then
                'For Each r As DataRow In sl3.m_dt.Rows
                'Next
                arl.Add(sl3)
                ' End If
            End If
        Else
            Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and [" & s & "$BOM Line].[Print On Law Label] = '1' and [" & s & "$BOM Line].[Operation Code] <> ''")

            If (Not sl3.IsNew) Then
                'MsgBox("else Adding " & item)
                'For Each r As DataRow In sl3.m_dt.Rows
                'Next
                arl.Add(sl3)
            End If
            For Each r As DataRow In sl3.m_dt.Rows
                If Module1.othertags.Contains(r.Item("")) Then

                End If
            Next
            'End If
        End If
        If Module1.testmode = 1 Then
            ' MsgBox(
        End If
    End Function



    Function FindMatSubAssemblies(ByVal counter As Integer, ByVal item As String, ByRef arl As ArrayList) As ArrayList
        'WARNING: Recursion below
        'Returns an array list of clsBOMLine objects
        Dim pre As String = item
        Dim it As New clsItem(item)
        item = it.m_row.Item("BOM No_")
        ' MsgBox(pre & " - " & item)
        If counter > 5 Then
            'Sanity check
            ' MsgBox("Too many levels of sub assemblies.")
            Return arl
        End If

        Dim s As String = Module1.CompanyName()
        Dim sl2 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '2' ")
        If Not sl2.IsNew Then
            For Each r As DataRow In sl2.m_dt.Rows
                FindMatSubAssemblies(counter + 1, r.Item("No_"), arl)
            Next
        End If
        If Not typeother Then
            'Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'  and Item.[Piece Rate Ticket] = '3'")
            Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '1' ")
            'MsgBox("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'  and Item.[Piece Rate Ticket] = '3'")
            'If Not IsDBNull(sl3.m_row.Item("No_")) Then
            '    Dim i As New clsItem(sl3.m_row.Item("No_"))
            '    If IsDBNull(i.m_row.Item("Piece Rate Ticket")) Then
            '        i.m_row.Item("Piece Rate Ticket") = "0"
            '    End If
            '    MsgBox(sl3.m_row.Item("No_") & " " & i.m_row.Item("Piece Rate Ticket"))
            If (Not sl3.IsNew) Then
                'MsgBox("Adding " & item)
                For Each r As DataRow In sl3.m_dt.Rows
                    '           MsgBox("zz " & r.Item("Inumber") & " - " & r.item("Piece Rate Ticket"))
                Next
                arl.Add(sl3)
                ' End If
            End If
        Else
            Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '1'  ")
            'Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'")
            'If Not IsDBNull(sl3.m_row.Item("No_")) Then
            '    Dim i As New clsItem(sl3.m_row.Item("No_"))
            '    If IsDBNull(i.m_row.Item("Piece Rate Ticket")) Then
            '        i.m_row.Item("Piece Rate Ticket") = "0"
            '    End If
            If (Not sl3.IsNew) Then
                'MsgBox("else Adding " & item)
                For Each r As DataRow In sl3.m_dt.Rows
                    'MsgBox("zz " & r.Item("Inumber"))
                Next
                arl.Add(sl3)
            End If
            For Each r As DataRow In sl3.m_dt.Rows
                If Module1.othertags.Contains(r.Item("")) Then

                End If
            Next
            'End If
        End If
        If Module1.testmode = 1 Then
            ' MsgBox(counter & " - " & pre & " - " & item & " - ")
        End If
    End Function



End Class
