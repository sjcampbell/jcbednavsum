
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class CRH
    Private CR As Object
    Private param1Fields As New CrystalDecisions.Shared.ParameterFields

    Sub New(ByRef o As Object)
        CR = o
    End Sub


    Public Sub SetText(ByVal fieldname As String, ByVal fieldvalue As String)
        Dim tObj As CrystalDecisions.CrystalReports.Engine.TextObject
        tObj = CR.ReportDefinition.ReportObjects.Item(fieldname)
        tObj.Text = fieldvalue
    End Sub


    Public Sub SetParam(ByVal fieldname As String, ByVal fieldvalue As String)
        Dim param1Field As New CrystalDecisions.Shared.ParameterField
        Dim param1Range As New CrystalDecisions.Shared.ParameterDiscreteValue

        param1Field.ParameterFieldName = fieldname
        param1Range.Value = fieldvalue
        param1Field.CurrentValues.Add(param1Range)
        param1Fields.Add(param1Field)
    End Sub

    Public Function Params() As CrystalDecisions.Shared.ParameterFields
        Return Me.param1Fields
    End Function

    Public Sub LogIn(ByVal server As String, ByVal db As String, ByVal userid As String, ByVal password As String)
        Dim ac As New ApplyCRLogin
        ac._dbName = db
        ac._serverName = server
        ac._userID = userid
        ac._passWord = password
        ac.ApplyInfo(Me.CR)

    End Sub

    Public Sub CRLF(ByVal fieldname As String, ByVal fieldvalue As String)
        Dim crFormulaTextField1 As FormulaFieldDefinition
        Dim crFormulas As FormulaFieldDefinitions
        'set the Formulas collection to the current report's formula
        'collection
        crFormulas = CR.DataDefinition.FormulaFields

        crFormulaTextField1 = crFormulas.Item(fieldname)
        crFormulaTextField1.Text = TranslateStringToCRFormula(fieldvalue)

    End Sub



    Public Sub pdf(ByVal destination As String)
        CR.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile

        Dim Options As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        Options.DiskFileName = destination
        CR.ExportOptions.DestinationOptions = Options
        CR.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile
        CR.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat

        CR.Export()
    End Sub


    Public Sub doc(ByVal destination As String)
        CR.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile

        Dim Options As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        Options.DiskFileName = destination
        CR.ExportOptions.DestinationOptions = Options
        CR.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile
        CR.ExportOptions.ExportFormatType = ExportFormatType.WordForWindows

        CR.Export()
    End Sub


    Public Sub xls(ByVal destination As String)
        CR.ExportOptions.ExportDestinationType = CrystalDecisions.Shared.ExportDestinationType.DiskFile

        Dim Options As CrystalDecisions.Shared.DiskFileDestinationOptions = New CrystalDecisions.Shared.DiskFileDestinationOptions

        Options.DiskFileName = destination
        CR.ExportOptions.DestinationOptions = Options
        CR.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile
        CR.ExportOptions.ExportFormatType = ExportFormatType.Excel

        CR.Export()
    End Sub





    Private Class ApplyCRLogin
        'Found this code at: http://aspalliance.com/490 - SC 7/29/05'

        Public _dbName As String
        Public _serverName As String
        Public _userID As String
        Public _passWord As String
        Public Sub ApplyInfo(ByRef _oRpt As CrystalDecisions.CrystalReports.Engine.ReportDocument)
            Dim oCRDb As CrystalDecisions.CrystalReports.Engine.Database = _oRpt.Database
            Dim oCRTables As CrystalDecisions.CrystalReports.Engine.Tables = oCRDb.Tables
            Dim oCRTable As CrystalDecisions.CrystalReports.Engine.Table
            Dim oCRTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo
            Dim oCRConnectionInfo As New CrystalDecisions.Shared.ConnectionInfo
            oCRConnectionInfo.DatabaseName = _dbName
            oCRConnectionInfo.ServerName = _serverName
            oCRConnectionInfo.UserID = _userID
            oCRConnectionInfo.Password = _passWord
            For Each oCRTable In oCRTables
                oCRTableLogonInfo = oCRTable.LogOnInfo
                oCRTableLogonInfo.ConnectionInfo = oCRConnectionInfo
                oCRTable.ApplyLogOnInfo(oCRTableLogonInfo)


            Next


        End Sub


    End Class



    Private Function TranslateStringToCRFormula(ByVal VBString As String) As String

        Dim Returnstring As String = "'"

        'Split the string at every LF
        For Each SubString As String In VBString.Split(Chr(10))

            'Trim all the CR / LF characters
            SubString = SubString.Trim(vbCrLf.ToCharArray)

            'Form your string to the compatible CR Formula format. Chr(10) &nd Chr(13) should be inserted as a string, not as values!!
            Returnstring = Returnstring & "' & Chr(10) & Chr(13) & '" & SubString

        Next

        Returnstring = Returnstring & "'"

        Return Returnstring

    End Function




End Class
