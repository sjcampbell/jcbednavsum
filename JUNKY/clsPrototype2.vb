Imports DbMaster
Public Class clsPrototype2

    Inherits clSqlMaster

    Sub New(ByVal arg As String)
        'Passes the table name, and key field to the parent class
        'MyBase.New(arg, "[Holding Company, LLC$Ship-To Route Code]", "[Route Code]")
        'arg += "DISTINCT [Prototype No_]"

        MyBase.New("DISTINCT [Prototype No_],Model,Test", arg, "[" & Module1.CompanyName() & "$Prototype]", "Model")
    End Sub

    Public Function FoundationList() As String
        If Me.IsNew Then
            Return ""
        End If
        If Me.m_dt.Rows.Count = 0 Then
            Return ""
        End If
        Dim s(Me.m_dt.Rows.Count - 1) As String
        Dim count As Integer = 0
        For Each r As DataRow In Me.m_dt.Rows
            If r.Item("Test") = 2 Then
                s(count) = r.Item("Foundation Model No_")
                count += 1
            End If
        Next

        Return Join(s, ",")


    End Function

    Public Function ProtoList() As String
        If Me.IsNew Then
            Return ""
        End If
        If Me.m_dt.Rows.Count = 0 Then
            Return ""
        End If
        '**********jel 09.19.07 added various variables and calcs to allow for two prototypes on a singles line in the array*********
        Dim num As Decimal
        Dim even As Boolean = False
        Dim s(Me.m_dt.Rows.Count - 1) As String
        Dim count As Integer = 0
        Dim countr As Integer = 1
        For Each r As DataRow In Me.m_dt.Rows
            If r.Item("Test") = 2 Then
                num = countr / 2
                If Math.Ceiling(num) <> num Then
                    even = False
                Else
                    even = True
                End If

                If even = False Then
                    s(count) = r.Item("Prototype No_")
                Else
                    s(count) = s(count) & ", " & r.Item("Prototype No_")
                    count += 1
                End If
                countr += 1
            End If
        Next
        ' s(0) = "Prototype ID: " & s(0)
        Return Join(s, ControlChars.CrLf)


    End Function

End Class