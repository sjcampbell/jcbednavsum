Imports CRHelper


Public Class clsGenerateOthers
    Public ordernum As String
    Public linenum As String
    Public instance As String
    Public pd As PrintDialog
    Public result As DialogResult
    Public typeother As Boolean

    'Sub New(ByVal OrderNumber As String, ByVal LineNumber As String, ByVal instancenum As String, ByVal pdialog As PrintDialog, ByVal presult As DialogResult, ByVal other As Boolean)
    Sub New(ByVal pdialog As PrintDialog, ByVal presult As DialogResult)
        'ordernum = OrderNumber
        'linenum = LineNumber
        'instance = instancenum
        pd = pdialog
        result = presult
        'typeother = other
    End Sub

    Function lawtag(ByRef r As DataRow) As String
        Return r.Item("Tag Number")
    End Function

    Function fixnulldec(ByVal o As Object) As Decimal
        If IsDBNull(o) Then
            Return 0
        End If
        Return o
    End Function

    Function FindSubAssemblies(ByVal counter As Integer, ByVal item As String, ByRef arl As ArrayList) As ArrayList
        'WARNING: Recursion below
        'Returns an array list of clsBOMLine objects
        If counter > 5 Then
            'Sanity check
            MsgBox("Too many levels of sub assemblies.")
            Return arl
        End If
        Dim s As String = Module1.CompanyName()
        Dim sl2 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '2' ")
        If Not sl2.IsNew Then
            For Each r As DataRow In sl2.m_dt.Rows
                FindSubAssemblies(counter + 1, sl2.m_row.Item("No_"), arl)
            Next
        End If

        Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'  and Item.[Piece Rate Ticket] = '2'")
        'Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'")
        'If Not IsDBNull(sl3.m_row.Item("No_")) Then
        '    Dim i As New clsItem(sl3.m_row.Item("No_"))
        '    If IsDBNull(i.m_row.Item("Piece Rate Ticket")) Then
        '        i.m_row.Ite""m("Piece Rate Ticket") = "0"
        '    End If
        If (Not sl3.IsNew) Then
            Module1.otherarl.Add(sl3)
        End If


    End Function



    Public Sub PrintIt(ByVal arl As ArrayList)
        'Prints one page per item that contains all the lawtags

        Dim sl2 As New clsTearOff2(" [" & Module1.CompanyName & "$Sales Line].[Document No_]='0'")
        'Build string of order numbers e.g. in('10013','76545')
        Dim instr As String = Join(arl.ToArray, ",")
        Dim l As New clsOtherLawtags(instr)
        'Build in clause of law tag numbers
        Dim ltArl As New ArrayList
        For Each rl As DataRow In l.m_dt.Rows
            ltArl.Add("'" & rl.Item("Tag Number") & "'")
        Next
        Dim ltstr As String = Join(ltArl.ToArray, ",")
        'MsgBox("3")
        Dim i As New clsOtherItem("")

        For Each ri As DataRow In i.m_dt.Rows
            Dim itemno As String = ri.Item("No_")
            sl2.m_dt.Rows.Clear()
            'get a list of all items that are flagged as 'other' (piece rate = 2) and those that are in the 
            ' list of lawtags (ltstr) then print a page of labels 
            Dim ol As New clsOtherList(" ItemNo = '" & itemno & "' and LawTagNum in(" & ltstr & ")")
            '    For Each ds As clsBOMLineSelect In Module1.otherarl
            If Not ol.IsNew Then
                For Each r As DataRow In ol.m_dt.Rows
                    'r.Item("Printed") = "Y"
                    Dim str As String = "update [" & Module1.CompanyName & "$TearOffTag] set [Printed] = 'Y' where [id] = '" & r.Item("id") & "'"
                    Dim con As SqlClient.SqlConnection = ol.m_con
                    con.Open()
                    Dim cmd As New SqlClient.SqlCommand(str, con)
                    cmd.ExecuteNonQuery()
                    con.Close()
                    ' MsgBox("4 1" & r.Item("LawTagNum") & "xyz")
                    Dim lt As New clsLawTag(" [Tag Number] = '" & r.Item("LawTagNum") & "'")
                    'Dim lt As New clsLawTag(" [Tag Number] = '0'")
                    ' MsgBox("4 1 1 ")

                    Dim it As New clsItem(itemno)
                    Dim drow2 As DataRow = sl2.m_dt.NewRow
                    '            For Each col As DataColumn In sl2.m_dt.Columns
                    '                drow2.Item(col.ColumnName) = sl2.m_row.Item(col.ColumnName)
                    '            Next
                    ' MsgBox("4 1 2 ")
                    drow2.Item("Labor Cost") = r.Item("Minutes")
                    drow2.Item("BDescription") = it.m_row.Item("Description") & " - " & itemno
                    drow2.Item("Item") = r.Item("BOMNo")
                    drow2.Item("ProdGroup") = it.m_row.Item("Product Group Code")
                    Dim ord As New clsOrderHeader("HeaderNum = '" & lt.m_row.Item("Document No_") & "'")
                    ' MsgBox("....4 1 3 " & "HeaderNum = '" & lt.m_row.Item("Document No_") & "'")
                    ' MsgBox("post " & CStr(ord.m_row.Item("Production Date")))
                    If IsDBNull(ord.m_row.Item("Production Date")) Then
                        MsgBox("ISNULL")
                    End If
                    'MsgBox(r.Item("id") & " - Pre - " & lt.m_row.Item("Document No_"))
                    drow2.Item("ProdDate") = ord.m_row.Item("Production Date")
                    '  MsgBox("1-----------")
                    drow2.Item("Route") = ord.m_row.Item("Route")
                    
                    drow2.Item("ScanField") = "*" & r.Item("id") & "*"
                   
                    If Not it.m_row.Item("Description") = "Fringe & Overhead" Then
                        sl2.m_dt.Rows.Add(drow2)
                    End If

                Next
            End If
            
            Dim crpt As New CrystalReport5
       

            If Not ol.IsNew Then
                'ol.update()
                clsPrintOther.Print(crpt, sl2.m_dt.DataSet, pd, result)
            End If
           

        Next
        If testmode() = 1 Then
            MsgBox(instr & " --- " & ltstr)
        End If
    End Sub







End Class
