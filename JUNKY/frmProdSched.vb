Imports CRHelper
Public Class frmProdSched
    Inherits System.Windows.Forms.Form
    Public str(5) As String

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.DataGrid1 = New System.Windows.Forms.DataGrid
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(40, 16)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(96, 23)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Display report"
        '
        'DataGrid1
        '
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(8, 48)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.Size = New System.Drawing.Size(1136, 328)
        Me.DataGrid1.TabIndex = 1
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(400, 16)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(240, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(136, 23)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Please select a start day"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem10, Me.MenuItem8})
        Me.MenuItem1.Text = "Tools"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Route summary"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "Production summary"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 2
        Me.MenuItem4.Text = "Route details"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "Production details"
        '
        'MenuItem6
        '
        Me.MenuItem6.Enabled = False
        Me.MenuItem6.Index = 4
        Me.MenuItem6.Text = "Weekly report"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 5
        Me.MenuItem7.Text = "Law tags"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 6
        Me.MenuItem10.Text = "-"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 7
        Me.MenuItem8.Text = "Exit application"
        '
        'frmProdSched
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(1168, 446)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.DataGrid1)
        Me.Controls.Add(Me.Button1)
        Me.Menu = Me.MainMenu1
        Me.Name = "frmProdSched"
        Me.Text = "Production Schedule"
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub frmProdSched_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim startd As Date = Me.DateTimePicker1.Value
        str(0) = "Uns"
        str(1) = "Mon"
        str(2) = "Tue"
        str(3) = "Wed"
        str(4) = "Thur"
        str(5) = "Fri"
        'str(0) = startd.ToShortDateString
        'str(1) = startd.AddDays(1).ToShortDateString
        'str(2) = "Tue"
        'str(3) = "Wed"
        'str(4) = "Thur"
        'str(5) = "Fri"
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim startd As Date = Date.Parse(Me.DateTimePicker1.Value.ToShortDateString)

        Dim dow As Integer = startd.DayOfWeek()
        If dow <> 1 Then
            'MsgBox("Please select a Monday as the start of week date")
            'Return
        End If
        Dim endd As Date = startd.AddDays(6)
        Dim ps As New clsProdSched(Module1.CompanyName, startd.ToShortDateString, endd.ToShortDateString)

        Dim ds As New DataSet
        Dim dt As New DataTable("pabd")
        dt.Columns.Add("Route", GetType(String))
        dt.Columns.Add("Dealer", GetType(String))
        dt.Columns.Add("Method", GetType(String))
        dt.Columns.Add("Order", GetType(String))
        dt.Columns.Add("City", GetType(String))
        dt.Columns.Add("Credit", GetType(String))
        dt.Columns.Add("Due", GetType(String))
        For x As Integer = 0 To 5
            'str(x) = startd.AddDays(x).ToShortDateString
            dt.Columns.Add(str(x) & " Qty", GetType(Decimal))
            dt.Columns.Add(str(x) & " Done", GetType(Decimal))
            dt.Columns.Add(str(x) & " Left", GetType(Decimal))
        Next
        'MsgBox(Date.Today.AddDays(3).DayOfWeek)


        ds.Tables.Add(dt)
        If ps.IsNew Then
            MsgBox("No matching records found")
        Else

            For Each r As DataRow In ps.m_dt.Rows
                Dim nr As DataRow = dt.NewRow
                For Each c As DataColumn In dt.Columns
                    nr.Item(c.ColumnName) = "0"
                Next
                nr.Item("Route") = r.Item("Ship-to Route Code")
                nr.Item("Dealer") = r.Item("Name")
                nr.Item("Method") = r.Item("Method")
                If r.Item("Method") = "REGULAR" Then
                    nr.Item("Method") = ""
                End If
                nr.Item("Order") = r.Item("No_")
                nr.Item("City") = r.Item("City")
                nr.Item("Credit") = r.Item("CChk")
                Dim DueDate As Date = Date.Parse(r.Item("Due"))
                nr.Item("Due") = DueDate.ToShortDateString
                If (r.Item("Due") = "1/1/2001") And (r.Item("Reviewed") = 1) Then
                    nr.Item("Due") = "reviewed"
                End If
                Dim ItemDate As Date = Date.Parse(r.Item("Production Date"))
                'Dim ItemDow As Integer = ItemDate.DayOfWeek
                Dim ItemDow As Integer = ItemDate.Subtract(startd).Days
                If startd.DayOfWeek = DayOfWeek.Tuesday And ItemDow > 3 Then
                    ItemDow -= 2
                End If
                If startd.DayOfWeek = DayOfWeek.Wednesday And ItemDow > 2 Then
                    ItemDow -= 2
                End If
                If startd.DayOfWeek = DayOfWeek.Thursday And ItemDow > 1 Then
                    ItemDow -= 2
                End If
                If startd.DayOfWeek = DayOfWeek.Friday And ItemDow > 0 Then
                    ItemDow -= 2
                End If

                ItemDow += 1
                Dim i As Integer = Date.Parse("1/1/2001").Subtract(Date.Parse("2/2/2001")).Days

                If ItemDate.Year < 2002 Then
                    ItemDow = 0
                End If
                If CInt(r.Item("QtyProd")) < 0 Then
                    r.Item("QtyProd") = 0
                End If
                nr.Item(str(ItemDow) & " Qty") = CStr(CInt(r.Item("Qty")))
                nr.Item(str(ItemDow) & " Done") = CStr(CInt(r.Item("QtyProd")))
                nr.Item(str(ItemDow) & " Left") = CStr(CInt(r.Item("Qty")) - CInt(r.Item("QtyProd")))
                If Not ((ItemDate.DayOfWeek = DayOfWeek.Saturday) Or (ItemDate.DayOfWeek = DayOfWeek.Sunday)) Then
                    dt.Rows.Add(nr)
                End If
            Next
        End If
        Me.DataGrid1.DataSource = ps.m_dt

        ds.WriteXmlSchema("ProdSched2.xml")

        Dim crpt As New crptProductionSchedule


        Dim targetform As New frmCrptProdSched
        targetform.crpt = New crptProductionSchedule
        'targetform.filename = "commission.pdf"
        Dim cr As New CRH(targetform.crpt)
        'Dim sp2 As New clsSalesperson(Me.ListBox1.SelectedValue)
        cr.SetText("TextCompany", Module1.CompanyName)
        cr.SetText("TextWeek", startd.ToShortDateString) ' & " - " & endd.ToShortDateString)
        cr.SetText("Text13", startd.AddDays(calcday(startd, 0)).ToShortDateString)
        cr.SetText("Text20", startd.AddDays(calcday(startd, 1)).ToShortDateString)
        cr.SetText("Text27", startd.AddDays(calcday(startd, 2)).ToShortDateString)
        cr.SetText("Text34", startd.AddDays(calcday(startd, 3)).ToShortDateString)
        cr.SetText("Text35", startd.AddDays(calcday(startd, 4)).ToShortDateString)

        cr.SetText("Text18", startd.AddDays(calcday(startd, 0)).DayOfWeek.ToString)
        cr.SetText("Text19", startd.AddDays(calcday(startd, 1)).DayOfWeek.ToString)
        cr.SetText("Text24", startd.AddDays(calcday(startd, 2)).DayOfWeek.ToString)
        cr.SetText("Text25", startd.AddDays(calcday(startd, 3)).DayOfWeek.ToString)
        cr.SetText("Text26", startd.AddDays(calcday(startd, 4)).DayOfWeek.ToString)


        targetform.ds = ds

        targetform.ShowDialog()


    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        Dim f As New frmHoursSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim f As New RouteSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        Dim f As New Form1
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem5.Click
        Dim f As New Form3
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem7.Click
        Dim f As New Form2
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim f As New frmAddRoute
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem8.Click
        Application.Exit()
    End Sub


    Public Function calcday(ByVal sday As Date, ByVal offset As Integer) As Integer
        Dim dow As Integer = sday.DayOfWeek
        dow += offset
        If dow > 5 Then
            Return (offset + 2)
        End If
 
        Return offset

    End Function
End Class
