'NOT USED - Asked for early on but it turned out to be a bad idea

Public Class frmAddRoute
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents txtRouteCode As System.Windows.Forms.TextBox
    Friend WithEvents txtSequence As System.Windows.Forms.TextBox
    Friend WithEvents txtColor As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu
        Me.MenuItem1 = New System.Windows.Forms.MenuItem
        Me.MenuItem2 = New System.Windows.Forms.MenuItem
        Me.MenuItem3 = New System.Windows.Forms.MenuItem
        Me.MenuItem4 = New System.Windows.Forms.MenuItem
        Me.MenuItem5 = New System.Windows.Forms.MenuItem
        Me.MenuItem6 = New System.Windows.Forms.MenuItem
        Me.MenuItem7 = New System.Windows.Forms.MenuItem
        Me.MenuItem9 = New System.Windows.Forms.MenuItem
        Me.MenuItem10 = New System.Windows.Forms.MenuItem
        Me.MenuItem8 = New System.Windows.Forms.MenuItem
        Me.txtRouteCode = New System.Windows.Forms.TextBox
        Me.txtSequence = New System.Windows.Forms.TextBox
        Me.txtColor = New System.Windows.Forms.TextBox
        Me.ListBox1 = New System.Windows.Forms.ListBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem9, Me.MenuItem10, Me.MenuItem8})
        Me.MenuItem1.Text = "Tools"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Route summary"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "Production summary"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 2
        Me.MenuItem4.Text = "Route details"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "Production details"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 4
        Me.MenuItem6.Text = "Weekly report"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 5
        Me.MenuItem7.Text = "Law tags"
        '
        'MenuItem9
        '
        Me.MenuItem9.Enabled = False
        Me.MenuItem9.Index = 6
        Me.MenuItem9.Text = "Add new route"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 7
        Me.MenuItem10.Text = "-"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 8
        Me.MenuItem8.Text = "Exit application"
        '
        'txtRouteCode
        '
        Me.txtRouteCode.Location = New System.Drawing.Point(40, 40)
        Me.txtRouteCode.Name = "txtRouteCode"
        Me.txtRouteCode.TabIndex = 0
        Me.txtRouteCode.Text = ""
        '
        'txtSequence
        '
        Me.txtSequence.Location = New System.Drawing.Point(256, 40)
        Me.txtSequence.Name = "txtSequence"
        Me.txtSequence.TabIndex = 1
        Me.txtSequence.Text = ""
        '
        'txtColor
        '
        Me.txtColor.Location = New System.Drawing.Point(40, 120)
        Me.txtColor.Name = "txtColor"
        Me.txtColor.TabIndex = 2
        Me.txtColor.Text = ""
        '
        'ListBox1
        '
        Me.ListBox1.Items.AddRange(New Object() {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"})
        Me.ListBox1.Location = New System.Drawing.Point(248, 120)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(120, 82)
        Me.ListBox1.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(40, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Route Code"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(256, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Sequence"
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(40, 96)
        Me.Label3.Name = "Label3"
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Color"
        '
        'Label4
        '
        Me.Label4.Location = New System.Drawing.Point(256, 96)
        Me.Label4.Name = "Label4"
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Day of Week"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(48, 176)
        Me.Button1.Name = "Button1"
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Add"
        '
        'frmAddRoute
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(448, 266)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.txtColor)
        Me.Controls.Add(Me.txtSequence)
        Me.Controls.Add(Me.txtRouteCode)
        Me.Menu = Me.MainMenu1
        Me.Name = "frmAddRoute"
        Me.Text = "Add Route"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Public dh As Hashtable
    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        Dim f As New frmHoursSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim f As New RouteSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        Dim f As New Form1
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem5.Click
        Dim f As New Form3
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem6.Click
        Dim f As New frmProdSched
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem7.Click
        Dim f As New Form2
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem8.Click
        Application.Exit()
    End Sub

    Private Sub frmAddRoute_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim dowhash As New Hashtable
        dowhash.Add("Sunday", 7)
        dowhash.Add("Monday", 1)
        dowhash.Add("Tuesday", 2)
        dowhash.Add("Wednesday", 3)
        dowhash.Add("Thursday", 4)
        dowhash.Add("Friday", 5)
        dowhash.Add("Saturday", 6)
        dh = dowhash
        'Me.ListBox1.
        'me.ListBox1.
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim rt As New clsRoute(Me.txtRouteCode.Text)
        'rt.m_row.Item("timestamp") = ""
        rt.m_row.Item("Route Code") = Me.txtRouteCode.Text
        rt.m_row.Item("Sequence") = Me.txtSequence.Text
        rt.m_row.Item("Color Code") = Me.txtColor.Text
        rt.m_row.Item("Day of Week") = Me.ListBox1.SelectedIndex + 1
        'Try
        '    rt.update()
        'Catch ex As Exception
        'End Try
        If Me.txtColor.Text = "" Then
            MsgBox("Please choose a color")
            Return
        End If
        If Me.txtRouteCode.Text = "" Then
            MsgBox("Please choose a Route Code")
            Return
        End If
        If Me.txtSequence.Text = "" Then
            MsgBox("Please choose a Sequence")
            Return
        End If
        If Me.ListBox1.SelectedIndex = -1 Then
            MsgBox("Please choose a day of week")
            Return
        End If


        Dim con As SqlClient.SqlConnection = rt.m_con
        If rt.IsNew Then
            con.Open()
            Dim str As String = "insert into [" & Module1.CompanyName() & "$Ship-To Route Code]([Route Code],[Day of Week],[Sequence],[Color Code]) values('" & Me.txtRouteCode.Text & "','" & Me.ListBox1.SelectedIndex - 1 & "','" & Me.txtSequence.Text & "','" & Me.txtColor.Text & "')"
            Dim cmd As New SqlClient.SqlCommand(str, con)

            Try
                cmd.ExecuteNonQuery()
                MsgBox("Route " & Me.txtRouteCode.Text & " added")
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            con.Close()
        End If
    End Sub
End Class
