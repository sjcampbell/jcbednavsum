Imports DbMaster

Public Class clsSR2

    Inherits clSqlMaster

    Sub New(ByVal arg As String)
        'Passes the table name, and key field to the parent class

        MyBase.New()
        Dim s As String = CompanyName()
        'Me.m_con.Open()
  

        Dim tablename As String = Module1.CompanyName
        Dim Str As String = ""
        Str = Str & " SELECT dbo.[" & tablename & "$Sales Header].No_ AS HeaderID, dbo.[" & tablename & "$Sales Line].[Document No_] AS OrderNum, "
        Str = Str & " dbo.[" & tablename & "$Sales Line].[Line No_] AS LineNum, dbo.[" & tablename & "$Sales Line].No_ AS Item, "
        Str = Str & " dbo.[" & tablename & "$Customer].Name, dbo.[" & tablename & "$Sales Line].Description, "
        Str = Str & " ISNULL(dbo.[" & tablename & "$Sales Line].[Qty_ to Ship], 0) AS QtyToShip, "
        Str = Str & " dbo.[" & tablename & "$Sales Line].[Outstanding Quantity] AS QtyRemaining, dbo.[" & tablename & "$Item].Length, dbo.[" & tablename & "$Item].Width, dbo.[" & tablename & "$Item].Height, "
        Str = Str & " dbo.[" & tablename & "$Item].[Compression Factor] AS Compress, dbo.[" & tablename & "$Sales Line].Description AS ManHours, "
        Str = Str & " dbo.[" & tablename & "$Item].[Product Group Code] AS ProdGroup, "
        Str = Str & " dbo.[" & tablename & "$Sales Header].[Production Date] AS ProdDate, "
        Str = Str & " dbo.[" & tablename & "$Sales Header].[Requested Delivery Date] AS [Shipment Date], "
        Str = Str & " dbo.[" & tablename & "$Sales Line].Description AS Cubes, dbo.[" & tablename & "$Sales Line].Description AS Reviewed, "
        Str = Str & " dbo.[" & tablename & "$Sales Header].[Ship-to Route Code] AS Route, dbo.[" & tablename & "$Item].Style, dbo.[" & tablename & "$Item].Size, dbo.[" & tablename & "$Item].Filler, "
        Str = Str & " dbo.[" & tablename & "$Item].[Item Category Code] AS Type, '''' AS LawTag, dbo.[" & tablename & "$BOM Line].Description AS BDescription, "
        Str = Str & " dbo.[" & tablename & "$BOM Line].Type AS BType, dbo.[" & tablename & "$BOM Line].[Labor Cost],'''' AS ScanField"
        Str = Str & " FROM dbo.[" & tablename & "$Sales Line] LEFT OUTER JOIN  dbo.[" & tablename & "$Item] ON dbo.[" & tablename & "$Sales Line].No_ = dbo.[" & tablename & "$Item].No_ "
        Str = Str & " INNER JOIN dbo.[" & tablename & "$BOM Line] ON "
        Str = Str & " dbo.[" & tablename & "$Item].[BOM No_] = dbo.[" & tablename & "$BOM Line].[BOM No_] "
        Str = Str & "LEFT OUTER JOIN"
        Str = Str & " dbo.[" & tablename & "$Customer] ON "
        Str = Str & " dbo.[" & tablename & "$Sales Line].[Sell-to Customer No_] = dbo.[" & tablename & "$Customer].No_ LEFT OUTER JOIN"
        Str = Str & " dbo.[" & tablename & "$Sales Header] ON "
        Str = Str & " dbo.[" & tablename & "$Sales Line].[Document No_] = dbo.[" & tablename & "$Sales Header].No_"
        If arg <> "" Then
            Str = Str & " where " & arg
        End If
        Str = Str & " "
        '  MsgBox(Str)
        Me.SelectPhrase(Str, "PViewTearOffs2", "")



    End Sub
End Class

