Imports DbMaster
Public Class clsProdSched

    Inherits clSqlMaster

    Sub New(ByVal tablename As String, ByVal startdate As String, ByVal enddate As String)
        'Passes the table name, and key field to the parent class

        MyBase.New()
        'Me.m_con.Open()
        Dim command As SqlClient.SqlCommand = _
             New SqlClient.SqlCommand("spProductionSchedule", Me.m_con)
        command.Parameters.Add("@tablename", tablename)

        command.Parameters.Add("@startdate", startdate)
        command.Parameters.Add("@enddate", enddate)


        command.CommandType = CommandType.StoredProcedure
        Me.StoredProcedure(command)
    End Sub
End Class
