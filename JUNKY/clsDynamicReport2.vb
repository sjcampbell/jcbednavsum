Imports DbMaster
Public Class clsDynamicReport2

    Inherits clSqlMaster

    Sub New(ByVal tablename As String, ByVal arg As String, ByVal startdate As String, ByVal enddate As String, ByVal orderlist As String)
        'Passes the table name, and key field to the parent class

        MyBase.New()
        'Me.m_con.Open()
        '''Dim command As SqlClient.SqlCommand = _
        '''     New SqlClient.SqlCommand("spSalespersonDealer", Me.m_con)
        '''command.Parameters.Add("@tablename", tablename)
        '''command.Parameters.Add("@salesperson", salesperson)

        '''command.Parameters.Add("@startdate", startdate)
        '''command.Parameters.Add("@enddate", enddate)


        '''command.CommandType = CommandType.StoredProcedure
        ' Me.StoredProcedure(command)
        Dim Str As String = ""
        Str = Str & " SELECT sum(dbo.[" & tablename & "$BOM Explosion].[Quantity Required]) as Qty,"
        Str = Str & " "
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Item No_], "
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Description],"
        Str = Str & "  "
        Str = Str & "  dbo.[" & tablename & "$BOM Explosion].[Ship-to Route Code]"
        Str = Str & " "
        Str = Str & " FROM dbo.[" & tablename & "$BOM Explosion]"
        Str = Str & "  "
        Str = Str & " where (dbo.[" & tablename & "$BOM Explosion].[Production Date] >="
        Str = Str & " '" & startdate & "' and dbo.[" & tablename & "$BOM Explosion].[Production Date] <= '"
        Str = Str & enddate & " ') "
        If Not (arg = "") Then
            Str = Str & " and " & arg
        End If
        Str = Str & " " & orderlist
        Str = Str & " group by dbo.[" & tablename & "$BOM Explosion].[Item No_],dbo.[" & tablename & "$BOM Explosion].[Description],"
        Str = Str & " "
        Str = Str & "  dbo.[" & tablename & "$BOM Explosion].[Ship-to Route Code]"
        Str = Str & " "
        Str = Str & "  order by dbo.[" & tablename & "$BOM Explosion].[Item No_]"



        Me.SelectPhrase(Str, "BOMEX", "")


    End Sub
End Class
