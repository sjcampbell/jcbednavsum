Public Class clsStraightToPrinterDR
    Shared Sub Print(ByRef crpt As Object, ByVal ds As DataSet, ByVal pd As PrintDialog, ByVal result As DialogResult)
        crpt.SetDataSource(ds)



        'Dim pd As New PrintDialog
        'pd.Document = New System.Drawing.Printing.PrintDocument


        'Dim result As DialogResult = pd.ShowDialog()


        Dim Printer As String = pd.Document.PrinterSettings.PrinterName
        Dim Copies As String = pd.Document.PrinterSettings.Copies
        Dim FromPage As Integer = pd.Document.PrinterSettings.FromPage
        Dim ToPage As Integer = pd.Document.PrinterSettings.ToPage

        If (result = DialogResult.OK) Then
            crpt.PrintOptions.PrinterName = Printer
            crpt.PrintToPrinter(Copies, False, FromPage, ToPage)
        End If
    End Sub
End Class
