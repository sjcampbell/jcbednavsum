Imports DbMaster
Public Class clsDynamicAssemblyIS

    Inherits clSqlMaster

    Sub New(ByVal tablename As String, ByVal arg As String, ByVal startdate As String, ByVal enddate As String, ByVal orderlist As String)
        'Passes the table name, and key field to the parent class

        MyBase.New()

        Dim Str As String = ""
        Str = Str & " SELECT sum(dbo.[" & tablename & "$BOM Explosion].[Quantity Required]) as Qty,"
        Str = Str & " "
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Item No_], "
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Line No_], "
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Description],"
        Str = Str & "  dbo.[" & tablename & "$BOM Explosion].[Parent No_],"
        Str = Str & "  dbo.[" & tablename & "$BOM Explosion].[Layer Position],"
        Str = Str & "  dbo.[" & tablename & "$BOM Explosion].[Ship-to Route Code]"
        Str = Str & " "
        Str = Str & " FROM dbo.[" & tablename & "$BOM Explosion]"
        Str = Str & "  "
        Str = Str & " where (dbo.[" & tablename & "$BOM Explosion].[Production Date] >="
        Str = Str & " '" & startdate & "' and dbo.[" & tablename & "$BOM Explosion].[Production Date] <= '"
        Str = Str & enddate & " ') and  "
        Str = Str & "dbo.[" & tablename & "$BOM Explosion].[Foam Encased_Innerspring]='2' "
        If Not (arg = "") Then
            Str = Str & " and " & arg
        End If
        Str = Str & " " & orderlist
        Str = Str & " group by dbo.[" & tablename & "$BOM Explosion].[Ship-to Route Code],dbo.[" & tablename & "$BOM Explosion].[Parent No_],dbo.[" & tablename & "$BOM Explosion].[Item No_],dbo.[" & tablename & "$BOM Explosion].[Description],"
        Str = Str & " dbo.[" & tablename & "$BOM Explosion].[Line No_],dbo.[" & tablename & "$BOM Explosion].[Layer Position] "
        Str = Str & "  "
        Str = Str & " "
        'Str = Str & "  order by dbo.[" & tablename & "$BOM Explosion].[Item No_]"



        Me.SelectPhrase(Str, "spASFE", "")


    End Sub
End Class
