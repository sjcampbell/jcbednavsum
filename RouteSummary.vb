Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Drawing.Imaging
Imports System.Drawing.Text

Public Class RouteSummary
    Inherits System.Windows.Forms.Form
    Public route As String = ""
    Public month As String
    Public day As String
    Public year As String
    Public g As Graphics
    Public intCount As Integer = 0
    Dim c As Integer = 1
    Public arl As New ArrayList
    Public max As Decimal = 0
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents cboStartYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboStartDay As System.Windows.Forms.ComboBox
    Friend WithEvents cboStartMonth As System.Windows.Forms.ComboBox
    Friend WithEvents lblStart As System.Windows.Forms.Label
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents cboEndYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboEndDay As System.Windows.Forms.ComboBox
    Friend WithEvents cboEndMonth As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents Button5 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cboStartYear = New System.Windows.Forms.ComboBox()
        Me.cboStartDay = New System.Windows.Forms.ComboBox()
        Me.cboStartMonth = New System.Windows.Forms.ComboBox()
        Me.lblStart = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.DataGrid1 = New System.Windows.Forms.DataGrid()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.cboEndYear = New System.Windows.Forms.ComboBox()
        Me.cboEndDay = New System.Windows.Forms.ComboBox()
        Me.cboEndMonth = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem10 = New System.Windows.Forms.MenuItem()
        Me.MenuItem8 = New System.Windows.Forms.MenuItem()
        Me.Button5 = New System.Windows.Forms.Button()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cboStartYear
        '
        Me.cboStartYear.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022"})
        Me.cboStartYear.Location = New System.Drawing.Point(192, 63)
        Me.cboStartYear.Name = "cboStartYear"
        Me.cboStartYear.Size = New System.Drawing.Size(77, 24)
        Me.cboStartYear.TabIndex = 22
        '
        'cboStartDay
        '
        Me.cboStartDay.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})
        Me.cboStartDay.Location = New System.Drawing.Point(125, 63)
        Me.cboStartDay.Name = "cboStartDay"
        Me.cboStartDay.Size = New System.Drawing.Size(48, 24)
        Me.cboStartDay.TabIndex = 21
        '
        'cboStartMonth
        '
        Me.cboStartMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cboStartMonth.Location = New System.Drawing.Point(38, 63)
        Me.cboStartMonth.Name = "cboStartMonth"
        Me.cboStartMonth.Size = New System.Drawing.Size(68, 24)
        Me.cboStartMonth.TabIndex = 20
        '
        'lblStart
        '
        Me.lblStart.Location = New System.Drawing.Point(48, 36)
        Me.lblStart.Name = "lblStart"
        Me.lblStart.Size = New System.Drawing.Size(120, 19)
        Me.lblStart.TabIndex = 19
        Me.lblStart.Text = "Select start date:"
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(298, 65)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(124, 26)
        Me.btnSearch.TabIndex = 17
        Me.btnSearch.Text = "Search Records"
        '
        'DataGrid1
        '
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me.DataGrid1.Location = New System.Drawing.Point(29, 194)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.PreferredColumnWidth = 105
        Me.DataGrid1.RowHeaderWidth = 95
        Me.DataGrid1.Size = New System.Drawing.Size(681, 341)
        Me.DataGrid1.TabIndex = 16
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(298, 18)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(124, 27)
        Me.Button1.TabIndex = 23
        Me.Button1.Text = "Route Details"
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Location = New System.Drawing.Point(749, 18)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(441, 517)
        Me.Panel1.TabIndex = 24
        '
        'cboEndYear
        '
        Me.cboEndYear.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022"})
        Me.cboEndYear.Location = New System.Drawing.Point(192, 129)
        Me.cboEndYear.Name = "cboEndYear"
        Me.cboEndYear.Size = New System.Drawing.Size(77, 24)
        Me.cboEndYear.TabIndex = 28
        '
        'cboEndDay
        '
        Me.cboEndDay.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})
        Me.cboEndDay.Location = New System.Drawing.Point(125, 129)
        Me.cboEndDay.Name = "cboEndDay"
        Me.cboEndDay.Size = New System.Drawing.Size(48, 24)
        Me.cboEndDay.TabIndex = 27
        '
        'cboEndMonth
        '
        Me.cboEndMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cboEndMonth.Location = New System.Drawing.Point(38, 129)
        Me.cboEndMonth.Name = "cboEndMonth"
        Me.cboEndMonth.Size = New System.Drawing.Size(68, 24)
        Me.cboEndMonth.TabIndex = 26
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(48, 102)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 19)
        Me.Label1.TabIndex = 25
        Me.Label1.Text = "Select end date:"
        '
        'CheckBox1
        '
        Me.CheckBox1.Location = New System.Drawing.Point(528, 148)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(192, 27)
        Me.CheckBox1.TabIndex = 29
        Me.CheckBox1.Text = "Exclude reviewed orders"
        Me.CheckBox1.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(451, 18)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(90, 56)
        Me.Button2.TabIndex = 30
        Me.Button2.Text = "Print Lawtags"
        Me.Button2.Visible = False
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(595, 18)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(90, 56)
        Me.Button3.TabIndex = 31
        Me.Button3.Text = "Production Summary"
        Me.Button3.Visible = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(528, 83)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(154, 46)
        Me.Button4.TabIndex = 32
        Me.Button4.Text = "Weekly Prod Sched Report"
        Me.Button4.Visible = False
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem10, Me.MenuItem8})
        Me.MenuItem1.Text = "Tools"
        '
        'MenuItem2
        '
        Me.MenuItem2.Enabled = False
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Route summary"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "Production summary"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 2
        Me.MenuItem4.Text = "Route details"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "Production details"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 4
        Me.MenuItem6.Text = "Weekly report"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 5
        Me.MenuItem7.Text = "Law tags"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 6
        Me.MenuItem10.Text = "-"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 7
        Me.MenuItem8.Text = "Exit application"
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(298, 138)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(124, 27)
        Me.Button5.TabIndex = 33
        Me.Button5.Text = "Unscheduled only"
        '
        'RouteSummary
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(6, 15)
        Me.ClientSize = New System.Drawing.Size(1024, 482)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.cboEndYear)
        Me.Controls.Add(Me.cboEndDay)
        Me.Controls.Add(Me.cboEndMonth)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.cboStartYear)
        Me.Controls.Add(Me.cboStartDay)
        Me.Controls.Add(Me.cboStartMonth)
        Me.Controls.Add(Me.lblStart)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.DataGrid1)
        Me.Menu = Me.MainMenu1
        Me.Name = "RouteSummary"
        Me.Text = "RouteSummary"
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub RouteSummary_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.cboStartYear.DataSource = Module1.DropBoxYears
        Me.cboEndYear.DataSource = Module1.DropBoxYears
        Me.Button1.Enabled = False
        Dim prestate As Boolean = gRouteDatasetFresh
        If gstartday = "" Then
            Me.cboStartDay.Text = Date.Now.Day
        Else
            Me.cboStartDay.Text = gstartday
        End If
        If gstartmonth = "" Then
            Me.cboStartMonth.Text = Date.Now.Month
        Else
            Me.cboStartMonth.Text = gstartmonth
        End If
        If gstartyear = "" Then
            Me.cboStartYear.Text = Date.Now.Year
        Else
            Me.cboStartYear.Text = gstartyear
        End If
        Dim ed As Date = Date.Now.AddDays(7)
        If gendday = "" Then
            Me.cboEndDay.Text = ed.Day
        Else
            Me.cboEndDay.Text = gendday
        End If
        If gendmonth = "" Then
            Me.cboEndMonth.Text = ed.Month
        Else
            Me.cboEndMonth.Text = gendmonth
        End If
        If gendyear = "" Then
            Me.cboEndYear.Text = ed.Year
        Else
            Me.cboEndYear.Text = gendyear
        End If
        gRouteDatasetFresh = prestate
        If gLoadRoutes Then
            Me.btnSearch.PerformClick()
        End If
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.Button1.Enabled = True

        Dim ds As New DataSet
        gRouteDatasetFresh = False
        If Not gRouteDatasetFresh = True Then
            Dim strStartDate As String
            Dim strEndDate As String
            Dim rvstr As String = ""
            If Me.CheckBox1.Checked Then
                rvstr = " and Reviewed='1'"
            End If



            Dim strNumRows As String
            c = 1
            intCount = 0
            arl.Clear()
            max = 0
            strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
            strEndDate = cboEndMonth.Text & "/" & cboEndDay.Text & "/" & cboEndYear.Text
            Dim d1s() As String = strStartDate.Split("/")
            Dim dts As New Date(d1s(2), d1s(0), d1s(1))
            Dim d2s() As String = strEndDate.Split("/")
            Dim dte As New Date(d2s(2), d2s(0), d2s(1))
            Dim dtc As New Date(d1s(2), d1s(0), d1s(1))


            Dim dt As New DataTable("Routes")
            dt.Columns.Add("Route", GetType(String))
            dt.Columns.Add("Cubes", GetType(Decimal))
            dt.Columns.Add("TotalPcs", GetType(Decimal))
            dt.Columns.Add("Ship date", GetType(DateTime))
            ds.Tables.Add(dt)

            Dim pg As New clsSalesHeader("not (No_ = '')")
            'Dim pg As New clsOrder("")
            Dim dt3 As New DataTable
            dt3 = pg.distinct("[Ship-to Route Code]")
            Dim o As New clsOrderSum("([Requested Delivery Date] >= '" & strStartDate & "' and [Requested Delivery Date] <= '" & strEndDate & "') or [Requested Delivery Date] <= '1/1/2001'" & rvstr)
            For Each r As DataRow In o.m_dt.Rows
                Dim sr As DataRow = dt.NewRow
                sr.Item("Route") = r.Item("Ship-to Route Code")
                Dim z As String = r.Item("Ship-to Route Code")
                sr.Item("Cubes") = Decimal.Round(CDec(r.Item("Cubes")), 2) ' * CDec(r.Item("Pcs")), 2)
                sr.Item("TotalPcs") = Decimal.Round(r.Item("Pcs"), 0)
                sr.Item("Ship Date") = r.Item("Requested Delivery Date")
                'If CDec(sr.Item("Cubes")) > 0 The
                If CDec(r.Item("Cubes")) > max Then
                    max = CDec(r.Item("Cubes"))
                End If
                dt.Rows.Add(sr)
                Dim s As String() = {r.Item("Ship-to Route Code") & " - " & r.Item("Requested Delivery Date"), sr.Item("Cubes")}
                arl.Add(s)
                intCount += 1
                'End If
            Next
            'Dim sr As DataRow = dt.NewRow
            'sr.Item("Route") = "One"
            'sr.Item("Cubes") = 12.5
            'dt.Rows.Add(sr)
            'While (dtc <= dte)
            '    Dim strCurrentDate As String = dtc.Month & "/" & dtc.Day & "/" & dtc.Year
            '    Dim h As New Hashtable
            '    For Each r As DataRow In pg.m_dt.Rows
            '        If Not (IsDBNull(r.Item("Ship-to Route Code"))) Then
            '            If Not (h.Contains(r.Item("Ship-to Route Code"))) Then
            '                h.Add(r.Item("Ship-to Route Code"), 1)

            '                Dim decTotal As Decimal = 0
            '                Dim decTotalpcs As Decimal = 0
            '                Dim o As New clsOrder("[Shipment Date] = '" & strCurrentDate & "'   AND Route ='" & r.Item("Ship-to Route Code") & "'" & rvstr)
            '                'Dim o As New clsOrder("[Sell-to Customer No_] = '" & Me.ListBox1.SelectedValue & "'")

            '                For Each r2 As DataRow In o.m_dt.Rows
            '                    If Not IsDBNull(o.m_row.Item("QtyToShip")) Then
            '                        decTotalpcs += CDec(o.m_row.Item("QtyToShip"))
            '                        If IsDBNull(r2.Item("Compress")) Then
            '                            r2.Item("Compress") = 0
            '                        End If
            '                        If IsDBNull(r2.Item("Width")) Then
            '                            r2.Item("Width") = 0
            '                        End If
            '                        If IsDBNull(r2.Item("Length")) Then
            '                            r2.Item("Length") = 0
            '                        End If
            '                        If IsDBNull(r2.Item("Height")) Then
            '                            r2.Item("Height") = 0
            '                        End If
            '                        If r2.Item("Compress") = 0 Then
            '                            r2.Item("Compress") = 1
            '                        End If
            '                        'r2.Item("Cubes") = CDec(r2.Item("QtyToShip")) * ((CDec(r2.Item("Length")) / 12) * (CDec(r2.Item("Width")) / 12) * (CDec(r2.Item("Height") * CDec(r2.Item("Compress"))) / 12))
            '                        decTotal += CDec(r2.Item("Cubes"))
            '                        If CDec(r2.Item("Cubes")) > max Then
            '                            max = CDec(r2.Item("Cubes"))
            '                        End If
            '                    End If

            '                Next

            '                Dim sr As DataRow = dt.NewRow
            '                sr.Item("Route") = r.Item("Ship-to Route Code")
            '                Dim z As String = r.Item("Ship-to Route Code")
            '                sr.Item("Cubes") = Math.Round(decTotal, 1)
            '                sr.Item("TotalPcs") = Math.Round(decTotalpcs, 1)
            '                sr.Item("Ship Date") = strCurrentDate
            '                If CDec(sr.Item("Cubes")) > 0 Then
            '                    dt.Rows.Add(sr)
            '                    Dim s As String() = {r.Item("Ship-to Route Code") & " - " & strCurrentDate, CStr(decTotal)}
            '                    arl.Add(s)
            '                    intCount += 1
            '                End If
            '            End If
            '        End If
            '    Next
            '    dtc = dtc.AddDays(1)
            'End While
            Me.intCount = dt.Rows.Count

            gRouteDataset = dt.DataSet
            gRouteDatasetFresh = True
        Else
            ds = gRouteDataset
            Me.intCount = ds.Tables(0).Rows.Count
        End If
        Me.DataGrid1.DataSource = ds.Tables(0)
        If Not (rendmonth = "" Or rendyear = "" Or rendday = "") Then
            Dim mydate As Date = Date.Parse(rendmonth & "/" & rendday & "/" & rendyear)
            For x As Integer = 0 To (Me.intCount - 1)
                If Me.DataGrid1.Item(x, 3) = mydate And Me.DataGrid1.Item(x, 0) = groute Then
                    'Me.DataGrid1.CurrentRowIndex = x
                    Me.DataGrid1.Select(x)

                    'Me.DataGrid1.Item(x, 3).Focus()
                    'Me.DataGrid1.
                End If
            Next
        End If
        c += 1
        Me.Panel1.Invalidate()
        '   Me.Panel1_Paint(sender, e)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        month = Me.cboStartMonth.Text
        day = Me.cboStartDay.Text
        year = Me.cboStartYear.Text
        For x As Integer = 0 To (Me.intCount - 1)
            'Dim i As Integer = Me.DataGrid1.CurrentRowIndex()

            If x = 0 Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                Me.route = s
            End If
            If Me.DataGrid1.IsSelected(x) Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                Me.route = s
                Dim d As String = CStr(Me.DataGrid1.Item(x, 3))
                Dim da() As String = d.Split("/")
                month = da(0)
                day = da(1)
                year = da(2)

            End If
        Next
        gendday = Me.cboEndDay.Text
        gendmonth = Me.cboEndMonth.Text
        gendyear = Me.cboEndYear.Text
        gstartday = Me.cboStartDay.Text
        gstartmonth = Me.cboStartMonth.Text
        gstartyear = Me.cboStartYear.Text
        groute = Me.route
        rendday = day
        rendmonth = month
        rendyear = year
        rstartday = day
        rstartmonth = month
        rstartyear = year
        gLoadRoutes = True


        Dim f As New Form1
        Me.Hide()
        f.ShowDialog()
        Me.Close()
    End Sub


    Private Sub Panel1_Paint(ByVal sender As System.Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Panel1.Paint

        Dim font1 As New Font("Arial", 8)
        g = e.Graphics
        g.Clear(System.Drawing.Color.Beige)
        If max > 0 Then
            Dim b As New SolidBrush(Color.FromArgb(128, 128, 128))
            Dim y As Integer = 10
            Dim mult As Decimal = Me.Panel1.Width / max

            For Each s As String() In arl
                Dim w As Integer = CInt(CInt(s(1)) * mult)
                g.FillRectangle(b, 0, y, w, 10)
                g.DrawString(s(0), font1, Brushes.Orange, 0, y - 2)
                y += 18
            Next
        End If
        g.Dispose()

    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Dim f As New Form2
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim f As New frmHoursSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim f As New frmProdSched
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim f As New RouteSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        Dim f As New frmHoursSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        Dim f As New Form1
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem5.Click
        Dim f As New Form3
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem6.Click
        Dim f As New frmProdSched
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem7.Click
        Dim f As New Form2
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim f As New frmAddRoute
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem8.Click
        Application.Exit()
    End Sub

    Private Sub cboStartMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStartMonth.SelectedIndexChanged
        gRouteDatasetFresh = False
    End Sub

    Private Sub cboStartDay_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStartDay.SelectedIndexChanged

        gRouteDatasetFresh = False
    End Sub

    Private Sub cboStartYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboStartYear.SelectedIndexChanged

        gRouteDatasetFresh = False
    End Sub

    Private Sub cboEndMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEndMonth.SelectedIndexChanged

        gRouteDatasetFresh = False
    End Sub

    Private Sub cboEndDay_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEndDay.SelectedIndexChanged

        gRouteDatasetFresh = False
    End Sub

    Private Sub cboEndYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboEndYear.SelectedIndexChanged

        gRouteDatasetFresh = False
    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Me.Button1.Enabled = True

        Dim ds As New DataSet
        gRouteDatasetFresh = False
        If Not gRouteDatasetFresh = True Then
            Dim strStartDate As String
            Dim strEndDate As String
            Dim rvstr As String = ""
            If Me.CheckBox1.Checked Then
                rvstr = " and Reviewed='1'"
            End If



            Dim strNumRows As String
            c = 1
            intCount = 0
            arl.Clear()
            max = 0
            strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
            strEndDate = cboEndMonth.Text & "/" & cboEndDay.Text & "/" & cboEndYear.Text
            Dim d1s() As String = strStartDate.Split("/")
            Dim dts As New Date(d1s(2), d1s(0), d1s(1))
            Dim d2s() As String = strEndDate.Split("/")
            Dim dte As New Date(d2s(2), d2s(0), d2s(1))
            Dim dtc As New Date(d1s(2), d1s(0), d1s(1))


            Dim dt As New DataTable("Routes")
            dt.Columns.Add("Route", GetType(String))
            dt.Columns.Add("Cubes", GetType(Decimal))
            dt.Columns.Add("TotalPcs", GetType(Decimal))
            dt.Columns.Add("Ship date", GetType(DateTime))
            ds.Tables.Add(dt)

            Dim pg As New clsSalesHeader("not (No_ = '')")
            'Dim pg As New clsOrder("")
            Dim dt3 As New DataTable
            dt3 = pg.distinct("[Ship-to Route Code]")
            Dim o As New clsOrderSum("( [Requested Delivery Date] <= '1/1/2001')" & rvstr)
            For Each r As DataRow In o.m_dt.Rows
                Dim sr As DataRow = dt.NewRow
                sr.Item("Route") = r.Item("Ship-to Route Code")
                Dim z As String = r.Item("Ship-to Route Code")
                sr.Item("Cubes") = Decimal.Round(r.Item("Cubes"), 2)
                sr.Item("TotalPcs") = Decimal.Round(r.Item("Pcs"), 0)
                sr.Item("Ship Date") = r.Item("Requested Delivery Date")
                'If CDec(sr.Item("Cubes")) > 0 Then
                If CDec(r.Item("Cubes")) > max Then
                    max = CDec(r.Item("Cubes"))
                End If
                dt.Rows.Add(sr)
                Dim s As String() = {r.Item("Ship-to Route Code") & " - " & r.Item("Requested Delivery Date"), sr.Item("Cubes")}
                arl.Add(s)
                intCount += 1
                'End If
            Next

            Me.intCount = dt.Rows.Count

            gRouteDataset = dt.DataSet
            gRouteDatasetFresh = True
        Else
            ds = gRouteDataset
            Me.intCount = ds.Tables(0).Rows.Count
        End If
        Me.DataGrid1.DataSource = ds.Tables(0)

        c += 1
        Me.Panel1.Invalidate()
        '   Me.Panel1_Paint(sender, e)
    End Sub
End Class
