'This is the form for Route Details



Imports System.Net

Public Class Form1
    Inherits System.Windows.Forms.Form
    Dim intCount As Integer = 0
    Dim switch As Boolean = True
    Dim service As NavSummary.jcbedws.SalesOrder_Service = New jcbedws.SalesOrder_Service()
    Dim serviceline As NavSummary.SalesLineWS.SalesLineforRouteWS_Service = New SalesLineWS.SalesLineforRouteWS_Service()
    Friend WithEvents Button11 As Button
    Dim servicerelreo As NavSummary.RelReo.ReleaseReopenWS = New RelReo.ReleaseReopenWS()

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()
        service.Url = "http://JCBed-2014.JCBed.local:6047/JCBED/WS/JC%20Bedding/Page/SalesOrder"
        serviceline.Url = "http://jcbed-2014.jcbed.local:6047/JCBED/WS/JC%20Bedding/Page/SalesLineforRouteWS"
        servicerelreo.Url = "http://jcbed-2014.jcbed.local:6047/JCBED/WS/JC%20Bedding/Codeunit/ReleaseReopenWS"

        Dim myCredentials As NetworkCredential = New NetworkCredential("jcbed\administrator", "1934parker")
        service.UseDefaultCredentials = False
        service.Credentials = myCredentials
        serviceline.UseDefaultCredentials = False
        serviceline.Credentials = myCredentials
        servicerelreo.UseDefaultCredentials = False
        servicerelreo.Credentials = myCredentials
        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents lblStart As System.Windows.Forms.Label
    Friend WithEvents lblStop As System.Windows.Forms.Label
    Friend WithEvents cboStartYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboStartMonth As System.Windows.Forms.ComboBox
    Friend WithEvents cboStartDay As System.Windows.Forms.ComboBox
    Friend WithEvents cboStopYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboStopDay As System.Windows.Forms.ComboBox
    Friend WithEvents cboStopMonth As System.Windows.Forms.ComboBox
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents lblSum As System.Windows.Forms.Label
    Friend WithEvents lblTotalQuantity As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents DateTimePicker1 As System.Windows.Forms.DateTimePicker
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataGrid1 = New System.Windows.Forms.DataGrid()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.lblStart = New System.Windows.Forms.Label()
        Me.lblStop = New System.Windows.Forms.Label()
        Me.cboStartMonth = New System.Windows.Forms.ComboBox()
        Me.cboStartDay = New System.Windows.Forms.ComboBox()
        Me.cboStartYear = New System.Windows.Forms.ComboBox()
        Me.cboStopYear = New System.Windows.Forms.ComboBox()
        Me.cboStopDay = New System.Windows.Forms.ComboBox()
        Me.cboStopMonth = New System.Windows.Forms.ComboBox()
        Me.lblSum = New System.Windows.Forms.Label()
        Me.lblTotalQuantity = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem9 = New System.Windows.Forms.MenuItem()
        Me.MenuItem10 = New System.Windows.Forms.MenuItem()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button11 = New System.Windows.Forms.Button()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGrid1
        '
        Me.DataGrid1.AlternatingBackColor = System.Drawing.Color.Lavender
        Me.DataGrid1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.DataGrid1.BackgroundColor = System.Drawing.Color.White
        Me.DataGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataGrid1.CaptionBackColor = System.Drawing.Color.Khaki
        Me.DataGrid1.CaptionForeColor = System.Drawing.Color.MidnightBlue
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.FlatMode = True
        Me.DataGrid1.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.DataGrid1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.DataGrid1.GridLineColor = System.Drawing.Color.Gainsboro
        Me.DataGrid1.GridLineStyle = System.Windows.Forms.DataGridLineStyle.None
        Me.DataGrid1.HeaderBackColor = System.Drawing.Color.MidnightBlue
        Me.DataGrid1.HeaderFont = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.DataGrid1.HeaderForeColor = System.Drawing.Color.WhiteSmoke
        Me.DataGrid1.LinkColor = System.Drawing.Color.Teal
        Me.DataGrid1.Location = New System.Drawing.Point(16, 147)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.ParentRowsBackColor = System.Drawing.Color.Gainsboro
        Me.DataGrid1.ParentRowsForeColor = System.Drawing.Color.MidnightBlue
        Me.DataGrid1.PreferredColumnWidth = 95
        Me.DataGrid1.ReadOnly = True
        Me.DataGrid1.RowHeaderWidth = 95
        Me.DataGrid1.SelectionBackColor = System.Drawing.Color.CadetBlue
        Me.DataGrid1.SelectionForeColor = System.Drawing.Color.WhiteSmoke
        Me.DataGrid1.Size = New System.Drawing.Size(1136, 424)
        Me.DataGrid1.TabIndex = 0
        Me.DataGrid1.TabStop = False
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(232, 96)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(104, 23)
        Me.btnSearch.TabIndex = 1
        Me.btnSearch.Text = "Search Records"
        '
        'ListBox1
        '
        Me.ListBox1.Location = New System.Drawing.Point(368, 40)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(120, 56)
        Me.ListBox1.TabIndex = 4
        '
        'lblStart
        '
        Me.lblStart.Location = New System.Drawing.Point(24, 8)
        Me.lblStart.Name = "lblStart"
        Me.lblStart.Size = New System.Drawing.Size(100, 23)
        Me.lblStart.TabIndex = 5
        Me.lblStart.Text = "Start:"
        '
        'lblStop
        '
        Me.lblStop.Location = New System.Drawing.Point(24, 72)
        Me.lblStop.Name = "lblStop"
        Me.lblStop.Size = New System.Drawing.Size(100, 23)
        Me.lblStop.TabIndex = 6
        Me.lblStop.Text = "Stop:"
        '
        'cboStartMonth
        '
        Me.cboStartMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cboStartMonth.Location = New System.Drawing.Point(16, 32)
        Me.cboStartMonth.Name = "cboStartMonth"
        Me.cboStartMonth.Size = New System.Drawing.Size(56, 21)
        Me.cboStartMonth.TabIndex = 8
        '
        'cboStartDay
        '
        Me.cboStartDay.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})
        Me.cboStartDay.Location = New System.Drawing.Point(88, 32)
        Me.cboStartDay.Name = "cboStartDay"
        Me.cboStartDay.Size = New System.Drawing.Size(40, 21)
        Me.cboStartDay.TabIndex = 9
        '
        'cboStartYear
        '
        Me.cboStartYear.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009"})
        Me.cboStartYear.Location = New System.Drawing.Point(144, 32)
        Me.cboStartYear.Name = "cboStartYear"
        Me.cboStartYear.Size = New System.Drawing.Size(64, 21)
        Me.cboStartYear.TabIndex = 10
        '
        'cboStopYear
        '
        Me.cboStopYear.Items.AddRange(New Object() {"2005", "2006", "2007", "2008", "2009"})
        Me.cboStopYear.Location = New System.Drawing.Point(144, 96)
        Me.cboStopYear.Name = "cboStopYear"
        Me.cboStopYear.Size = New System.Drawing.Size(64, 21)
        Me.cboStopYear.TabIndex = 13
        '
        'cboStopDay
        '
        Me.cboStopDay.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})
        Me.cboStopDay.Location = New System.Drawing.Point(88, 96)
        Me.cboStopDay.Name = "cboStopDay"
        Me.cboStopDay.Size = New System.Drawing.Size(40, 21)
        Me.cboStopDay.TabIndex = 12
        '
        'cboStopMonth
        '
        Me.cboStopMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cboStopMonth.Location = New System.Drawing.Point(16, 96)
        Me.cboStopMonth.Name = "cboStopMonth"
        Me.cboStopMonth.Size = New System.Drawing.Size(56, 21)
        Me.cboStopMonth.TabIndex = 11
        '
        'lblSum
        '
        Me.lblSum.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSum.Location = New System.Drawing.Point(240, 32)
        Me.lblSum.Name = "lblSum"
        Me.lblSum.Size = New System.Drawing.Size(104, 32)
        Me.lblSum.TabIndex = 14
        Me.lblSum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblTotalQuantity
        '
        Me.lblTotalQuantity.Location = New System.Drawing.Point(240, 8)
        Me.lblTotalQuantity.Name = "lblTotalQuantity"
        Me.lblTotalQuantity.Size = New System.Drawing.Size(100, 23)
        Me.lblTotalQuantity.TabIndex = 15
        Me.lblTotalQuantity.Text = "Total Cubes:"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(648, 71)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 23)
        Me.Button1.TabIndex = 16
        Me.Button1.Text = "Update selected routes"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(368, 8)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(100, 23)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Search Route:"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(504, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 23)
        Me.Label2.TabIndex = 19
        Me.Label2.Text = "New Route:"
        '
        'ListBox2
        '
        Me.ListBox2.Location = New System.Drawing.Point(504, 40)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(120, 56)
        Me.ListBox2.TabIndex = 18
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(648, 40)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 23)
        Me.Button2.TabIndex = 20
        Me.Button2.Text = "Route Summary"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(144, -8)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(38, 23)
        Me.Button3.TabIndex = 21
        Me.Button3.Text = "Update CreditCheck"
        Me.Button3.Visible = False
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(824, 88)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(120, 24)
        Me.Button4.TabIndex = 22
        Me.Button4.Text = "Mark as reviewed"
        Me.Button4.Visible = False
        '
        'CheckBox1
        '
        Me.CheckBox1.Location = New System.Drawing.Point(120, 64)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(104, 24)
        Me.CheckBox1.TabIndex = 23
        Me.CheckBox1.Text = "Show reviewed"
        Me.CheckBox1.Visible = False
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(624, 8)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker1.TabIndex = 24
        Me.DateTimePicker1.Visible = False
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(1080, 8)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(72, 56)
        Me.Button5.TabIndex = 25
        Me.Button5.Text = "Update production date"
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem9, Me.MenuItem10})
        Me.MenuItem1.Text = "Tools"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Route summary"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "Production summary"
        '
        'MenuItem4
        '
        Me.MenuItem4.Enabled = False
        Me.MenuItem4.Index = 2
        Me.MenuItem4.Text = "Route details"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "Production details"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 4
        Me.MenuItem6.Text = "Weekly report"
        '
        'MenuItem7
        '
        Me.MenuItem7.Index = 5
        Me.MenuItem7.Text = "Law tags"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 6
        Me.MenuItem9.Text = "-"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 7
        Me.MenuItem10.Text = "Exit application"
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(952, 8)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(112, 43)
        Me.Button6.TabIndex = 26
        Me.Button6.Text = "Update both ship and prod dates"
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(952, 57)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(112, 23)
        Me.Button7.TabIndex = 27
        Me.Button7.Text = "Unschedule"
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(952, 88)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(112, 23)
        Me.Button8.TabIndex = 28
        Me.Button8.Text = "Sum selected"
        '
        'Button9
        '
        Me.Button9.Location = New System.Drawing.Point(826, 9)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(120, 40)
        Me.Button9.TabIndex = 29
        Me.Button9.Text = "Update Ship Date"
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(232, 64)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(104, 24)
        Me.Button10.TabIndex = 30
        Me.Button10.Text = "All Unscheduled"
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(824, 57)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(122, 23)
        Me.Button11.TabIndex = 31
        Me.Button11.Text = "Update Sequence"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(2409, 558)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblTotalQuantity)
        Me.Controls.Add(Me.lblSum)
        Me.Controls.Add(Me.cboStopYear)
        Me.Controls.Add(Me.cboStopDay)
        Me.Controls.Add(Me.cboStopMonth)
        Me.Controls.Add(Me.cboStartYear)
        Me.Controls.Add(Me.cboStartDay)
        Me.Controls.Add(Me.cboStartMonth)
        Me.Controls.Add(Me.lblStop)
        Me.Controls.Add(Me.lblStart)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.DataGrid1)
        Me.Menu = Me.MainMenu1
        Me.Name = "Form1"
        Me.Text = "`"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Me.switch = True
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal
        Me.intCount = 0



        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text

        'Dim ReviewTest As New clsOrderHeader("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "' and Route='" & Me.ListBox1.SelectedValue & "' and Reviewed='0'")
        Dim rvstr As String = " and Reviewed='0'"

        rvstr = ""
        'End If

        Dim o As New clsOrderHeaderNoStar("(([Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "') or [Shipment Date] <= '1/1/2001') and Route='" & Me.ListBox1.SelectedValue & "'" & rvstr)


        Dim ds As New DataSet
        Dim dt As New DataTable("Routes")
        dt.Columns.Add("HeaderNum", GetType(String))
        dt.Columns.Add("Name", GetType(String))
        dt.Columns.Add("Shipment Date", GetType(String))
        dt.Columns.Add("Production Date", GetType(String))
        dt.Columns.Add("Route", GetType(String))
        dt.Columns.Add("Sequence", GetType(String))
        dt.Columns.Add("Method", GetType(String))
        dt.Columns.Add("City", GetType(String))
        dt.Columns.Add("Qty", GetType(String))
        dt.Columns.Add("Cubes", GetType(String))
        dt.Columns.Add("Promised Date", GetType(String))
        dt.Columns.Add("CreditCheck", GetType(String))
        dt.Columns.Add("Reviewed", GetType(String))
        dt.TableName = "PView2"
        ds.Tables.Add(dt)


        Dim d As New DateTime(cboStartYear.Text, cboStartMonth.Text, cboStartDay.Text)


        Dim aGridTableStyle As New DataGridTableStyle
        aGridTableStyle.MappingName = "PView2"
        '
        ' Create GridColumnStyle objects for the grid columns 
        '
        aGridTableStyle.AlternatingBackColor = Color.Cornsilk
        aGridTableStyle.HeaderBackColor() = Color.Beige
        aGridTableStyle.AllowSorting = True


        Dim arl As New ArrayList
        Dim colhide As New Hashtable

        Dim colformat As New Hashtable
        'colformat.Add("QtyToShip", "#0")
        'colformat.Add("QtyRemaining", "#0")
        'colformat.Add("Compress", "#0.00")

        For Each c As DataColumn In dt.Columns
            Dim aCol1 As New DataGridTextBoxColumn

            aCol1.MappingName = c.ColumnName
            aCol1.HeaderText = c.ColumnName
            'aCol1.Format = "#0.00"
            aCol1.Width = 90
            If colhide.Contains(c.ColumnName) Then
                aCol1.Width = 0
            End If
            If colformat.Contains(c.ColumnName) Then
                aCol1.Format = colformat(c.ColumnName)
            End If
            arl.Add(aCol1)
        Next
        For Each acol As DataGridTextBoxColumn In arl
            aGridTableStyle.GridColumnStyles.Add(acol)
        Next

        Me.DataGrid1.TableStyles.Clear()
        Me.DataGrid1.TableStyles.Add(aGridTableStyle)

        'Dim o As New clsOrder("[Sell-to Customer No_] = '" & Me.ListBox1.SelectedValue & "'")

        'If Not IsDBNull(o.m_row.Item("QtyToShip")) Then

        If Not o.IsNew Then
            For Each r2 As DataRow In o.m_dt.Rows

                r2.Item("Cubes") = Decimal.Round(CDec(r2.Item("Cubes")), 2) ' * CDec(r2.Item("Qty"))), 2)
                r2.Item("Qty") = Decimal.Round((CDec(r2.Item("Qty"))), 0)
                r2.Item("Sequence") = Decimal.Round((CDec(r2.Item("Sequence"))), 2)

                Dim dr As DataRow = dt.NewRow
                For Each c As DataColumn In dt.Columns
                    'If Not c.ColumnName = "CreditCheck" Then
                    dr.Item(c.ColumnName) = CStr(r2.Item(c.ColumnName))
                    'End If
                Next
                If Date.Parse(r2.Item("Promised Date")) <= Date.Parse("1/1/2001") Then
                    dr.Item("Promised Date") = " "
                End If

                dt.Rows.Add(dr)




            Next


            'Me.DataGrid1.DataSource = o.m_dt
            Me.DataGrid1.DataSource = dt
        End If

        If o.IsNew Then
            '    MessageBox.Show("No rows available with selected query. Please try different dates.")
            Me.Button1.Enabled = False
            Me.DataGrid1.DataSource = o.m_dt

        Else
            Me.Button1.Enabled = True
            For Each r As DataRow In o.m_dt.Rows
                decTotal += Decimal.Round((CDec(r.Item("Cubes"))), 2)

                intCount += 1
            Next
        End If

        lblSum.Text = CStr(decTotal)


    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim pg As New clsRoute("")
        Dim dt3 As New DataTable
        dt3 = pg.distinct("[Route Code]")
        Me.ListBox1.DisplayMember = "Route Code"
        Me.ListBox1.ValueMember = "Route Code"
        Me.ListBox1.DataSource = dt3
        Dim pg2 As New clsRoute("")
        Dim dt4 As New DataTable
        dt4 = pg.distinct("[Route Code]")
        Me.ListBox2.DisplayMember = "Route Code"
        Me.ListBox2.ValueMember = "Route Code"
        Me.ListBox2.DataSource = dt4
        Me.Button1.Enabled = False
        Me.cboStartYear.DataSource = Module1.DropBoxYears
        Me.cboStopYear.DataSource = Module1.DropBoxYears
        'Preserving the date range between the Summary and Details page is complicated

        If rstartday = "" Then
            Me.cboStartDay.Text = Date.Now.Day
            Me.cboStartMonth.Text = Date.Now.Month
            Me.cboStartYear.Text = Date.Now.Year

            Me.cboStopDay.Text = Date.Now.Day
            Me.cboStopMonth.Text = Date.Now.Month
            Me.cboStopYear.Text = Date.Now.Year
        Else
            Me.cboStartDay.Text = rstartday
            Me.cboStartMonth.Text = rstartmonth
            Me.cboStartYear.Text = rstartyear

            Me.cboStopDay.Text = rendday
            Me.cboStopMonth.Text = rendmonth
            Me.cboStopYear.Text = rendyear
        End If
        If Not groute = "" Then
            Me.ListBox1.SelectedValue = groute
            Me.btnSearch.PerformClick()
        End If



        'Me.Button2.PerformClick()
    End Sub

    Private Sub Button1_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'UPDATE SELECTED ROUTES

        gRouteDatasetFresh = False
        Dim s2 As String = ""
        'Me.DataGrid1.columns(1).visible = False


        For x As Integer = 0 To (Me.intCount - 1)
            'Dim i As Integer = Me.DataGrid1.CurrentRowIndex()
            If Me.DataGrid1.IsSelected(x) Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                ' Dim l As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim crdt As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim sh As New clsSalesHeader(" No_ ='" & s & "'")
                If Date.Parse(sh.m_row.Item("Production Date")) <= Date.Parse(Me.RouteToDate(Me.ListBox2.SelectedValue, crdt)) Then
                    Dim con As SqlClient.SqlConnection = sh.m_con
                    con.Open()
                    If Date.Parse(Me.RouteToDate(Me.ListBox2.SelectedValue, crdt)).Year > "1970" Then
                        '''Dim str As String
                        '''If crdt = "1/1/2001" Then
                        '''    str = "update [" & Module1.CompanyName & "$Sales Header] set [Ship-to Route Code] = '" & Me.ListBox2.SelectedValue & "' where [No_] = '" & s & "'"
                        '''Else
                        '''    str = "update [" & Module1.CompanyName & "$Sales Header] set [Ship-to Route Code] = '" & Me.ListBox2.SelectedValue & "' where [No_] = '" & s & "'"
                        '''    'str = "update [" & Module1.CompanyName & "$Sales Header] set [Ship-to Route Code] = '" & Me.ListBox2.SelectedValue & "',[Requested Delivery Date] = '" & Me.RouteToDate(Me.ListBox2.SelectedValue, crdt) & "' where [No_] = '" & s & "'"
                        '''End If
                        '''Dim cmd As New SqlClient.SqlCommand(str, con)
                        '''cmd.ExecuteNonQuery()
                        '''str = "update [" & Module1.CompanyName & "$Sales Line] set [Ship-to Route Code] = '" & Me.ListBox2.SelectedValue & "' where [Document No_] = '" & s & "'"
                        '''cmd.CommandText = str
                        '''cmd.ExecuteNonQuery()
                        '''con.Close()

                        UpdateRoute(s, sh.m_row.Item("Document Type").ToString(), Me.ListBox2.SelectedValue)
                        s2 = s2 & s & " "
                    Else
                        MsgBox("You must first set a Shipment Date for this item")
                    End If
                Else
                    MsgBox("Order # " & sh.m_row.Item("No_") & " would have a production date later than it's shipment date and has not been updated")
                End If

            End If
        Next
        If Not s2 = "" Then
            MsgBox("Document #s " & s2 & " have been re-routed")

            If switch Then
                Me.btnSearch.PerformClick()
            Else
                Me.Button10.PerformClick()
            End If
            'Button1_Click(sender, e)
        End If

    End Sub

    Private Function RouteToDate(ByVal route As String, ByVal curdate As String) As String
        Dim newdate As String

        Dim dowhash As New Hashtable
        dowhash.Add("Sunday", 7)
        dowhash.Add("Monday", 1)
        dowhash.Add("Tuesday", 2)
        dowhash.Add("Wednesday", 3)
        dowhash.Add("Thursday", 4)
        dowhash.Add("Friday", 5)
        dowhash.Add("Saturday", 6)

        Dim rt As New clsRoute(route)
        Dim dc() As String = curdate.Split("/")
        Dim dtcurrent As New Date(dc(2), dc(0), dc(1))
        Dim delta As Integer = (CInt(rt.GetVal("Day of Week")) - dowhash(dtcurrent.DayOfWeek.ToString))
        If delta < 0 Then
            delta = 7 + delta
        End If
        dtcurrent = dtcurrent.AddDays(delta)
        newdate = dtcurrent.Month.ToString & "/" & dtcurrent.Day.ToString & "/" & dtcurrent.Year.ToString
        Return newdate
    End Function



    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        'Allows you to return easily to Route Summary.  Somewhat unneeded with menu

        Dim rs As New RouteSummary
        Me.Visible = False

        Me.Close()
        rs.ShowDialog()


        'Me.ListBox1.SelectedValue = rs.route
        If Not rs.route = "" Then
            Me.cboStartDay.Text = rs.day
            Me.cboStartMonth.Text = rs.month
            Me.cboStartYear.Text = rs.year
            Me.cboStopDay.Text = rs.day
            Me.cboStopMonth.Text = rs.month
            Me.cboStopYear.Text = rs.year
            Me.ListBox1.SelectedValue = rs.route
        End If

        If Not rs.year = "" Then
            rs.Close()

            Me.Visible = True
        Else

            rs.Close()
            Me.Close()
            Return
        End If
        Me.Button1_Click(sender, e)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        'Toggles Credit Check field for selected items

        Dim s2 As String = ""

        For x As Integer = 0 To (Me.intCount - 1)
            If Me.DataGrid1.IsSelected(x) Then


                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                Dim cc As String = CStr(Me.DataGrid1.Item(x, 10))
                Dim crdt As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim sh As New clsSalesHeader("No_ = '" & s & "'")
                'If Date.Parse(sh.m_row.Item("Production Date")) <= d Then
                Dim con As SqlClient.SqlConnection = sh.m_con
                con.Open()
                Dim str As String
                If cc = "Y" Then
                    str = "update [" & Module1.CompanyName & "$Sales Header] set [CreditCheck] = '" & "" & "' where [No_] = '" & s & "'"
                Else
                    str = "update [" & Module1.CompanyName & "$Sales Header] set [CreditCheck] = '" & "Y" & "' where [No_] = '" & s & "'"
                End If
                Dim cmd As New SqlClient.SqlCommand(str, con)
                cmd.ExecuteNonQuery()

                con.Close()
                s2 = s2 & s & " "
                'Else
                '   MsgBox("Order # " & sh.m_row.Item("No_") & " would have a production date later than it's shipment date and has not been updated")

                ' End If
            End If
        Next
        If Not s2 = "" Then
            MsgBox("Document #s " & s2 & " have been marked as Credit Checked")



            If Not s2 = "" Then
                MsgBox("Document #s " & s2 & " have been re-routed")
                If switch Then
                    Me.btnSearch.PerformClick()
                Else
                    Me.Button10.PerformClick()
                End If
                'Button1_Click(sender, e)
            End If
        End If

    End Sub
    Public Sub UpdateRoute(ByVal salesOrderNum As String, ByVal salesOrderType As String, ByVal route As String)

        Try
            servicerelreo.DoReopen(salesOrderNum)
            Dim pso As NavSummary.jcbedws.SalesOrder = service.Read(salesOrderNum)
            pso.Ship_to_Route_Code = route
            service.Update(pso)


            Dim pso2 As NavSummary.jcbedws.SalesOrder = service.Read(salesOrderNum)
            For Each sol As NavSummary.jcbedws.Sales_Order_Line In pso2.SalesLines

                Dim slws As SalesLineWS.SalesLineforRouteWS = serviceline.Read(1, sol.Document_No, sol.Line_No)
                slws.Ship_to_Route_Code = route
                serviceline.Update(slws)

            Next

            servicerelreo.DoRelease(salesOrderNum)


        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Public Sub UpdateSequence(ByVal salesOrderNum As String, ByVal salesOrderType As String, ByVal sequence As String)

        Try
            Dim decseq As Decimal
            Try
                decseq = Decimal.Parse(sequence)
            Catch ex As Exception
                decseq = 0.0
            End Try
            servicerelreo.DoReopen(salesOrderNum)
            Try

                Dim pso As NavSummary.jcbedws.SalesOrder = service.Read(salesOrderNum)
                pso.Route_Sequence = decseq
                service.Update(pso)


            Catch ex As Exception

            End Try

            servicerelreo.DoRelease(salesOrderNum)


        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub


    Private Function ChangeDOW(ByVal newday As String, ByVal curdate As String) As String
        'Function to calculate new day of week for route changes

        Dim newdate As String

        Dim dowhash As New Hashtable
        dowhash.Add("Sunday", 7)
        dowhash.Add("Monday", 1)
        dowhash.Add("Tuesday", 2)
        dowhash.Add("Wednesday", 3)
        dowhash.Add("Thursday", 4)
        dowhash.Add("Friday", 5)
        dowhash.Add("Saturday", 6)


        Dim dc() As String = curdate.Split("/")
        Dim dtcurrent As New Date(dc(2), dc(0), dc(1))
        Dim delta As Integer = (dowhash(newday) - dowhash(dtcurrent.DayOfWeek.ToString))
        If delta < 0 Then
            delta = 7 + delta
        End If
        dtcurrent = dtcurrent.AddDays(delta)
        newdate = dtcurrent.Month.ToString & "/" & dtcurrent.Day.ToString & "/" & dtcurrent.Year.ToString
        Return newdate
    End Function



    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        'Sets reviewed flag
        'It was originally meant to effect the display of items but is now just a bookmark of sorts

        Dim s2 As String = ""
        For x As Integer = 0 To (Me.intCount - 1)
            If Me.DataGrid1.IsSelected(x) Then


                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                'Dim l As String = CStr(Me.DataGrid1.Item(x, 2))
                'Dim crdt As String = CStr(Me.DataGrid1.Item(x, 14))
                Dim sh As New clsSalesHeader("No_ = '" & s & "'")

                Dim con As SqlClient.SqlConnection = sh.m_con
                con.Open()
                Dim str As String = "update [" & Module1.CompanyName & "$Sales Header] set [Reviewed] = '1' where [No_]='" & s & "'"

                Dim cmd As New SqlClient.SqlCommand(str, con)
                cmd.ExecuteNonQuery()


                con.Close()
                s2 = s2 & s & " "
            End If
        Next
        MsgBox("Document #s " & s2 & " have been reviewed ")
        Me.btnSearch.PerformClick()
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        'Updates Production Date

        Dim s2 As String = ""

        Dim d As Date
        Dim f As New frmCalendar
        f.ShowDialog()
        d = f.d
        f.Close()


        For x As Integer = 0 To (Me.intCount - 1)
            If Me.DataGrid1.IsSelected(x) Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                'Dim l As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim crdt As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim sh As New clsSalesHeader("No_ = '" & s & "'")
                If Date.Parse(sh.m_row.Item("Requested Delivery Date")) >= d Then


                    'Dim con As SqlClient.SqlConnection = sh.m_con
                    'con.Open()
                    'Dim str As String = "update [" & Module1.CompanyName & "$Sales Header] set [Production Date] = '" & d.ToShortDateString & "' where [No_] = '" & s & "'"

                    'Dim cmd As New SqlClient.SqlCommand(str, con)
                    'cmd.ExecuteNonQuery()


                    'con.Close()
                    Try
                        servicerelreo.DoReopen(s)
                        Dim pso As NavSummary.jcbedws.SalesOrder = service.Read(s)
                        pso.Production_Date = d
                        service.Update(pso)




                        servicerelreo.DoRelease(s)


                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try

                    s2 = s2 & s & " "
                Else
                    MsgBox("Order # " & sh.m_row.Item("No_") & " would have a production date later than it's shipment date and has not been updated")

                End If
            End If
        Next
        If Not s2 = "" Then
            MsgBox("Document #s " & s2 & " production date has been changed to " & d.ToShortDateString)
            'Button1_Click(sender, e)
        End If
        If Not s2 = "" Then
            MsgBox("Document #s " & s2 & " have been re-routed")
            If switch Then
                Me.btnSearch.PerformClick()
            Else
                Me.Button10.PerformClick()
            End If
            'Button1_Click(sender, e)
        End If
    End Sub


    'Menu items. pretty obvious....

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim f As New RouteSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        Dim f As New frmHoursSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        Me.Button1.PerformClick()
    End Sub

    Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem5.Click
        Dim f As New Form3
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem6.Click
        Dim f As New frmProdSched
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem7.Click
        Dim f As New Form2
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem9.Click

        Application.Exit()
    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim f As New frmAddRoute
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub



    Private Sub MenuItem10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem10.Click
        Application.Exit()

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        'Updates production date AND shipdate
        'Pops up 2 small forms to accept new dates and checks to make sure ship date is not befor production date

        Dim s2 As String = ""

        Dim d As Date
        Dim f As New frmCalendar
        f.Label1.Text = "Ship Date"
        f.ShowDialog()
        d = f.d
        f.Close()


        For x As Integer = 0 To (Me.intCount - 1)
            If Me.DataGrid1.IsSelected(x) Then


                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                'Dim l As String = CStr(Me.DataGrid1.Item(x, 2))
                'Dim crdt As String = CStr(Me.DataGrid1.Item(x, 2))
                'Dim sh As New clsSalesHeader("No_ = '" & s & "'")
                ''If Date.Parse(sh.m_row.Item("Production Date")) <= d Then
                'Dim con As SqlClient.SqlConnection = sh.m_con
                'con.Open()
                'Dim str As String = "update [" & Module1.CompanyName & "$Sales Header] set [Requested Delivery Date] = '" & d.ToShortDateString & "' where [No_] = '" & s & "'"

                'Dim cmd As New SqlClient.SqlCommand(str, con)
                'cmd.ExecuteNonQuery()


                'con.Close()
                Try
                    servicerelreo.DoReopen(s)
                    Dim pso As NavSummary.jcbedws.SalesOrder = service.Read(s)
                    pso.Requested_Delivery_Date = d
                    service.Update(pso)




                    servicerelreo.DoRelease(s)


                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                s2 = s2 & s & " "
                '     Else
                '        MsgBox("Order # " & sh.m_row.Item("No_") & " would have a production date later than it's shipment date and has not been updated")
                '
                '           End If
            End If
        Next
        If Not s2 = "" Then
            MsgBox("Document #s " & s2 & " have been moved to " & d.ToShortDateString)
        End If
        Dim savedate As Date = d
        f.Label1.Text = "Production Date"
        f.ShowDialog()
        d = f.d
        f.Close()

        s2 = ""
        For x As Integer = 0 To (Me.intCount - 1)
            If Me.DataGrid1.IsSelected(x) Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                'Dim l As String = CStr(Me.DataGrid1.Item(x, 2))
                'Dim crdt As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim sh As New clsSalesHeader("No_ = '" & s & "'")
                If Date.Parse(sh.m_row.Item("Requested Delivery Date")) >= d Then


                    '    Dim con As SqlClient.SqlConnection = sh.m_con
                    '    con.Open()
                    '    Dim str As String = "update [" & Module1.CompanyName & "$Sales Header] set [Production Date] = '" & d.ToShortDateString & "' where [No_] = '" & s & "'"

                    '    Dim cmd As New SqlClient.SqlCommand(str, con)
                    '    cmd.ExecuteNonQuery()


                    '    con.Close()
                    Try
                        servicerelreo.DoReopen(s)
                        Dim pso As NavSummary.jcbedws.SalesOrder = service.Read(s)
                        pso.Production_Date = d
                        service.Update(pso)




                        servicerelreo.DoRelease(s)


                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try

                    s2 = s2 & s & " "
                Else
                    MsgBox("Order # " & sh.m_row.Item("No_") & " would have a production date later than it's shipment date and has not been updated")

                End If
            End If
        Next
        If Not s2 = "" Then
            MsgBox("Document #s " & s2 & " production date has been changed to " & d.ToShortDateString)
            'Button1_Click(sender, e)
        End If
        d = savedate




        If Not s2 = "" Then
            '    MsgBox("Document #s " & s2 & " have been re-routed")
            If switch Then
                Me.btnSearch.PerformClick()
            Else
                Me.Button10.PerformClick()
            End If
            'Button1_Click(sender, e)
        End If


    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        'Updates Production and shipment dates by setting both to 1/1/2001

        gRouteDatasetFresh = False
        Dim s2 As String = ""

        For x As Integer = 0 To (Me.intCount - 1)
            If Me.DataGrid1.IsSelected(x) Then


                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                'Dim l As String = CStr(Me.DataGrid1.Item(x, 2))
                'Dim crdt As String = CStr(Me.DataGrid1.Item(x, 2))
                'Dim sh As New clsSalesHeader("No_ = '" & s & "'")
                ''If Date.Parse(sh.m_row.Item("Production Date")) <= d Then
                'Dim con As SqlClient.SqlConnection = sh.m_con
                'con.Open()
                'Dim str As String = "update [" & Module1.CompanyName & "$Sales Header] set [Requested Delivery Date] = '" & "1/1/2001" & "' where [No_] = '" & s & "'"

                'Dim cmd As New SqlClient.SqlCommand(str, con)
                'cmd.ExecuteNonQuery()
                'str = "update [" & Module1.CompanyName & "$Sales Header] set [Production Date] = '" & "1/1/2001" & "',[Route Sequence] = 0.00 where [No_] = '" & s & "'"
                'Dim cmd2 As New SqlClient.SqlCommand(str, con)
                'cmd2.ExecuteNonQuery()

                'con.Close()
                Try
                    Dim d As DateTime = "1/1/2001"
                    servicerelreo.DoReopen(s)
                    Dim pso As NavSummary.jcbedws.SalesOrder = service.Read(s)
                    pso.Shipment_Date = d
                    pso.Production_Date = d
                    pso.Route_Sequence = 0
                    service.Update(pso)




                    servicerelreo.DoRelease(s)


                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                s2 = s2 & s & " "

            End If
        Next
        If Not s2 = "" Then
            MsgBox("Document #s " & s2 & " have been unscheduled")




            If Not s2 = "" Then
                MsgBox("Document #s " & s2 & " have been re-routed")
                If switch Then
                    Me.btnSearch.PerformClick()
                Else
                    Me.Button10.PerformClick()
                End If
                'Button1_Click(sender, e)
            End If
        End If
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        'Pops up a little window with sum of Pcs and Cubes

        Dim s2 As String = ""

        Dim ct As Decimal = 0
        Dim cu As Decimal = 0

        For x As Integer = 0 To (Me.intCount - 1)
            If Me.DataGrid1.IsSelected(x) Then
                ct += CDec(Me.DataGrid1.Item(x, 7))
                cu += CDec(Me.DataGrid1.Item(x, 8))

            End If
        Next
        MsgBox("Cubes = " & cu & " --- Pcs = " & ct)
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        'Update ship date and status

        gRouteDatasetFresh = False
        Dim s2 As String = ""

        Dim d As Date
        Dim f As New frmCalendar
        f.ShowDialog()
        d = f.d
        f.Close()
        For x As Integer = 0 To (Me.intCount - 1)
            If Me.DataGrid1.IsSelected(x) Then


                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                'Dim l As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim crdt As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim sh As New clsSalesHeader("No_ = '" & s & "'")
                If Date.Parse(sh.m_row.Item("Production Date")) <= d Then
                    Try
                        servicerelreo.DoReopen(s)
                        Dim pso As NavSummary.jcbedws.SalesOrder = service.Read(s)
                        pso.Shipment_Date = d
                        service.Update(pso)




                        servicerelreo.DoRelease(s)


                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                    End Try
                    'Dim con As SqlClient.SqlConnection = sh.m_con
                    'con.Open()
                    'Dim str As String = "update [" & Module1.CompanyName & "$Sales Header] set [Shipment Date] = '" & d.ToShortDateString & "' , Status = '2' where [No_] = '" & s & "'"

                    'Dim cmd As New SqlClient.SqlCommand(str, con)
                    'cmd.ExecuteNonQuery()


                    'con.Close()
                    s2 = s2 & s & " "
                Else
                    MsgBox("Order # " & sh.m_row.Item("No_") & " would have a production date later than it's shipment date and has not been updated")

                End If
            End If
        Next
        If Not s2 = "" Then
            MsgBox("Document #s " & s2 & "Ship Dates have been moved to " & d.ToShortDateString)




            If Not s2 = "" Then
                MsgBox("Document #s " & s2 & " have been re-routed")
                If switch Then
                    Me.btnSearch.PerformClick()
                Else
                    Me.Button10.PerformClick()
                End If
                'Button1_Click(sender, e)
            End If

        End If


    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        'SHow all unscheduled orders

        Me.switch = False
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal
        Me.intCount = 0



        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text

        'Dim ReviewTest As New clsOrderHeader("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "' and Route='" & Me.ListBox1.SelectedValue & "' and Reviewed='0'")
        Dim rvstr As String = " and Reviewed='0'"

        rvstr = ""
        'End If

        Dim o As New clsOrderHeader("[Shipment Date] <= '1/1/2001'" & rvstr)


        Dim ds As New DataSet
        Dim dt As New DataTable("Routes")
        dt.Columns.Add("HeaderNum", GetType(String))
        dt.Columns.Add("Name", GetType(String))
        dt.Columns.Add("Shipment Date", GetType(String))
        dt.Columns.Add("Production Date", GetType(String))
        dt.Columns.Add("Route", GetType(String))
        dt.Columns.Add("Sequence", GetType(String))
        dt.Columns.Add("Method", GetType(String))
        dt.Columns.Add("City", GetType(String))
        dt.Columns.Add("Qty", GetType(String))
        dt.Columns.Add("Cubes", GetType(String))
        dt.Columns.Add("Promised Date", GetType(String))
        ' dt.Columns.Add("CreditCheck", GetType(String))
        dt.Columns.Add("Reviewed", GetType(String))

        dt.TableName = "PView2"
        ds.Tables.Add(dt)


        Dim d As New DateTime(cboStartYear.Text, cboStartMonth.Text, cboStartDay.Text)


        Dim aGridTableStyle As New DataGridTableStyle
        aGridTableStyle.MappingName = "PView2"
        '
        ' Create GridColumnStyle objects for the grid columns 
        '
        aGridTableStyle.AlternatingBackColor = Color.Cornsilk
        aGridTableStyle.HeaderBackColor() = Color.Beige
        aGridTableStyle.AllowSorting = True


        Dim arl As New ArrayList
        Dim colhide As New Hashtable
        'colhide.Add("HeaderID", 1)
        'colhide.Add("Length", 1)
        'colhide.Add("Width", 1)
        'colhide.Add("Height", 1)
        'colhide.Add("Compress", 1)
        Dim colformat As New Hashtable
        'colformat.Add("QtyToShip", "#0")
        'colformat.Add("QtyRemaining", "#0")
        'colformat.Add("Compress", "#0.00")

        For Each c As DataColumn In dt.Columns
            Dim aCol1 As New DataGridTextBoxColumn

            aCol1.MappingName = c.ColumnName
            aCol1.HeaderText = c.ColumnName
            'aCol1.Format = "#0.00"
            aCol1.Width = 90
            If colhide.Contains(c.ColumnName) Then
                aCol1.Width = 0
            End If
            If colformat.Contains(c.ColumnName) Then
                aCol1.Format = colformat(c.ColumnName)
            End If
            arl.Add(aCol1)
        Next
        For Each acol As DataGridTextBoxColumn In arl
            aGridTableStyle.GridColumnStyles.Add(acol)
        Next

        Me.DataGrid1.TableStyles.Clear()
        Me.DataGrid1.TableStyles.Add(aGridTableStyle)

        'Dim o As New clsOrder("[Sell-to Customer No_] = '" & Me.ListBox1.SelectedValue & "'")

        'If Not IsDBNull(o.m_row.Item("QtyToShip")) Then

        If Not o.IsNew Then
            For Each r2 As DataRow In o.m_dt.Rows

                r2.Item("Cubes") = Decimal.Round(CDec(r2.Item("Cubes")), 2) ' * CDec(r2.Item("Qty"))), 2)
                r2.Item("Sequence") = Decimal.Round(CDec(r2.Item("Sequence")), 2) ' * CDec(r2.Item("Qty"))), 2)
                r2.Item("Qty") = Decimal.Round((CDec(r2.Item("Qty"))), 0)

                Dim dr As DataRow = dt.NewRow
                For Each c As DataColumn In dt.Columns
                    dr.Item(c.ColumnName) = CStr(r2.Item(c.ColumnName))
                Next
                If Date.Parse(r2.Item("Promised Date")) <= Date.Parse("1/1/2001") Then
                    dr.Item("Promised Date") = " "
                End If
                dt.Rows.Add(dr)



            Next


            'Me.DataGrid1.DataSource = o.m_dt
            Me.DataGrid1.DataSource = dt
        End If

        If o.IsNew Then
            '    MessageBox.Show("No rows available with selected query. Please try different dates.")
            Me.Button1.Enabled = False
            Me.DataGrid1.DataSource = o.m_dt

        Else
            Me.Button1.Enabled = True
            For Each r As DataRow In o.m_dt.Rows
                decTotal += Decimal.Round((CDec(r.Item("Cubes"))), 2)

                intCount += 1
            Next
        End If

        lblSum.Text = CStr(decTotal)


    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        'UPDATE SELECTED ROUTES

        gRouteDatasetFresh = False
        Dim s2 As String = ""
        'Me.DataGrid1.columns(1).visible = False
        Dim seq As String
        Dim f As New frmTextbox
        f.ShowDialog()
        seq = f.txtTextbox.Text
        f.Close()


        For x As Integer = 0 To (Me.intCount - 1)
            'Dim i As Integer = Me.DataGrid1.CurrentRowIndex()
            If Me.DataGrid1.IsSelected(x) Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                ' Dim l As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim crdt As String = CStr(Me.DataGrid1.Item(x, 2))
                Dim sh As New clsSalesHeader(" No_ ='" & s & "'")

                Dim con As SqlClient.SqlConnection = sh.m_con
                con.Open()



                UpdateSequence(s, sh.m_row.Item("Document Type").ToString(), seq)
                s2 = s2 & s & " "


            End If
        Next
        If Not s2 = "" Then
            MsgBox("Document #s " & s2 & " have been re-sequenced")

            If switch Then
                Me.btnSearch.PerformClick()
            Else
                Me.Button10.PerformClick()
            End If
            'Button1_Click(sender, e)
        End If
    End Sub
End Class
