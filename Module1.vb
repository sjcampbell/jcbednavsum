Module Module1
    Public gstartmonth As String = ""
    Public gendmonth As String = ""
    Public gstartday As String = ""
    Public gendday As String = ""
    Public gstartyear As String = ""
    Public gendyear As String = ""
    Public groute As String = ""
    Public rstartmonth As String = ""
    Public rendmonth As String = ""
    Public rstartday As String = ""
    Public rendday As String = ""
    Public rstartyear As String = ""
    Public rendyear As String = ""
    Public gLoadRoutes As Boolean = False
    Public gRouteDatasetFresh As Boolean = False
    Public gRouteDataset As DataSet
    Public gcount As Integer
    Public othertags As New Hashtable
    Public otherarl As New ArrayList
    Public pdlt As PrintDialog
    Public pdtherest As PrintDialog
    Public dresult1 As DialogResult
    Public dresult2 As DialogResult
    Public ticketcounter As Integer
    Public PrintSet As New DataSet
    Public pshash As New Hashtable


    Sub main(ByVal Args() As String)
        PrintSetInit()
        Dim asm As System.Reflection.Assembly
        asm = System.Reflection.Assembly.GetExecutingAssembly
        If Args.Length > 0 Then
            For Each t As System.Type In asm.GetTypes
                'MsgBox(t.ToString)
                Dim s As String() = t.ToString.Split(".")
                If s(1) = Args(0) Then
                    Dim o As Object = Activator.CreateInstance(t)
                    o.WindowState = FormWindowState.Normal
                    o.showdialog()
                End If
            Next
        Else
            'Your default form goes here
            Dim f As New Form2
            f.ShowDialog()
        End If

    End Sub


    Public Function CompanyName() As String
        Return companynamestr()

        Return "Holding Company, LLC"
    End Function


    Public Function companynamestr() As String
        Dim colValues As System.Collections.Specialized.NameValueCollection
        colValues = System.Configuration.ConfigurationSettings.AppSettings()
        'Console.WriteLine("YOU ARE HERE")

        Return colValues.Get("company")
        ' Return "Holding Company, LLC"
    End Function

    Public Function testmode() As String
        Dim colValues As System.Collections.Specialized.NameValueCollection
        colValues = System.Configuration.ConfigurationSettings.AppSettings()
        'Console.WriteLine("YOU ARE HERE")

        Return colValues.Get("testmode")
        ' Return "Holding Company, LLC"
    End Function

    Public Function lawtagtray() As String
        Dim colValues As System.Collections.Specialized.NameValueCollection
        colValues = System.Configuration.ConfigurationSettings.AppSettings()
        'Console.WriteLine("YOU ARE HERE")

        Return colValues.Get("lawtagtray")
        ' Return "Holding Company, LLC"
    End Function

    Public Function DropBoxYears() As String()
        Dim ar(10) As String
        ar(0) = DateTime.Now.AddYears(-4).Year.ToString()
        ar(1) = DateTime.Now.AddYears(-3).Year.ToString()
        ar(2) = DateTime.Now.AddYears(-2).Year.ToString()
        ar(3) = DateTime.Now.AddYears(-1).Year.ToString()
        ar(4) = DateTime.Now.Year.ToString()
        ar(5) = DateTime.Now.AddYears(1).Year.ToString()
        ar(6) = DateTime.Now.AddYears(2).Year.ToString()
        ar(7) = DateTime.Now.AddYears(3).Year.ToString()
        ar(8) = DateTime.Now.AddYears(4).Year.ToString()
        ar(9) = DateTime.Now.AddYears(5).Year.ToString()
        Return ar

    End Function
    Public Sub PrintSetInit()

        Dim dt As New DataTable("Printed")
        dt.Columns.Add("Order", GetType(String))
        dt.Columns.Add("Company", GetType(String))

        PrintSet.Tables.Add(dt)
    End Sub


    Public Sub PrintSetAddRow(ByVal r As DataRow)
        If Not pshash.Contains(r.Item("OrderNum")) Then
            pshash.Add(r.Item("OrderNum"), 1)
            Dim nr As DataRow = PrintSet.Tables(0).NewRow
            nr.Item("Order") = r.Item("OrderNum")
            nr.Item("Company") = r.Item("Name")
            PrintSet.Tables(0).Rows.Add(nr)
        End If
    End Sub



End Module
