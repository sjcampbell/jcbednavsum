Imports DbMaster
Public Class clsPrototype

    Inherits clSqlMaster

    Sub New(ByVal arg As String)
        'Passes the table name, and key field to the parent class
        'MyBase.New(arg, "[Holding Company, LLC$Ship-To Route Code]", "[Route Code]")
        MyBase.New(arg, "[" & Module1.CompanyName() & "$Prototype]", "Model")
    End Sub

    Public Function FoundationList() As String
        If Me.IsNew Then
            Return ""
        End If
        If Me.m_dt.Rows.Count = 0 Then
            Return ""
        End If
        Dim s(Me.m_dt.Rows.Count - 1) As String
        Dim count As Integer = 0
        For Each r As DataRow In Me.m_dt.Rows
            If r.Item("Test") = 2 Then
                s(count) = r.Item("Foundation Model No_")
                count += 1
            End If
        Next

        Return Join(s, ",")


    End Function

    Public Function ProtoList() As String
        If Me.IsNew Then
            Return ""
        End If
        If Me.m_dt.Rows.Count = 0 Then
            Return ""
        End If
        Dim s(Me.m_dt.Rows.Count - 1) As String
        Dim count As Integer = 0
        For Each r As DataRow In Me.m_dt.Rows
            If r.Item("Test") = 2 Then
                s(count) = r.Item("Prototype No_") & " - " & r.Item("Foundation Model No_")
                count += 1
            End If
        Next
        ' s(0) = "Prototype ID: " & s(0)
        Return Join(s, ControlChars.CrLf)


    End Function

End Class