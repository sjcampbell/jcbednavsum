Imports DbMaster

Public Class clsOrderRoute

    Inherits clSqlMaster

    Sub New(ByVal arg As String, Optional ByVal SA As Boolean = False)
        'Passes the table name, and key field to the parent class
        'MyBase.New("PView2", arg)
        'Passes the table name, and key field to the parent class

        MyBase.New()
        Dim s As String = CompanyName()
        'Me.m_con.Open()
        Dim command As SqlClient.SqlCommand =
             New SqlClient.SqlCommand("spPOrderRoute", Me.m_con)
        If SA Then
            command = New SqlClient.SqlCommand("spPOrderRouteSA", Me.m_con)
        End If
        command.Parameters.Add("@tablename", s)
        'command.Parameters.Add("@argument", arg)


        command.CommandType = CommandType.StoredProcedure
        Me.StoredProcedure(command, arg)
        'Dim s2 As String = Me.m_row.Item("Cubes")
    End Sub
End Class

