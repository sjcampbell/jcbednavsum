Imports DbMaster
Public Class clsSalesLine

    Inherits clSqlMaster

    Sub New(ByVal arg As String)
        'Passes the table name, and key field to the parent class
        'MyBase.New(arg, "[Holding Company, LLC$Ship-To Route Code]", "[Route Code]")
        MyBase.New("[" & CompanyName() & "$Sales Line]", arg)
    End Sub
End Class