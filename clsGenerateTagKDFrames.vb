Imports CRHelper
Imports System.IO
'Imports DatasetHelper

Public Class clsGenerateTagKDFrames
    Public ordernum As String
    Public linenum As String
    Public instance As String
    Public pd As PrintDialog
    Public result As DialogResult
    Public typeother As Boolean

    Sub New(ByVal OrderNumber As String, ByVal LineNumber As String, ByVal instancenum As String, ByVal pdialog As PrintDialog, ByVal presult As DialogResult, ByVal other As Boolean)
        ordernum = OrderNumber
        linenum = LineNumber
        instance = instancenum
        pd = pdialog
        result = presult
        typeother = other
    End Sub

    Public Function PrintIt() As Integer
        'Only use type 2
        'MsgBox("KD FRames verion")
        Dim line As New clsSalesLine("[Document No_] ='" & ordernum & "' and [Line No_]='" & linenum & "'")
        If Not (line.m_row.Item("Type") = "2") Then
            Return 0
        End If

        Module1.ticketcounter += 1 'Uses global so we can print the page number for the whole print job


        'Look up law tag and create new one if not present
        Dim lw As New clsLawTag("[Document No_] ='" & ordernum & "' and [Line No_]='" & linenum & "' and [Instance]='" & instance & "'")
        If lw.IsNew Then
            lw.m_cmd.QuotePrefix = "["
            lw.m_cmd.QuoteSuffix = "]"
            lw.m_row.Item("Tag Number") = lw.NextId("Tag Number")
            lw.m_row.Item("Document No_") = ordernum
            lw.m_row.Item("Line No_") = linenum
            lw.m_row.Item("Instance") = instance
            lw.m_row.Item("PM Redemption Date") = "1/1/1753"
            lw.m_row.Item("PM Date") = "1/1/1753"
            lw.m_row.Item("Pack Date") = "1/1/1753"
            lw.m_row.Item("Piece Date") = "1/1/1753"
            lw.m_row.Item("Packed By") = "NOT PACKED"
            lw.m_row.Item("Packed") = 0
            lw.m_row.Item("Inventory Updated") = 0
            lw.m_row.Item("Changed by Doc No_") = " "


            lw.update()
        End If


        Dim sl3 As New clsTearOff2(" [" & Module1.CompanyName & "$Sales Line].[Document No_]='" & ordernum & "' and [" & Module1.CompanyName & "$Sales Line].[Line No_]='" & linenum & "'")

        Dim sl As New clsTagView(" OrderNum='" & ordernum & "' and LineNum='" & linenum & "'")
        ''''''''''''''''''Dim sl2 As New clsTearOff(" OrderNum='" & ordernum & "' and LineNum='" & linenum & "'")
        Dim sl2 As New clsTearOff("  [" & Module1.CompanyName & "$Sales Line].[Document No_]='" & ordernum & "' and [" & Module1.CompanyName & "$Sales Line].[Line No_]='" & linenum & "'")
        Dim templine As New clsSalesLine(" [Document No_] ='" & ordernum & "' and [Line No_]='" & linenum & "'")
        Dim sub2 As New clsSR2("  [" & Module1.CompanyName & "$Sales Line].[Document No_]='" & ordernum & "' and [" & Module1.CompanyName & "$Sales Line].[Line No_]='" & linenum & "'")

        If Not templine.m_row.Item("BOM Item No_") = "" Then
            '  MsgBox("In if")
            sl2.m_row.Item("Item") = templine.m_row.Item("BOM Item No_")
            sl.m_row.Item("Item") = templine.m_row.Item("BOM Item No_")
        End If


        Dim arl As New ArrayList
        'MsgBox(" OrderNum='" & ordernum & "' and LineNum='" & linenum & "'")
        If (Not sl2.IsNew) Or (Not templine.m_row.Item("BOM Item No_") = "") Then
            ''  MsgBox("In If " & sl2.m_row.Item("Item"))
            FindSubAssemblies(0, sl2.m_row.Item("Item"), arl)
        Else
            ' MsgBox("Is new")
        End If
        Dim holder As DataRow = sl2.m_row
        'MsgBox("3")

        sl2.m_dt.Rows.Clear()
        sub2.m_dt.Rows.Clear()
        Dim gitem As String = templine.m_row.Item("BOM Item No_")
        Dim counter As Integer = 0
        For Each ds As clsBOMLineSelect In arl
            'MsgBox("bls1")

            For Each r As DataRow In ds.m_dt.Rows
                'MsgBox("bls2")

                Dim drow2 As DataRow = sl2.m_dt.NewRow
                Dim drow3 As DataRow = sub2.m_dt.NewRow

                For Each col As DataColumn In sl2.m_dt.Columns
                    drow2.Item(col.ColumnName) = sl3.m_row.Item(col.ColumnName)
                Next

                For Each col As DataColumn In sub2.m_dt.Columns
                    drow3.Item(col.ColumnName) = sl3.m_row.Item(col.ColumnName)
                Next
                drow2.Item("Labor Cost") = r.Item("Quantity")
                'MsgBox(r.Item("Description") & " - " & r.Item("No_"))
                drow2.Item("BDescription") = r.Item("Description") & " - " & r.Item("No_")
                drow2.Item("Item") = r.Item("BOM No_")
                drow2.Item("ProdGroup") = r.Item("ProdGroup")

                drow3.Item("Labor Cost") = r.Item("Quantity")
                'MsgBox(r.Item("Description") & " - " & r.Item("No_"))
                drow3.Item("BDescription") = r.Item("Description") & " - " & r.Item("No_")
                drow3.Item("Item") = r.Item("BOM No_")
                drow3.Item("ProdGroup") = r.Item("ProdGroup")




                If Not r.Item("Description") = "Fringe & Overhead" Then
                    If r.Item("Piece Rate Ticket") = 3 Then
                        counter += 1
                        sl2.m_dt.Rows.Add(drow2)
                        If counter >= 11 And counter <= 13 Then
                            sub2.m_dt.Rows.Add(drow3)
                        End If
                    End If
                    Dim junk As String = "*" & lw.m_row.Item("Tag Number") & "_" & r.Item("BOM No_") & "_" & r.Item("Line No_") & "*"


                    drow3.Item("ScanField") = "*" & lw.m_row.Item("Tag Number") & "_" & r.Item("BOM No_") & "_" & r.Item("Line No_") & "*"

                    drow2.Item("ScanField") = "*" & lw.m_row.Item("Tag Number") & "_" & r.Item("BOM No_") & "_" & r.Item("Line No_") & "*"
                    'Write row to TearOffTag table
                    Dim eto As New clsEmployeeTearOff("BOMNo ='" & r.Item("BOM No_") & "' and LineNum='" & r.Item("Line No_") & "' and LawTagNum='" & lw.m_row.Item("Tag Number") & "'", CompanyName)
                    If eto.IsNew Then
                        eto.m_row.Item("LawTagNum") = lw.m_row.Item("Tag Number")
                        eto.m_row.Item("BOMNo") = r.Item("BOM No_")
                        eto.m_row.Item("LineNum") = r.Item("Line No_")
                        eto.m_row.Item("Amount") = r.Item("Labor Cost")
                        Dim d As Decimal = CDec(r.Item("Quantity"))
                        d = d.Round(d, 4)
                        eto.m_row.Item("Minutes") = CStr(d) 'r.Item("Quantity")

                        eto.m_row.Item("ItemType") = r.Item("Piece Rate Ticket")
                        eto.m_row.Item("ItemNo") = r.Item("No_")
                        ' MsgBox(r.Item("Piece Rate Ticket") & " - " & r.Item("No_"))
                        eto.m_cmd.QuotePrefix = "["
                        eto.m_cmd.QuoteSuffix = "]"
                        eto.update()
                    End If
                    Dim eto2 As New clsEmployeeTearOff("BOMNo ='" & r.Item("BOM No_") & "' and LineNum='" & r.Item("Line No_") & "' and LawTagNum='" & lw.m_row.Item("Tag Number") & "'", CompanyName)
                    drow2.Item("ScanField") = "*" & eto2.m_row.Item("id") & "*"
                    drow3.Item("ScanField") = "*" & eto2.m_row.Item("id") & "*"
                End If


            Next
        Next
        For Each r As DataRow In sl.m_dt.Rows
            r.Item("LawTag") = "*" & lawtag(lw.m_row) & "*"
        Next
        For Each r As DataRow In sl2.m_dt.Rows
            r.Item("LawTag") = "*" & lawtag(lw.m_row) & "*"
        Next
        'sl2.m_ds.WriteXmlSchema("TearOff2.xml")
        'sl.m_ds.WriteXmlSchema("TV2.xml")
        'Return 1



        'Dim crpt As New Object
        'If Module1.frtestmode = 1 Then
        Dim BoxMattTest As New clsItem(line.m_row.Item("No_"))
        'If BoxMattTest.m_row.Item("Item Category Code") = "BOX SPRING" Then
        'Dim crpt As New crptTicketwithFRtagbedinabox
        Dim crpt As New crptTicketwithFRtagKDFrames
        'Else
        '    Dim crpt As New crptTicketwithFR
        'End If
        'Else
        '    crpt = New crptTicket
        'End If
        'Dim subrpt As CrystalDecisions.CrystalReports.Engine.ReportDocument = crpt.OpenSubreport("TearOffs")


        Dim targetform As New frrmCRVProdAnDealer

        targetform.crpt = crpt
        'subrpt.SetDataSource(sl2.m_ds)
        If gitem = "" Then
            gitem = sl.m_row.Item("Item")
        End If
        Dim it As New clsItem(gitem)


        Dim MatArl As New ArrayList
        Me.FindMatSubAssemblies(0, gitem, MatArl)



        Dim bl As New clsBOMLine(" [BOM No_] = '" & it.m_row.Item("BOM No_") & "' and Type = '1' ")
        Dim s As String = ""
        Dim totnetwgt As Decimal = 0
        Dim smaterials As String = ""
        ' For Each bl As clsBOMLineSelect In MatArl
        'Dim cnt As New clsContents(sl.m_row.Item("Item"))
        For Each r As DataRow In bl.m_dt.Rows
            Dim q As Decimal = fixnulldec(r.Item("Quantity"))
            q = q.Round(q, 2)
            smaterials = smaterials & CStr(q) & " " & r.Item("Description") & ControlChars.CrLf
        Next
        '  Next


        If smaterials = "" Then
            smaterials = "No BOM Items"
        End If


        Dim cnt As New clsContents(sl.m_row.Item("Item"))

        For Each r As DataRow In cnt.m_dt.Rows
            'Dim q As Decimal = fixnulldec(r.Item("Quantity"))
            'q = q.Round(q, 2)
            'Mod for temp contents code at JCBedding
            If Not IsDBNull(r.Item("Percent of Total")) Then
                s = s & r.Item("Description") & " " & Decimal.Round(CDec(r.Item("Percent of Total")), 1) & "%" & ControlChars.CrLf
                totnetwgt = totnetwgt + r.Item("ItemNetWgt")
            End If
            's = s & r.Item("Description") & ControlChars.CrLf
        Next

        s = s & "Total Filler Weight: " & Math.Round(totnetwgt) & " lb" & ControlChars.CrLf
        Dim sz As New clsSize(sl.m_row.Item("Size"))
        If (line.m_row.Item("Sell-to Customer No_") = "9083") Or (line.m_row.Item("Sell-to Customer No_") = "9260") Then
            s = "POLYPROPYLENE........42%" & ControlChars.CrLf
            s = s & "POLYESTER..................58%" & ControlChars.CrLf
            s = s & "____________________________" & ControlChars.CrLf
            s = s & "NC STAMP EXEMPTION No.33" & ControlChars.CrLf
        End If
        Dim cr As New CRH(crpt)
        Dim test As Boolean = False
        If Module1.testmode = "1" Then
            test = True
        End If
        ''Dim sp2 As New clsSalesperson(Me.ListBox1.SelectedValue)
        'cr.SetText("txtBOM", s)
        'cr.CRLF("txtMats", smaterials)
        Dim ci As New clsCompanyInfo
        'If line.m_row.Item("Sell-to Customer No_") = "9260" Then
        '    cr.SetText("Text28", "Made For:")
        '    cr.SetText("TxtAddr1", "BEDINABOX LLC")
        '    cr.SetText("TxtAddr2", "220 E. Millard St.")
        '    cr.SetText("TxtAddr3", "Johnson City, TN  37601")
        '    cr.SetText("Text25", "MADE IN USA")
        '    cr.SetText("Text9", "Reg. No. VA17570(TN)")
        'ElseIf line.m_row.Item("Sell-to Customer No_") = "9083" Then
        '    cr.SetText("Text28", "Manufactured by:")
        '    cr.SetText("TxtAddr1", "BEDINABOX LLC")
        '    cr.SetText("TxtAddr2", "220 E. Millard St.")
        '    cr.SetText("TxtAddr3", "Johnson City, TN  37601")
        '    cr.SetText("Text25", "MADE IN USA")
        '    cr.SetText("Text9", "Reg. No. VA17570(TN)")
        'Else
        '    cr.SetText("Text28", "Manufactured by:")
        '    cr.SetText("TxtAddr1", ci.m_row.Item("Law Tag Name"))
        '    cr.SetText("TxtAddr2", ci.m_row.Item("Law Tag Address"))
        '    cr.SetText("TxtAddr3", ci.m_row.Item("Law Tag City") & ", " & ci.m_row.Item("Law Tag State") & " " & ci.m_row.Item("Law Tag Zip"))
        '    cr.SetText("Text9", "Reg. No. VA 216 (TN)")
        'End If
        'cr.SetText("txtCount", CStr(Module1.ticketcounter))
        'cr.SetText("TxtSize", sz.m_row.Item("Size Description"))
        'cr.SetText("txtCount2", CStr(Module1.ticketcounter))

        'If Not lw.IsNew Then
        cr.SetText("txtReprint", "")
        'End If
        targetform.ds = sl.m_ds
        'Me.DataGrid1.DataSource = sl2.m_dt
        'Dim warranty As New clsWarranty(sl.m_row.Item("Item"))

        Dim fp As String = crpt.FilePath
        'MsgBox(fp)
        File.Copy(fp, "Lawtag", True)

        'Try
        'File.Copy(fp, "Lawtag", True)
        'Catch ex As Exception
        '   MsgBox(ex.Message)
        '  End Try
        'MsgBox("Post")

        Dim crpt2 As New CrystalDecisions.CrystalReports.Engine.ReportDocument
        crpt2.Load("Lawtag")

        Dim cr2 As New CRH(crpt2)


        'Below is protype lookup code for FR tag style
        'Will be copied into original at this point when switching to FR style only



        'If True Then
        Dim CFR As String = "This mattress meets the requirements of" & ControlChars.CrLf
        CFR &= "16 CFR Part 1633 (federal flammability" & ControlChars.CrLf
        CFR &= "(open flame) standard for mattress sets)" & ControlChars.CrLf
        CFR &= "when used without a foundation or with" & ControlChars.CrLf
        CFR &= "foundations "

        'Dim dshelper As DatasetHelper.DataSetHelper
        Dim proto As New clsPrototype(it.m_row.Item("Model"))
        Dim proto2 As New clsPrototype2(it.m_row.Item("Model"))
        'dshelper.SelectDistinct("proto3", , "Prototype No_")
        CFR &= proto.FoundationList
        'cr2.CRLF("txtCFR", CFR)
        'cr2.CRLF("txtProto", proto2.ProtoList)
        'cr2.SetText("txtBigFoundation", proto.FoundationList)

        'END PROTOTYPE CODE***************************************************************

        'Else


        '    ''Dim sp2 As New clsSalesperson(Me.ListBox1.SelectedValue)

        '    If Not lw.IsNew Then
        '        cr2.SetText("txtReprint", "REPRINT")
        '    End If
        '    cr2.SetText("txtCount2", CStr(Module1.ticketcounter))

        'End If
        'cr2.SetText("TxtFound", BoxMattTest.m_row.Item("Product Group Code"))
        'cr2.SetText("txtBOM", s)
        'cr2.CRLF("txtMats", smaterials)
        Dim ci2 As New clsCompanyInfo
        'If line.m_row.Item("Sell-to Customer No_") = "9260" Then
        '    cr2.SetText("Text28", "Made For:")
        '    cr2.SetText("TxtAddr1", "BEDINABOX LLC")
        '    cr2.SetText("TxtAddr2", "220 E. Millard St.")
        '    cr2.SetText("TxtAddr3", "Johnson City, TN  37601")
        '    cr2.SetText("Text25", "MADE IN USA")
        '    cr2.SetText("Text9", "Reg. No. VA17570(TN)")
        'ElseIf line.m_row.Item("Sell-to Customer No_") = "9083" Then
        '    cr2.SetText("Text28", "Manufactured by:")
        '    cr2.SetText("TxtAddr1", "BEDINABOX LLC")
        '    cr2.SetText("TxtAddr2", "220 E. Millard St.")
        '    cr2.SetText("TxtAddr3", "Johnson City, TN  37601")
        '    cr2.SetText("Text25", "MADE IN USA")
        '    cr2.SetText("Text9", "Reg. No. VA17570(TN)")
        'Else
        '    cr2.SetText("Text28", "Manufactured by:")
        '    cr2.SetText("TxtAddr1", ci.m_row.Item("Law Tag Name"))
        '    cr2.SetText("TxtAddr2", ci.m_row.Item("Law Tag Address"))
        '    cr2.SetText("TxtAddr3", ci.m_row.Item("Law Tag City") & ", " & ci.m_row.Item("Law Tag State") & " " & ci.m_row.Item("Law Tag Zip"))
        '    cr2.SetText("Text9", "Reg. No. VA 216 (TN)")
        'End If
        'cr2.SetText("txtCount", CStr(Module1.ticketcounter))
        'cr2.SetText("txtCount", CStr(Module1.ticketcounter))
        'cr2.SetText("TxtSize", sz.m_row.Item("Size Description"))
        'cr2.SetText("txtCount2", CStr(Module1.ticketcounter))

        Dim warranty As New clsWarranty(it.m_row.Item("BOM No_"))
        'cr2.SetText("txtWarranty", "Warranty: " & warranty.m_row.Item("Code"))
        targetform.crpt = crpt2

        targetform.ds2 = sl2.m_dt.DataSet
        'If Not lw.IsNew Then
        'cr2.SetText("txtReprint", "REPRINT")
        'End If

        crpt2.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.EnvManual

        If Not test Then
            clsStraighttoPrinterFRstyle.Print(crpt2, sl.m_dt.DataSet, sl2.m_dt.DataSet, sub2.m_dt.DataSet, pd, result)

        Else
            targetform.ShowDialog()
        End If
        crpt.Close()
        targetform.Close()
        crpt2.Close()
        crpt.Dispose()
        targetform.Dispose()
        crpt2.Dispose()
        GC.Collect()


        Return 1


    End Function

    Function lawtag(ByRef r As DataRow) As String
        Return CStr(r.Item("Tag Number"))
    End Function

    Function fixnulldec(ByVal o As Object) As Decimal
        If IsDBNull(o) Then
            Return 0
        End If
        Return o
    End Function

    Function FindSubAssemblies(ByVal counter As Integer, ByVal item As String, ByRef arl As ArrayList) As ArrayList
        'WARNING: Recursion below
        'Returns an array list of clsBOMLine objects
        Dim pre As String = item
        Dim it As New clsItem(item)
        item = it.m_row.Item("BOM No_")
        'MsgBox(pre & " - " & item)
        If counter > 5 Then
            'Sanity check
            ' MsgBox("Too many levels of sub assemblies.")
            Return arl
        End If

        Dim s As String = Module1.CompanyName()
        Dim sl2 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '2' ")
        If Not sl2.IsNew Then
            For Each r As DataRow In sl2.m_dt.Rows
                FindSubAssemblies(counter + 1, r.Item("No_"), arl)
            Next
        End If
        If Not typeother Then
            'Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'  and Item.[Piece Rate Ticket] = '3'")
            Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4' and [" & s & "$Item].[Piece Rate Ticket] > '1'")
            'MsgBox("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'  and Item.[Piece Rate Ticket] = '3'")
            'If Not IsDBNull(sl3.m_row.Item("No_")) Then
            '    Dim i As New clsItem(sl3.m_row.Item("No_"))
            '    If IsDBNull(i.m_row.Item("Piece Rate Ticket")) Then
            '        i.m_row.Item("Piece Rate Ticket") = "0"
            '    End If
            '    MsgBox(sl3.m_row.Item("No_") & " " & i.m_row.Item("Piece Rate Ticket"))
            If (Not sl3.IsNew) Then
                'MsgBox("Adding " & item)
                For Each r As DataRow In sl3.m_dt.Rows
                    '           MsgBox("zz " & r.Item("Inumber") & " - " & r.item("Piece Rate Ticket"))
                Next
                arl.Add(sl3)
                ' End If
            End If
        Else
            Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'  and [" & s & "$Item].[Piece Rate Ticket] = '2'")
            'Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'")
            'If Not IsDBNull(sl3.m_row.Item("No_")) Then
            '    Dim i As New clsItem(sl3.m_row.Item("No_"))
            '    If IsDBNull(i.m_row.Item("Piece Rate Ticket")) Then
            '        i.m_row.Item("Piece Rate Ticket") = "0"
            '    End If
            If (Not sl3.IsNew) Then
                'MsgBox("else Adding " & item)
                For Each r As DataRow In sl3.m_dt.Rows
                    '          MsgBox("zz " & r.Item("Inumber") & " - " & r.item("Piece Rate Ticket"))
                Next
                arl.Add(sl3)
            End If
            For Each r As DataRow In sl3.m_dt.Rows
                If Module1.othertags.Contains(r.Item("")) Then

                End If
            Next
            'End If
        End If
        If Module1.testmode = 1 Then
            ' MsgBox(counter & " - " & pre & " - " & item & " - ")
        End If
    End Function



    Function FindMatSubAssemblies(ByVal counter As Integer, ByVal item As String, ByRef arl As ArrayList) As ArrayList
        'WARNING: Recursion below
        'Returns an array list of clsBOMLine objects
        Dim pre As String = item
        Dim it As New clsItem(item)
        item = it.m_row.Item("BOM No_")
        ' MsgBox(pre & " - " & item)
        If counter > 5 Then
            'Sanity check
            ' MsgBox("Too many levels of sub assemblies.")
            Return arl
        End If

        Dim s As String = Module1.CompanyName()
        Dim sl2 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '2' ")
        If Not sl2.IsNew Then
            For Each r As DataRow In sl2.m_dt.Rows
                FindMatSubAssemblies(counter + 1, r.Item("No_"), arl)
            Next
        End If
        If Not typeother Then
            'Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'  and Item.[Piece Rate Ticket] = '3'")
            Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '1' ")
            'MsgBox("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'  and Item.[Piece Rate Ticket] = '3'")
            'If Not IsDBNull(sl3.m_row.Item("No_")) Then
            '    Dim i As New clsItem(sl3.m_row.Item("No_"))
            '    If IsDBNull(i.m_row.Item("Piece Rate Ticket")) Then
            '        i.m_row.Item("Piece Rate Ticket") = "0"
            '    End If
            '    MsgBox(sl3.m_row.Item("No_") & " " & i.m_row.Item("Piece Rate Ticket"))
            If (Not sl3.IsNew) Then
                'MsgBox("Adding " & item)
                For Each r As DataRow In sl3.m_dt.Rows
                    '           MsgBox("zz " & r.Item("Inumber") & " - " & r.item("Piece Rate Ticket"))
                Next
                arl.Add(sl3)
                ' End If
            End If
        Else
            Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '1'  ")
            'Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'")
            'If Not IsDBNull(sl3.m_row.Item("No_")) Then
            '    Dim i As New clsItem(sl3.m_row.Item("No_"))
            '    If IsDBNull(i.m_row.Item("Piece Rate Ticket")) Then
            '        i.m_row.Item("Piece Rate Ticket") = "0"
            '    End If
            If (Not sl3.IsNew) Then
                'MsgBox("else Adding " & item)
                For Each r As DataRow In sl3.m_dt.Rows
                    'MsgBox("zz " & r.Item("Inumber"))
                Next
                arl.Add(sl3)
            End If
            For Each r As DataRow In sl3.m_dt.Rows
                If Module1.othertags.Contains(r.Item("")) Then

                End If
            Next
            'End If
        End If
        If Module1.testmode = 1 Then
            ' MsgBox(counter & " - " & pre & " - " & item & " - ")
        End If
    End Function



End Class
