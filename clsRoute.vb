Imports DbMaster
Public Class clsRoute

    Inherits clSqlMaster

    Sub New(ByVal arg As String)
        'Passes the table name, and key field to the parent class
        'MyBase.New(arg, "[Holding Company, LLC$Ship-To Route Code]", "[Route Code]")
        MyBase.New(arg, "[" & CompanyName() & "$Ship-To Route Code]", "[Route Code]")
    End Sub
End Class