Imports DbMaster

Public Class clsBOMLine

    Inherits clSqlMaster

    Sub New(ByVal arg As String)
        'Passes the table name, and key field to the parent class
        MyBase.New("[" & CompanyName() & "$BOM Line]", arg)
    End Sub


    Public Function GetManHours() As Decimal
        Dim val As Decimal = 0.0
        If Me.IsNew Then
            Return val
        End If
        'For Each r As DataRow In Me.m_dt.Rows
        '    If Not IsDBNull(r.Item("Labor Cost")) Then
        '        val += CDec(r.Item("Labor Cost"))
        '    End If
        'Next
        Me.FindSubAssemblies(0, Me.m_row.Item("BOM No_"), val)
        Return val
    End Function


    Private Sub FindSubAssemblies(ByVal counter As Integer, ByVal item As String, ByRef total As Decimal)
        'WARNING: Recursion below
        'Returns an array list of clsBOMLine objects
        If counter > 5 Then
            'Sanity check
            MsgBox("Too many levels of sub assemblies.")
            Return
        End If
        Dim s As String = Module1.CompanyName()
        Dim sl2 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '2' ")
        If Not sl2.IsNew Then
            For Each r As DataRow In sl2.m_dt.Rows
                FindSubAssemblies(counter + 1, sl2.m_row.Item("No_"), total)
            Next
        End If
        'Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4'  and Item.[Piece Rate Ticket] = 'L'")
        Dim sl3 As New clsBOMLineSelect("[" & s & "$BOM Line].[BOM No_] = '" & item & "' and Type = '4' and Item.[Piece Rate Ticket] > 1")
        If Not sl3.IsNew Then
            For Each r As DataRow In sl3.m_dt.Rows
                If Not IsDBNull(r.Item("Quantity")) Then
                    total += (CDec(r.Item("Quantity")) / 60)
                End If
            Next
        End If

    End Sub
End Class

