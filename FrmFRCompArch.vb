Imports CRHelper
Public Class FrmFRCompArch
    Inherits System.Windows.Forms.Form

#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Txtdocno As System.Windows.Forms.TextBox
    Friend WithEvents RdoInvoice As System.Windows.Forms.RadioButton
    Friend WithEvents RdoOrder As System.Windows.Forms.RadioButton
    Friend WithEvents cmdPrint As System.Windows.Forms.Button
    Friend WithEvents cmdExit As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.Txtdocno = New System.Windows.Forms.TextBox
        Me.RdoInvoice = New System.Windows.Forms.RadioButton
        Me.RdoOrder = New System.Windows.Forms.RadioButton
        Me.cmdPrint = New System.Windows.Forms.Button
        Me.cmdExit = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Txtdocno
        '
        Me.Txtdocno.Location = New System.Drawing.Point(98, 160)
        Me.Txtdocno.Name = "Txtdocno"
        Me.Txtdocno.TabIndex = 0
        Me.Txtdocno.Text = ""
        '
        'RdoInvoice
        '
        Me.RdoInvoice.Checked = True
        Me.RdoInvoice.Location = New System.Drawing.Point(96, 88)
        Me.RdoInvoice.Name = "RdoInvoice"
        Me.RdoInvoice.Size = New System.Drawing.Size(120, 24)
        Me.RdoInvoice.TabIndex = 1
        Me.RdoInvoice.TabStop = True
        Me.RdoInvoice.Text = "Sales Invoice No."
        '
        'RdoOrder
        '
        Me.RdoOrder.Location = New System.Drawing.Point(96, 112)
        Me.RdoOrder.Name = "RdoOrder"
        Me.RdoOrder.TabIndex = 2
        Me.RdoOrder.Text = "Sales Order No."
        '
        'cmdPrint
        '
        Me.cmdPrint.Location = New System.Drawing.Point(111, 224)
        Me.cmdPrint.Name = "cmdPrint"
        Me.cmdPrint.TabIndex = 3
        Me.cmdPrint.Text = "Print"
        '
        'cmdExit
        '
        Me.cmdExit.Location = New System.Drawing.Point(216, 304)
        Me.cmdExit.Name = "cmdExit"
        Me.cmdExit.TabIndex = 4
        Me.cmdExit.Text = "Exit"
        '
        'FrmFRCompArch
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(296, 334)
        Me.Controls.Add(Me.cmdExit)
        Me.Controls.Add(Me.cmdPrint)
        Me.Controls.Add(Me.RdoOrder)
        Me.Controls.Add(Me.RdoInvoice)
        Me.Controls.Add(Me.Txtdocno)
        Me.Name = "FrmFRCompArch"
        Me.Text = "FR Compliance Cert. Archive"
        Me.ResumeLayout(False)

    End Sub

#End Region

    Private Sub cmdPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdPrint.Click
        Dim printed As Integer = 0
        Dim pd As PrintDialog
        Dim result As DialogResult
        If Module1.pdlt Is Nothing Then
            pd = New PrintDialog
            pd.Document = New System.Drawing.Printing.PrintDocument


            result = pd.ShowDialog()
        Else
            pd = Module1.pdlt
            result = Module1.dresult1
        End If

        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim ds3 As New DataSet
        Dim dt As New DataTable("FRComp")
        dt.Columns.Add("Model", GetType(String))
        dt.Columns.Add("Document No_", GetType(String))
        dt.Columns.Add("Prototype No_", GetType(String))
        dt.Columns.Add("Description", GetType(String))
        dt.Columns.Add("Test", GetType(String))
        dt.Columns.Add("Test Date", GetType(Date))
        dt.Columns.Add("TF Name", GetType(String))
        dt.Columns.Add("TF Address", GetType(String))
        dt.Columns.Add("TF Address2", GetType(String))
        dt.Columns.Add("TF City", GetType(String))
        dt.Columns.Add("TF State", GetType(String))
        dt.Columns.Add("TF Zip", GetType(String))
        dt.Columns.Add("Man Name", GetType(String))
        dt.Columns.Add("Man Address", GetType(String))
        dt.Columns.Add("Man City", GetType(String))
        dt.Columns.Add("Man State", GetType(String))
        dt.Columns.Add("Man Zip", GetType(String))
        dt.Columns.Add("Man Phone", GetType(String))
        dt.Columns.Add("Man Country", GetType(String))
        dt.Columns.Add("Man Site", GetType(String))
        dt.Columns.Add("RK Name", GetType(String))
        dt.Columns.Add("RK Address", GetType(String))
        dt.Columns.Add("RK City", GetType(String))
        dt.Columns.Add("RK State", GetType(String))
        dt.Columns.Add("RK Zip", GetType(String))
        dt.Columns.Add("RK Country", GetType(String))
        dt.Columns.Add("RK Phone", GetType(String))
        dt.Columns.Add("RK Email", GetType(String))
        dt.Columns.Add("Man Date", GetType(Date))
        ds.Tables.Add(dt)
        If Me.RdoInvoice.Checked = True Then
            Dim sih As New ClsSalesInvoiceHeader("[No_] = '" & Me.Txtdocno.Text & "'")
            If sih.IsNew Then
                MsgBox("Sales Invoice No. was not found!")
                Me.Txtdocno.Text = ""
                Return
            Else
                Dim fr As New ClsFRCompArch("[Document No_] = '" & sih.m_row.Item("Order No_") & "'")
                If Not fr.IsNew Then
                    For Each f As DataRow In fr.m_dt.Rows
                        Dim sr As DataRow = dt.NewRow
                        sr.Item("Model") = f.Item("Model")
                        sr.Item("Document No_") = f.Item("Document No_")
                        sr.Item("Prototype No_") = f.Item("Prototype No_")
                        sr.Item("Description") = f.Item("Description")
                        If f.Item("Test") = 1 Then
                            sr.Item("Test") = "16 CFR 1632"
                        End If
                        If f.Item("Test") = 2 Then
                            sr.Item("Test") = "16 CFR 1633"
                        End If
                        sr.Item("Test Date") = f.Item("Test Date")
                        sr.Item("Man Date") = f.Item("Man Date")
                        sr.Item("TF Name") = f.Item("TF Name")
                        sr.Item("TF Address") = f.Item("TF Address")
                        sr.Item("TF Address2") = f.Item("TF Address2")
                        sr.Item("TF City") = f.Item("TF City")
                        sr.Item("TF State") = f.Item("TF State")
                        sr.Item("TF Zip") = f.Item("TF Zip")
                        sr.Item("Man Name") = f.Item("Man Name")
                        sr.Item("Man Address") = f.Item("Man Address")
                        sr.Item("Man City") = f.Item("Man City")
                        sr.Item("Man State") = f.Item("Man State")
                        sr.Item("Man Zip") = f.Item("Man Zip")
                        sr.Item("Man Phone") = f.Item("Man Phone")
                        sr.Item("Man Country") = f.Item("Man Country")
                        sr.Item("Man Site") = f.Item("Man Site")
                        sr.Item("RK Name") = f.Item("RK Name")
                        sr.Item("RK Address") = f.Item("RK Address")
                        sr.Item("RK City") = f.Item("RK City")
                        sr.Item("RK State") = f.Item("RK State")
                        sr.Item("RK Zip") = f.Item("RK Zip")
                        sr.Item("RK Country") = f.Item("RK Country")
                        sr.Item("RK Phone") = f.Item("RK Phone")
                        sr.Item("RK Email") = f.Item("RK Email")
                        dt.Rows.Add(sr)
                    Next
                Else
                    MsgBox("No records found!")
                    Me.Txtdocno.Text = ""
                    Return
                End If
            End If
        Else
            Dim fr As New ClsFRCompArch("[Document No_] = '" & Me.Txtdocno.Text & "'")
            If Not fr.IsNew Then
                For Each f As DataRow In fr.m_dt.Rows
                    Dim sr As DataRow = dt.NewRow
                    sr.Item("Model") = f.Item("Model")
                    sr.Item("Document No_") = f.Item("Document No_")
                    sr.Item("Prototype No_") = f.Item("Prototype No_")
                    sr.Item("Description") = f.Item("Description")
                    If f.Item("Test") = 1 Then
                        sr.Item("Test") = "16 CFR 1632"
                    End If
                    If f.Item("Test") = 2 Then
                        sr.Item("Test") = "16 CFR 1633"
                    End If
                    sr.Item("Test Date") = f.Item("Test Date")
                    sr.Item("Man Date") = f.Item("Man Date")
                    sr.Item("TF Name") = f.Item("TF Name")
                    sr.Item("TF Address") = f.Item("TF Address")
                    sr.Item("TF Address2") = f.Item("TF Address2")
                    sr.Item("TF City") = f.Item("TF City")
                    sr.Item("TF State") = f.Item("TF State")
                    sr.Item("TF Zip") = f.Item("TF Zip")
                    sr.Item("Man Name") = f.Item("Man Name")
                    sr.Item("Man Address") = f.Item("Man Address")
                    sr.Item("Man City") = f.Item("Man City")
                    sr.Item("Man State") = f.Item("Man State")
                    sr.Item("Man Zip") = f.Item("Man Zip")
                    sr.Item("Man Phone") = f.Item("Man Phone")
                    sr.Item("Man Country") = f.Item("Man Country")
                    sr.Item("Man Site") = f.Item("Man Site")
                    sr.Item("RK Name") = f.Item("RK Name")
                    sr.Item("RK Address") = f.Item("RK Address")
                    sr.Item("RK City") = f.Item("RK City")
                    sr.Item("RK State") = f.Item("RK State")
                    sr.Item("RK Zip") = f.Item("RK Zip")
                    sr.Item("RK Country") = f.Item("RK Country")
                    sr.Item("RK Phone") = f.Item("RK Phone")
                    sr.Item("RK Email") = f.Item("RK Email")
                    dt.Rows.Add(sr)
                Next
            Else
                MsgBox("No records found!")
                Me.Txtdocno.Text = ""
                Return
            End If
        End If
        Dim targetform As New frrmCRVProdAnDealer
        '    Dim xz As New crptSalespersonDealer

        'targetform.crpt = New crptSalespersonDealer
        targetform.crpt = New CrptFRComp
        'targetform.filename = "FRTrack.pdf"
        Dim cr As New CRH(targetform.crpt)
        'cr.SetText("txtTitle", "FR Tracking by Prototype ID")
        'cr.SetText("txtDesc1", "Prototype # " & Me.TxtProto.Text)
        'cr.SetText("txtDesc2", "Date: " & Me.TxtProtoDate.Text)

        targetform.ds = ds
        'clsStraightToPrinter.Print(targetform.crpt, ds, ds2, ds3, pd, result)
        targetform.crpt.SetDataSource(ds)


        Dim Printer As String = pd.Document.PrinterSettings.PrinterName
        Dim Copies As String = pd.Document.PrinterSettings.Copies

        Dim FromPage As Integer = pd.Document.PrinterSettings.FromPage
        Dim ToPage As Integer = pd.Document.PrinterSettings.ToPage




        If (result = DialogResult.OK) Then
            targetform.crpt.PrintOptions.PrinterName = Printer
            'crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Manual
            'The case statement below should not make any difference, but I left it in case we need it sometime
            Select Case Module1.lawtagtray
                Case "Manual"
                    ' MsgBox("Manual")
                    targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Manual
                Case "LargeCapacity"
                    'MsgBox("LargeCapacity")
                    targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.LargeCapacity
                Case "Lower"
                    'MsgBox("Lower")
                    targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Lower
                Case "Middle"
                    'MsgBox("Middle")
                    targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Middle
                Case "Upper"
                    'MsgBox("Upper")
                    targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Upper
                Case "Auto"
                    'MsgBox("Auto")
                    targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Auto
            End Select

            If Module1.testmode = 0 Then
                targetform.crpt.PrintToPrinter(Copies, False, FromPage, ToPage)

            End If
        End If
    End Sub

    Private Sub cmdExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdExit.Click
        Me.Close()
    End Sub
End Class
