'Lawtag and bulk printing form
Imports CRHelper

Public Class Form2
    Inherits System.Windows.Forms.Form
    Public rc As Integer
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents btnSingle As Button
    Friend WithEvents txtSingle As TextBox
    Friend WithEvents Button20 As Button
    Friend WithEvents cbxVariant As CheckBox
    Public m As String
#Region " Windows Form Designer generated code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Not (components Is Nothing) Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents cboStopYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboStopDay As System.Windows.Forms.ComboBox
    Friend WithEvents cboStopMonth As System.Windows.Forms.ComboBox
    Friend WithEvents cboStartYear As System.Windows.Forms.ComboBox
    Friend WithEvents cboStartDay As System.Windows.Forms.ComboBox
    Friend WithEvents cboStartMonth As System.Windows.Forms.ComboBox
    Friend WithEvents lblStop As System.Windows.Forms.Label
    Friend WithEvents lblStart As System.Windows.Forms.Label
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents DataGrid1 As System.Windows.Forms.DataGrid
    Friend WithEvents MainMenu1 As System.Windows.Forms.MainMenu
    Friend WithEvents MenuItem1 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem2 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem3 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem4 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem5 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem6 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem7 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem10 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem8 As System.Windows.Forms.MenuItem
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cbxShowall As System.Windows.Forms.CheckBox
    Friend WithEvents MenuItem9 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem11 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem12 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem13 As System.Windows.Forms.MenuItem
    Friend WithEvents MenuItem14 As System.Windows.Forms.MenuItem
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtLawTag As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cmdLawTag As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Button17 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.cboStopYear = New System.Windows.Forms.ComboBox()
        Me.cboStopDay = New System.Windows.Forms.ComboBox()
        Me.cboStopMonth = New System.Windows.Forms.ComboBox()
        Me.cboStartYear = New System.Windows.Forms.ComboBox()
        Me.cboStartDay = New System.Windows.Forms.ComboBox()
        Me.cboStartMonth = New System.Windows.Forms.ComboBox()
        Me.lblStop = New System.Windows.Forms.Label()
        Me.lblStart = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.DataGrid1 = New System.Windows.Forms.DataGrid()
        Me.MainMenu1 = New System.Windows.Forms.MainMenu(Me.components)
        Me.MenuItem1 = New System.Windows.Forms.MenuItem()
        Me.MenuItem2 = New System.Windows.Forms.MenuItem()
        Me.MenuItem3 = New System.Windows.Forms.MenuItem()
        Me.MenuItem4 = New System.Windows.Forms.MenuItem()
        Me.MenuItem5 = New System.Windows.Forms.MenuItem()
        Me.MenuItem6 = New System.Windows.Forms.MenuItem()
        Me.MenuItem7 = New System.Windows.Forms.MenuItem()
        Me.MenuItem10 = New System.Windows.Forms.MenuItem()
        Me.MenuItem8 = New System.Windows.Forms.MenuItem()
        Me.MenuItem11 = New System.Windows.Forms.MenuItem()
        Me.MenuItem12 = New System.Windows.Forms.MenuItem()
        Me.MenuItem13 = New System.Windows.Forms.MenuItem()
        Me.MenuItem14 = New System.Windows.Forms.MenuItem()
        Me.MenuItem9 = New System.Windows.Forms.MenuItem()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbxShowall = New System.Windows.Forms.CheckBox()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtLawTag = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.cmdLawTag = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.btnSingle = New System.Windows.Forms.Button()
        Me.txtSingle = New System.Windows.Forms.TextBox()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.cbxVariant = New System.Windows.Forms.CheckBox()
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(760, 173)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(272, 45)
        Me.Button4.TabIndex = 42
        Me.Button4.Text = "Print selected orders"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(760, 48)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(272, 40)
        Me.Button2.TabIndex = 40
        Me.Button2.Text = "Print All"
        '
        'Label1
        '
        Me.Label1.Location = New System.Drawing.Point(482, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(180, 43)
        Me.Label1.TabIndex = 37
        Me.Label1.Text = "Select Route:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(760, 109)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(272, 45)
        Me.Button1.TabIndex = 36
        Me.Button1.Text = "Print for selected route"
        '
        'cboStopYear
        '
        Me.cboStopYear.Items.AddRange(New Object() {"2009", "2010", "2011", "2012", "2013", "2014", "2015"})
        Me.cboStopYear.Location = New System.Drawing.Point(293, 133)
        Me.cboStopYear.Name = "cboStopYear"
        Me.cboStopYear.Size = New System.Drawing.Size(129, 33)
        Me.cboStopYear.TabIndex = 33
        '
        'cboStopDay
        '
        Me.cboStopDay.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})
        Me.cboStopDay.Location = New System.Drawing.Point(168, 128)
        Me.cboStopDay.Name = "cboStopDay"
        Me.cboStopDay.Size = New System.Drawing.Size(80, 33)
        Me.cboStopDay.TabIndex = 32
        '
        'cboStopMonth
        '
        Me.cboStopMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cboStopMonth.Location = New System.Drawing.Point(22, 128)
        Me.cboStopMonth.Name = "cboStopMonth"
        Me.cboStopMonth.Size = New System.Drawing.Size(111, 33)
        Me.cboStopMonth.TabIndex = 31
        '
        'cboStartYear
        '
        Me.cboStartYear.Items.AddRange(New Object() {"2009", "2010", "2011", "2012", "2013", "2014", "2015"})
        Me.cboStartYear.Location = New System.Drawing.Point(288, 51)
        Me.cboStartYear.Name = "cboStartYear"
        Me.cboStartYear.Size = New System.Drawing.Size(127, 33)
        Me.cboStartYear.TabIndex = 30
        '
        'cboStartDay
        '
        Me.cboStartDay.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})
        Me.cboStartDay.Location = New System.Drawing.Point(168, 51)
        Me.cboStartDay.Name = "cboStartDay"
        Me.cboStartDay.Size = New System.Drawing.Size(80, 33)
        Me.cboStartDay.TabIndex = 29
        '
        'cboStartMonth
        '
        Me.cboStartMonth.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"})
        Me.cboStartMonth.Location = New System.Drawing.Point(22, 51)
        Me.cboStartMonth.Name = "cboStartMonth"
        Me.cboStartMonth.Size = New System.Drawing.Size(111, 33)
        Me.cboStartMonth.TabIndex = 28
        '
        'lblStop
        '
        Me.lblStop.Location = New System.Drawing.Point(22, 91)
        Me.lblStop.Name = "lblStop"
        Me.lblStop.Size = New System.Drawing.Size(200, 40)
        Me.lblStop.TabIndex = 27
        Me.lblStop.Text = "Stop:"
        '
        'lblStart
        '
        Me.lblStart.Location = New System.Drawing.Point(35, 13)
        Me.lblStart.Name = "lblStart"
        Me.lblStart.Size = New System.Drawing.Size(200, 32)
        Me.lblStart.TabIndex = 26
        Me.lblStart.Text = "Start:"
        '
        'ListBox1
        '
        Me.ListBox1.ItemHeight = 25
        Me.ListBox1.Location = New System.Drawing.Point(488, 45)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(240, 104)
        Me.ListBox1.TabIndex = 25
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(13, 218)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(209, 43)
        Me.btnSearch.TabIndex = 34
        Me.btnSearch.Text = "Search Records"
        '
        'DataGrid1
        '
        Me.DataGrid1.AlternatingBackColor = System.Drawing.Color.Lavender
        Me.DataGrid1.BackColor = System.Drawing.Color.WhiteSmoke
        Me.DataGrid1.BackgroundColor = System.Drawing.Color.White
        Me.DataGrid1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DataGrid1.CaptionBackColor = System.Drawing.Color.Khaki
        Me.DataGrid1.CaptionForeColor = System.Drawing.Color.MidnightBlue
        Me.DataGrid1.DataMember = ""
        Me.DataGrid1.FlatMode = True
        Me.DataGrid1.Font = New System.Drawing.Font("Tahoma", 8.0!)
        Me.DataGrid1.ForeColor = System.Drawing.Color.MidnightBlue
        Me.DataGrid1.GridLineColor = System.Drawing.Color.Gainsboro
        Me.DataGrid1.GridLineStyle = System.Windows.Forms.DataGridLineStyle.None
        Me.DataGrid1.HeaderBackColor = System.Drawing.Color.MidnightBlue
        Me.DataGrid1.HeaderFont = New System.Drawing.Font("Tahoma", 8.0!, System.Drawing.FontStyle.Bold)
        Me.DataGrid1.HeaderForeColor = System.Drawing.Color.WhiteSmoke
        Me.DataGrid1.LinkColor = System.Drawing.Color.Teal
        Me.DataGrid1.Location = New System.Drawing.Point(-80, 293)
        Me.DataGrid1.Name = "DataGrid1"
        Me.DataGrid1.ParentRowsBackColor = System.Drawing.Color.Gainsboro
        Me.DataGrid1.ParentRowsForeColor = System.Drawing.Color.MidnightBlue
        Me.DataGrid1.PreferredColumnWidth = 95
        Me.DataGrid1.ReadOnly = True
        Me.DataGrid1.RowHeaderWidth = 95
        Me.DataGrid1.SelectionBackColor = System.Drawing.Color.CadetBlue
        Me.DataGrid1.SelectionForeColor = System.Drawing.Color.WhiteSmoke
        Me.DataGrid1.Size = New System.Drawing.Size(1732, 374)
        Me.DataGrid1.TabIndex = 44
        Me.DataGrid1.TabStop = False
        '
        'MainMenu1
        '
        Me.MainMenu1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem1, Me.MenuItem11, Me.MenuItem9})
        '
        'MenuItem1
        '
        Me.MenuItem1.Index = 0
        Me.MenuItem1.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem2, Me.MenuItem3, Me.MenuItem4, Me.MenuItem5, Me.MenuItem6, Me.MenuItem7, Me.MenuItem10, Me.MenuItem8})
        Me.MenuItem1.Text = "Tools"
        '
        'MenuItem2
        '
        Me.MenuItem2.Index = 0
        Me.MenuItem2.Text = "Route summary"
        '
        'MenuItem3
        '
        Me.MenuItem3.Index = 1
        Me.MenuItem3.Text = "Production summary"
        '
        'MenuItem4
        '
        Me.MenuItem4.Index = 2
        Me.MenuItem4.Text = "Route details"
        '
        'MenuItem5
        '
        Me.MenuItem5.Index = 3
        Me.MenuItem5.Text = "Production details"
        '
        'MenuItem6
        '
        Me.MenuItem6.Index = 4
        Me.MenuItem6.Text = "Weekly report"
        '
        'MenuItem7
        '
        Me.MenuItem7.Enabled = False
        Me.MenuItem7.Index = 5
        Me.MenuItem7.Text = "Law tags"
        '
        'MenuItem10
        '
        Me.MenuItem10.Index = 6
        Me.MenuItem10.Text = "-"
        '
        'MenuItem8
        '
        Me.MenuItem8.Index = 7
        Me.MenuItem8.Text = "Exit application"
        '
        'MenuItem11
        '
        Me.MenuItem11.Index = 1
        Me.MenuItem11.MenuItems.AddRange(New System.Windows.Forms.MenuItem() {Me.MenuItem12, Me.MenuItem13, Me.MenuItem14})
        Me.MenuItem11.Text = "View"
        '
        'MenuItem12
        '
        Me.MenuItem12.Index = 0
        Me.MenuItem12.Text = "Color"
        '
        'MenuItem13
        '
        Me.MenuItem13.Index = 1
        Me.MenuItem13.Text = "Sequence"
        '
        'MenuItem14
        '
        Me.MenuItem14.Index = 2
        Me.MenuItem14.Text = "Day of week"
        '
        'MenuItem9
        '
        Me.MenuItem9.Index = 2
        Me.MenuItem9.Text = ""
        '
        'CheckBox1
        '
        Me.CheckBox1.Location = New System.Drawing.Point(35, 166)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(208, 45)
        Me.CheckBox1.TabIndex = 45
        Me.CheckBox1.Text = "Print 'other' type"
        Me.CheckBox1.Visible = False
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(1852, 520)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(271, 43)
        Me.Button3.TabIndex = 46
        Me.Button3.Text = "Other for selected"
        Me.Button3.Visible = False
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(1852, 565)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(271, 45)
        Me.Button5.TabIndex = 47
        Me.Button5.Text = "Other for route"
        Me.Button5.Visible = False
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(1762, 670)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(271, 40)
        Me.Button6.TabIndex = 48
        Me.Button6.Text = "Other all"
        Me.Button6.Visible = False
        '
        'ListBox2
        '
        Me.ListBox2.ItemHeight = 25
        Me.ListBox2.Items.AddRange(New Object() {"All", "Law Tags", "Piece Rate Tickets", "Other Tags", "Encasement Report", "Insulator Report", "Quilting Report", "Production Report", "Manpower Report", "ManpowerNR Report", "AssemblyFE Report", "AssemblyIS Report", "AssemblySP Report", "PickList Report"})
        Me.ListBox2.Location = New System.Drawing.Point(1737, 77)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple
        Me.ListBox2.Size = New System.Drawing.Size(240, 79)
        Me.ListBox2.TabIndex = 50
        Me.ListBox2.Visible = False
        '
        'Button7
        '
        Me.Button7.Enabled = False
        Me.Button7.Location = New System.Drawing.Point(1748, 283)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(209, 42)
        Me.Button7.TabIndex = 51
        Me.Button7.Text = "Bulk print all"
        Me.Button7.Visible = False
        '
        'Button8
        '
        Me.Button8.Enabled = False
        Me.Button8.Location = New System.Drawing.Point(1748, 346)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(209, 75)
        Me.Button8.TabIndex = 52
        Me.Button8.Text = "Bulk print selected route"
        Me.Button8.Visible = False
        '
        'Button9
        '
        Me.Button9.Enabled = False
        Me.Button9.Location = New System.Drawing.Point(1748, 435)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(209, 74)
        Me.Button9.TabIndex = 53
        Me.Button9.Text = "Bulk print selected orders"
        Me.Button9.Visible = False
        '
        'Button10
        '
        Me.Button10.Location = New System.Drawing.Point(1762, 13)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(176, 43)
        Me.Button10.TabIndex = 54
        Me.Button10.Text = "Select printers"
        Me.Button10.Visible = False
        '
        'Label3
        '
        Me.Label3.Location = New System.Drawing.Point(1777, 614)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(110, 44)
        Me.Label3.TabIndex = 55
        Me.Label3.Text = "V .995"
        Me.Label3.Visible = False
        '
        'cbxShowall
        '
        Me.cbxShowall.Location = New System.Drawing.Point(1748, 576)
        Me.cbxShowall.Name = "cbxShowall"
        Me.cbxShowall.Size = New System.Drawing.Size(209, 29)
        Me.cbxShowall.TabIndex = 56
        Me.cbxShowall.Text = "Show all fields"
        Me.cbxShowall.Visible = False
        '
        'Button11
        '
        Me.Button11.Location = New System.Drawing.Point(1497, 243)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(150, 43)
        Me.Button11.TabIndex = 57
        Me.Button11.Text = "By Lines"
        '
        'Button13
        '
        Me.Button13.Location = New System.Drawing.Point(1070, 179)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(270, 45)
        Me.Button13.TabIndex = 61
        Me.Button13.Text = "Print selected orders"
        '
        'Button14
        '
        Me.Button14.Location = New System.Drawing.Point(1070, 51)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(270, 42)
        Me.Button14.TabIndex = 60
        Me.Button14.Text = "Print All"
        '
        'Button15
        '
        Me.Button15.Location = New System.Drawing.Point(1070, 109)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(270, 45)
        Me.Button15.TabIndex = 59
        Me.Button15.Text = "Print for selected route"
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(813, 10)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(167, 36)
        Me.Label4.TabIndex = 62
        Me.Label4.Text = "Law Tags"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label5
        '
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(1082, 13)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(255, 37)
        Me.Label5.TabIndex = 63
        Me.Label5.Text = "Piece Rate Tickets"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.8!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(1383, 13)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(257, 37)
        Me.Label6.TabIndex = 64
        Me.Label6.Text = "FR Compliance Cert."
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'txtLawTag
        '
        Me.txtLawTag.Location = New System.Drawing.Point(143, 680)
        Me.txtLawTag.Name = "txtLawTag"
        Me.txtLawTag.Size = New System.Drawing.Size(200, 31)
        Me.txtLawTag.TabIndex = 65
        '
        'Label7
        '
        Me.Label7.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Label7.Location = New System.Drawing.Point(0, 680)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(128, 42)
        Me.Label7.TabIndex = 66
        Me.Label7.Text = "Law Tag #"
        '
        'cmdLawTag
        '
        Me.cmdLawTag.Location = New System.Drawing.Point(368, 680)
        Me.cmdLawTag.Name = "cmdLawTag"
        Me.cmdLawTag.Size = New System.Drawing.Size(400, 42)
        Me.cmdLawTag.TabIndex = 67
        Me.cmdLawTag.Text = "Reprint Law Tag by No."
        '
        'Button12
        '
        Me.Button12.Location = New System.Drawing.Point(1377, 51)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(271, 43)
        Me.Button12.TabIndex = 68
        Me.Button12.Text = "Print All"
        '
        'Button16
        '
        Me.Button16.Location = New System.Drawing.Point(1377, 178)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(271, 41)
        Me.Button16.TabIndex = 69
        Me.Button16.Text = "Print Selected Orders"
        '
        'Button17
        '
        Me.Button17.Location = New System.Drawing.Point(960, 680)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(320, 42)
        Me.Button17.TabIndex = 70
        Me.Button17.Text = "FR Compliance Cert. Archive"
        '
        'Button18
        '
        Me.Button18.Location = New System.Drawing.Point(1377, 109)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(270, 45)
        Me.Button18.TabIndex = 71
        Me.Button18.Text = "Print for selected route"
        '
        'Label2
        '
        Me.Label2.Location = New System.Drawing.Point(203, 2)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(257, 43)
        Me.Label2.TabIndex = 72
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Button19
        '
        Me.Button19.Location = New System.Drawing.Point(1332, 680)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(320, 42)
        Me.Button19.TabIndex = 73
        Me.Button19.Text = "BiB Test Labels"
        '
        'btnSingle
        '
        Me.btnSingle.Location = New System.Drawing.Point(1070, 240)
        Me.btnSingle.Name = "btnSingle"
        Me.btnSingle.Size = New System.Drawing.Size(125, 43)
        Me.btnSingle.TabIndex = 74
        Me.btnSingle.Text = "Single"
        Me.btnSingle.UseVisualStyleBackColor = True
        '
        'txtSingle
        '
        Me.txtSingle.Location = New System.Drawing.Point(1225, 243)
        Me.txtSingle.Name = "txtSingle"
        Me.txtSingle.Size = New System.Drawing.Size(262, 31)
        Me.txtSingle.TabIndex = 75
        '
        'Button20
        '
        Me.Button20.Location = New System.Drawing.Point(603, 218)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(125, 43)
        Me.Button20.TabIndex = 76
        Me.Button20.Text = "SpringAir"
        Me.Button20.UseVisualStyleBackColor = True
        '
        'cbxVariant
        '
        Me.cbxVariant.AutoSize = True
        Me.cbxVariant.Location = New System.Drawing.Point(293, 218)
        Me.cbxVariant.Name = "cbxVariant"
        Me.cbxVariant.Size = New System.Drawing.Size(118, 29)
        Me.cbxVariant.TabIndex = 77
        Me.cbxVariant.Text = "SA only"
        Me.cbxVariant.UseVisualStyleBackColor = True
        '
        'Form2
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(10, 24)
        Me.ClientSize = New System.Drawing.Size(2102, 800)
        Me.Controls.Add(Me.cbxVariant)
        Me.Controls.Add(Me.Button20)
        Me.Controls.Add(Me.txtSingle)
        Me.Controls.Add(Me.btnSingle)
        Me.Controls.Add(Me.Button19)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button18)
        Me.Controls.Add(Me.Button17)
        Me.Controls.Add(Me.Button16)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.cmdLawTag)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.txtLawTag)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.Button15)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.cbxShowall)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.DataGrid1)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.cboStopYear)
        Me.Controls.Add(Me.cboStopDay)
        Me.Controls.Add(Me.cboStopMonth)
        Me.Controls.Add(Me.cboStartYear)
        Me.Controls.Add(Me.cboStartDay)
        Me.Controls.Add(Me.cboStartMonth)
        Me.Controls.Add(Me.lblStop)
        Me.Controls.Add(Me.lblStart)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.btnSearch)
        Me.Menu = Me.MainMenu1
        Me.Name = "Form2"
        Me.Text = "Ticket Printing"
        CType(Me.DataGrid1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region

    Private Sub Form2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim pg As New clsRoute("")
        Dim dt3 As New DataTable
        dt3 = pg.distinct("[Route Code]")
        Me.ListBox1.DisplayMember = "Route Code"
        Me.ListBox1.ValueMember = "Route Code"
        Me.ListBox1.DataSource = dt3

        Me.cboStartYear.DataSource = Module1.DropBoxYears
        Me.cboStopYear.DataSource = Module1.DropBoxYears

        'Me.Button1.Enabled = False

        Me.cboStartDay.Text = Date.Now.Day
        Me.cboStartMonth.Text = Date.Now.Month
        Me.cboStartYear.Text = Date.Now.Year

        Me.cboStopDay.Text = Date.Now.Day
        Me.cboStopMonth.Text = Date.Now.Month
        Me.cboStopYear.Text = Date.Now.Year
    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal

        Dim saOnly As String = ""


        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text

        'Shipment Date for JCB
        'Dim o As New clsOrderRoute("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "'")
        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "'" & saOnly)
        'Shipment Date for JCB
        If cbxVariant.Checked Then
            o = New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "'" & saOnly, True)
        End If
        'MsgBox("Search " & o.m_dt.Rows.Count)


        Dim d As New DateTime(cboStartYear.Text, cboStartMonth.Text, cboStartDay.Text)

        'Code to hide columns on data grid
        Dim aGridTableStyle As New DataGridTableStyle
        aGridTableStyle.MappingName = "PView2"
        '
        ' Create GridColumnStyle objects for the grid columns 
        '
        aGridTableStyle.AlternatingBackColor = Color.Cornsilk
        aGridTableStyle.HeaderBackColor() = Color.Beige
        aGridTableStyle.AllowSorting = True


        Dim arl As New ArrayList


        Dim colhide As New Hashtable
        'colhide.Add("HeaderID", 1)
        'colhide.Add("Length", 1)
        colhide.Add("Ship to Route Code1", 1)

        'Menu item view holds list of items to hide

        If Not Me.MenuItem14.Checked Then
            colhide.Add("Day of Week", 1)
        End If
        If Not Me.MenuItem12.Checked Then
            colhide.Add("Color Code", 1)
        End If
        If Not Me.MenuItem13.Checked Then
            colhide.Add("Sequence", 1)
        End If
        'Dim colformat As New Hashtable
        'colformat.Add("QtyToShip", "#0")
        'colformat.Add("QtyRemaining", "#0")
        ''colformat.Add("Compress", "#0.00")

        For Each c As DataColumn In o.m_dt.Columns
            Dim aCol1 As New DataGridTextBoxColumn

            aCol1.MappingName = c.ColumnName
            aCol1.HeaderText = c.ColumnName
            'aCol1.Format = "#0.00"
            aCol1.Width = 70
            If Not Me.cbxShowall.Checked Then
                If colhide.Contains(c.ColumnName) Then
                    aCol1.Width = 0
                End If
            End If
            'If colformat.Contains(c.ColumnName) Then
            '    aCol1.Format = colformat(c.ColumnName)
            'End If
            arl.Add(aCol1)
        Next
        For Each acol As DataGridTextBoxColumn In arl
            aGridTableStyle.GridColumnStyles.Add(acol)
        Next

        Me.DataGrid1.TableStyles.Clear()
        Me.DataGrid1.TableStyles.Add(aGridTableStyle)
        'Loop through rows and set up some of the columns
        Me.rc = o.m_dt.Rows.Count
        For Each r As DataRow In o.m_dt.Rows
            r.Item("Pcs") = Decimal.Round(CDec(r.Item("Pcs")), 0)
            Dim lt As New clsLawTag(" [Document No_] = '" & r.Item("No_") & "'")
            If lt.IsNew Then
                r.Item("LawPrint") = "No"
                r.Item("OthPrint") = "No"
            Else
                r.Item("LawPrint") = "Yes"
                r.Item("OthPrint") = "No"
                Dim empty As Boolean = True
                For Each ltr As DataRow In lt.m_dt.Rows

                    If r.Item("OthPrint") = "No" Then
                        Dim olt As New clsOtherList(" LawTagNum = '" & ltr.Item("Tag Number") & "' and ItemType = '2'")
                        If Not olt.IsNew Then
                            'Do any others esist?
                            empty = False
                        End If
                        Dim ol As New clsOtherList(" LawTagNum = '" & ltr.Item("Tag Number") & "' and ItemType = '2' and Printed = 'Y'")
                        If Not ol.IsNew Then
                            'Have they been printed?

                            r.Item("OthPrint") = "Yes"
                        End If
                    End If
                Next
                If empty Then
                    'There are no oyher tags to print
                    r.Item("OthPrint") = "N/A"
                End If
            End If
        Next
        If cbxVariant.Checked Then

        End If
        'Dim o As New clsOrder("[Sell-to Customer No_] = '" & Me.ListBox1.SelectedValue & "'")

        'If Not IsDBNull(o.m_row.Item("QtyToShip")) Then
        Me.DataGrid1.DataSource = o.m_dt


        If o.IsNew Then
            MessageBox.Show("No rows available with selected query. Please try different dates.")
            Me.Button1.Enabled = False

        End If



    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        'The Print all button

        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal

        Module1.ticketcounter = 0


        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text
        Me.Label2.Text = "Working..."


        'Shipment Date for JCB
        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "'")
        'Dim o As New clsOrderRoute("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "'")
        'Shipment Date for JCB

        If Not o.IsNew Then
            Dim printed As Integer = 0
            Dim pd As PrintDialog
            Dim result As DialogResult
            If Module1.pdlt Is Nothing Then
                pd = New PrintDialog
                pd.UseEXDialog = True
                pd.Document = New System.Drawing.Printing.PrintDocument


                result = pd.ShowDialog()
            Else
                pd = Module1.pdlt
                result = Module1.dresult1
            End If

            Dim arl As New ArrayList

            For Each r As DataRow In o.m_dt.Rows
                Dim l As New clsSalesLine(" [Document No_] = '" & r.Item("No_") & "'")
                For Each lr As DataRow In l.m_dt.Rows
                    If lr.Item("Type") = 2 Then
                        'MsgBox(lr.Item("Line No_"))
                        If Not IsDBNull(lr.Item("Quantity")) Then

                            Dim quantity As Integer = CInt(lr.Item("Quantity"))
                            'quantity = 2
                            arl.Add(r.Item("No_"))
                            printed += lr.Item("Quantity")
                            printed = 0
                            For x As Integer = 1 To quantity
                                'jel 20070511 added the call to the item table and this if to send data
                                Dim itemno As String = lr.Item("No_")
                                Dim j As New clsItem(itemno)
                                For Each jr As DataRow In j.m_dt.Rows
                                    'If (Mid$(jr.Item("No_"), 1, 2) = "01") Or (Mid$(jr.Item("No_"), 1, 2) = "02") Or (Mid$(jr.Item("No_"), 1, 2) = "03") Or (Mid$(jr.Item("No_"), 1, 2) = "05") Or (Mid$(jr.Item("No_"), 1, 2) = "14") Or (Mid$(jr.Item("No_"), 1, 2) = "14") Or (Mid$(jr.Item("No_"), 1, 2) = "08") Then
                                    'If jr.Item("FR Lawtag") = 1 Then
                                    Dim itcat As New clsItemCat(jr.Item("Item Category Code"))
                                    If Not itcat.IsNew Then
                                        If (lr.Item("Sell-to Customer No_") = "9083" And itemno.Trim.Substring(0, 2) = "08") Then
                                            If itcat.m_row.Item("Law Tag Style") = 1 Then
                                                Dim gt As New clsGenerateTagFRstyleBox(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                                printed += gt.PrintIt()
                                            End If
                                            If itcat.m_row.Item("Law Tag Style") = 0 Then
                                                Dim gt As New clsGenerateTagFRstylebedinabox(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                                printed += gt.PrintIt()
                                            End If
                                            'Dim gt As New clsGenerateTagFRstylebedinabox(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                            'printed += gt.PrintIt()
                                        ElseIf lr.Item("Sell-to Customer No_").ToString.Trim = "9260" Then
                                            Dim gt As New clsGenerateTagKDFrames(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                            printed += gt.PrintIt()
                                        ElseIf (lr.Item("Sell-to Customer No_").ToString.Trim = "9085") And (itemno.Contains("01618") Or itemno.Contains("584")) Then
                                            Dim gt As New clsBernhardt(lr.Item("Document No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                            printed += gt.PrintIt()
                                        Else
                                            If itcat.m_row.Item("Law Tag Style") = 1 Then
                                                Dim gt As New clsGenerateTagFRstyleBox(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                                printed += gt.PrintIt()
                                            End If
                                            If itcat.m_row.Item("Law Tag Style") = 0 Then
                                                Dim gt As New clsGenerateTagFRstyle(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                                printed += gt.PrintIt()
                                            End If
                                        End If
                                    End If
                                    'Else
                                    'Dim gt As New clsGenerateTag(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                    'printed += gt.PrintIt()
                                    'End If
                                    'End If
                                Next

                                'Dim gt As New clsGenerateTag(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                'printed += gt.PrintIt()
                            Next
                        End If
                    End If
                Next

            Next
            If Module1.pdlt Is Nothing Then
                MsgBox(printed & " Items printed")
            End If
        Else
            If Module1.pdlt Is Nothing Then
                MsgBox("No orders found for this date range")
            End If
        End If
        Me.Label2.Text = ""
        Module1.ticketcounter = 0

        If Module1.pdlt Is Nothing Then
            Me.btnSearch.PerformClick()
        End If

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal

        Module1.ticketcounter = 0


        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text

        Me.Label2.Text = "Working..."

        'Shipment Date for JCB
        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "' and [Route Code] = '" & Me.ListBox1.SelectedValue & "'")
        'Dim o As New clsOrderRoute("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "' and [Route Code] = '" & Me.ListBox1.SelectedValue & "'")
        If Not o.IsNew Then
            Dim printed As Integer = 0
            Dim pd As PrintDialog
            Dim result As DialogResult
            If Module1.pdlt Is Nothing Then
                pd = New PrintDialog
                pd.UseEXDialog = True
                pd.Document = New System.Drawing.Printing.PrintDocument


                result = pd.ShowDialog()
            Else
                pd = Module1.pdlt
                result = Module1.dresult1
            End If
            Dim arl As New ArrayList

            For Each r As DataRow In o.m_dt.Rows
                Dim l As New clsSalesLine(" [Document No_] = '" & r.Item("No_") & "'")
                For Each lr As DataRow In l.m_dt.Rows
                    If lr.Item("Type") = 2 Then
                        'MsgBox(lr.Item("Line No_"))
                        If Not IsDBNull(lr.Item("Quantity")) Then


                            Dim quantity As Integer = CInt(lr.Item("Quantity"))
                            'quantity = 2
                            printed += lr.Item("Quantity")
                            printed = 0
                            arl.Add(r.Item("No_"))
                            For x As Integer = 1 To quantity
                                'jel 20070511 added the call to the item table and this if to send data
                                Dim itemno As String = lr.Item("No_")
                                Dim j As New clsItem(itemno)
                                For Each jr As DataRow In j.m_dt.Rows
                                    Dim itcat As New clsItemCat(jr.Item("Item Category Code"))
                                    If Not itcat.IsNew Then
                                        'If itcat.m_row.Item("Law Tag Style") = 1 Then
                                        '    Dim gt As New clsGenerateTagFRstyleBox(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                        '    printed += gt.PrintIt()
                                        'End If
                                        'If itcat.m_row.Item("Law Tag Style") = 0 Then
                                        '    Dim gt As New clsGenerateTagFRstyle(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                        '    printed += gt.PrintIt()
                                        'End If
                                        If lr.Item("Sell-to Customer No_") = "9083" And itemno.Trim.Substring(0, 2) = "08" Then
                                            If itcat.m_row.Item("Law Tag Style") = 0 Then
                                                Dim gt As New clsGenerateTagFRstylebedinabox(lr.Item("Document No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                                printed += gt.PrintIt()
                                            End If
                                            If itcat.m_row.Item("Law Tag Style") = 1 Then
                                                Dim gt As New clsGenerateTagFRstyleBox(lr.Item("Document No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                                printed += gt.PrintIt()
                                            End If
                                        ElseIf lr.Item("Sell-to Customer No_").ToString.Trim = "9260" Then
                                            Dim gt As New clsGenerateTagKDFrames(lr.Item("Document No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                            printed += gt.PrintIt()
                                        ElseIf (lr.Item("Sell-to Customer No_").ToString.Trim = "9085") And (itemno.Contains("01618") Or itemno.Contains("584")) Then
                                            Dim gt As New clsBernhardt(lr.Item("Document No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                            printed += gt.PrintIt()
                                        Else
                                            If itcat.m_row.Item("Law Tag Style") = 1 Then
                                                Dim gt As New clsGenerateTagFRstyleBox(lr.Item("Document No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                                printed += gt.PrintIt()
                                            End If
                                            If itcat.m_row.Item("Law Tag Style") = 0 Then
                                                Dim gt As New clsGenerateTagFRstyle(lr.Item("Document No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                                printed += gt.PrintIt()
                                            End If
                                        End If
                                    End If

                                Next

                                'Dim gt As New clsGenerateTag(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                'printed += gt.PrintIt()
                            Next
                        End If
                    End If
                Next

            Next
            If Module1.pdlt Is Nothing Then
                MsgBox(printed & " Items printed")
            End If
        Else
            If Module1.pdlt Is Nothing Then
                MsgBox("No orders found for this date range")
            End If
        End If
        Me.Label2.Text = ""
        Module1.ticketcounter = 0

        If Module1.pdlt Is Nothing Then
            Me.btnSearch.PerformClick()
        End If
    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal
        Module1.ticketcounter = 0

        'Module1.PrintSet.WriteXmlSchema("PrintSet.xml")
        'Return

        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text
        Me.Label2.Text = "Working..."

        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "'")
        'Dim o As New clsOrderRoute("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "'")
        'MsgBox("LT " & o.m_dt.Rows.Count)
        Dim printed As Integer = 0
        Dim pd As PrintDialog
        Dim result As DialogResult
        If Module1.pdlt Is Nothing Then
            'MsgBox("Print Dialog")
            pd = New PrintDialog
            pd.UseEXDialog = True
            pd.Document = New System.Drawing.Printing.PrintDocument
            pd.Document.DocumentName = "Freedom - Laser Law Label"

            result = pd.ShowDialog()
        Else
            'MsgBox("No Print Dialog")
            pd = Module1.pdlt
            result = Module1.dresult1
        End If

        Dim arl As New ArrayList
        For x As Integer = 0 To (o.m_dt.Rows.Count - 1)
            'Try
            If Me.DataGrid1.IsSelected(x) Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                's represents the order number
                Dim l As New clsSalesLine(" [Document No_] = '" & s & "'")
                For Each lr As DataRow In l.m_dt.Rows
                    If lr.Item("Type") = 2 Then

                        'MsgBox(s)
                        If Not IsDBNull(lr.Item("Quantity")) Then
                            Dim quantity As Integer = CInt(lr.Item("Quantity"))
                            'quantity = 2
                            arl.Add(s)
                            For y As Integer = 1 To quantity
                                'jel 20070511 added the call to the item table and this if to send data
                                Dim itemno As String = lr.Item("No_")
                                Dim j As New clsItem(itemno)
                                For Each jr As DataRow In j.m_dt.Rows
                                    Dim itcat As New clsItemCat(jr.Item("Item Category Code"))
                                    If Not itcat.IsNew Then
                                        If lr.Item("Sell-to Customer No_") = "9083" And itemno.Trim.Substring(0, 2) = "08" Then
                                            If itcat.m_row.Item("Law Tag Style") = 0 Then
                                                Dim gt As New clsGenerateTagFRstylebedinabox(lr.Item("Document No_"), lr.Item("Line No_"), CStr(y), pd, result, Me.CheckBox1.Checked)
                                                printed += gt.PrintIt()
                                            End If
                                            If itcat.m_row.Item("Law Tag Style") = 1 Then
                                                Dim gt As New clsGenerateTagFRstyleBox(lr.Item("Document No_"), lr.Item("Line No_"), CStr(y), pd, result, Me.CheckBox1.Checked)
                                                printed += gt.PrintIt()
                                            End If
                                        ElseIf lr.Item("Sell-to Customer No_").ToString.Trim = "9260" Then
                                            Dim gt As New clsGenerateTagKDFrames(lr.Item("Document No_"), lr.Item("Line No_"), CStr(y), pd, result, Me.CheckBox1.Checked)
                                            printed += gt.PrintIt()
                                        ElseIf (lr.Item("Sell-to Customer No_").ToString.Trim = "9085") And (itemno.Contains("01618") Or itemno.Contains("584")) Then
                                            Dim gt As New clsBernhardt(lr.Item("Document No_"), lr.Item("Line No_"), CStr(y), pd, result, Me.CheckBox1.Checked)
                                            printed += gt.PrintIt()
                                        Else
                                            If itcat.m_row.Item("Law Tag Style") = 1 Then
                                                Dim gt As New clsGenerateTagFRstyleBox(lr.Item("Document No_"), lr.Item("Line No_"), CStr(y), pd, result, Me.CheckBox1.Checked)
                                                printed += gt.PrintIt()
                                            End If
                                            If itcat.m_row.Item("Law Tag Style") = 0 Then
                                                Dim gt As New clsGenerateTagFRstyle(lr.Item("Document No_"), lr.Item("Line No_"), CStr(y), pd, result, Me.CheckBox1.Checked)
                                                printed += gt.PrintIt()
                                            End If
                                        End If
                                    End If

                                Next



                            Next
                        End If
                    End If
                Next
            End If
            'Catch ex As Exception
            'MsgBox(ex.Message)
            'MsgBox(ex.Source)
            'MsgBox(ex.StackTrace)
            'MsgBox(ex.TargetSite)
            'MsgBox(ex.InnerException)
            'MsgBox(ex.HelpLink)
            'End Try

        Next
        Me.Label2.Text = ""
        If Module1.pdlt Is Nothing Then
            MsgBox(printed & " Items printed")
        End If

        'Dim f As New frrmCRVProdAnDealer
        'f.crpt = New crptPrintSet
        'f.ds = Module1.PrintSet
        'f.ShowDialog()



        'Module1.ticketcounter = 0
        'Module1.PrintSet.Clear()
        'Module1.pshash.Clear()
        If Module1.pdlt Is Nothing Then
            Me.btnSearch.PerformClick()
        End If
    End Sub

    Private Sub MenuItem3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem3.Click
        Dim f As New frmHoursSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem2.Click
        Dim f As New RouteSummary
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem4.Click
        Dim f As New Form1
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem5.Click
        Dim f As New Form3
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem6.Click
        Dim f As New frmProdSched
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim f As New frmAddRoute
        Me.Hide()
        f.ShowDialog()
        Me.Show()
    End Sub

    Private Sub MenuItem8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem8.Click
        Application.Exit()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal



        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text

        Dim ogo As clsGenerateOthers
        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "'")
        'Dim o As New clsOrderRoute("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "'")
        'MsgBox("OT " & o.m_dt.Rows.Count)

        Dim printed As Integer = 0
        Dim pd As PrintDialog
        Dim result As DialogResult
        If Module1.pdtherest Is Nothing Then
            pd = New PrintDialog
            pd.UseEXDialog = True
            pd.Document = New System.Drawing.Printing.PrintDocument


            result = pd.ShowDialog()
        Else
            pd = Module1.pdtherest
            result = Module1.dresult2
        End If
        Dim arl As New ArrayList



        For x As Integer = 0 To (o.m_dt.Rows.Count - 1)
            Try

                If Me.DataGrid1.IsSelected(x) Then
                    Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                    arl.Add(s)

                    Dim l As New clsSalesLine(" [Document No_] = '" & s & "'")
                    For Each lr As DataRow In l.m_dt.Rows
                        'MsgBox(s)
                        If Not IsDBNull(lr.Item("Quantity")) Then
                            Dim quantity As Integer = CInt(lr.Item("Quantity"))
                            'quantity = 2

                            'For y As Integer = 1 To quantity
                            '    'Dim gt As New clsGenerateOthers(s, lr.Item("Line No_"), CStr(y), pd, result, Me.CheckBox1.Checked)
                            '    'gt.LoadIt()
                            '    ' printed += lr.Item("Quantity")
                            '    'ogo = gt
                            'Next
                        End If
                    Next
                End If
            Catch
            End Try

        Next


        Dim count As Integer = 0
        Dim arl2 As New ArrayList
        For Each s As String In arl
            arl2.Add("'" & s & "'")
            '    count += 1
        Next

        Dim gt As New clsGenerateOthers(pd, result)
        If arl.Count > 0 Then
            gt.PrintIt(arl2)
            If Module1.pdlt Is Nothing Then
                MsgBox("Others printed for " & Join(arl.ToArray, " - "))
            End If
        Else
            If Module1.pdlt Is Nothing Then
                MsgBox("No orders selected")
            End If
        End If
        If Module1.pdlt Is Nothing Then
            Me.btnSearch.PerformClick()
        End If
    End Sub

    Private Sub DataGrid1_Navigate(ByVal sender As System.Object, ByVal ne As System.Windows.Forms.NavigateEventArgs) Handles DataGrid1.Navigate

    End Sub

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal



        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text


        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "' and [Route Code] = '" & Me.ListBox1.SelectedValue & "'")
        'Dim o As New clsOrderRoute("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "' and [Route Code] = '" & Me.ListBox1.SelectedValue & "'")
        If Not o.IsNew Then
            Dim printed As Integer = 0
            Dim pd As PrintDialog
            Dim result As DialogResult
            If Module1.pdtherest Is Nothing Then
                pd = New PrintDialog
                pd.UseEXDialog = True
                pd.Document = New System.Drawing.Printing.PrintDocument


                result = pd.ShowDialog()
            Else
                pd = Module1.pdtherest
                result = Module1.dresult2
            End If
            'Dim result As DialogResult = pd.ShowDialog()
            Dim arl As New ArrayList

            For Each r As DataRow In o.m_dt.Rows
                Dim l As New clsSalesLine(" [Document No_] = '" & r.Item("No_") & "'")
                arl.Add(r.Item("No_"))
                For Each lr As DataRow In l.m_dt.Rows
                    'MsgBox(lr.Item("Line No_"))
                    If Not IsDBNull(lr.Item("Quantity")) Then


                        Dim quantity As Integer = CInt(lr.Item("Quantity"))
                        'quantity = 2
                        'For x As Integer = 1 To quantity
                        '    Dim gt As New clsGenerateTag(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                        '    gt.PrintIt()
                        '    printed += lr.Item("Quantity")
                        'Next
                    End If
                Next

            Next
            Dim count As Integer = 0
            Dim arl2 As New ArrayList
            For Each s As String In arl
                arl2.Add("'" & s & "'")
                '    count += 1
            Next

            Dim gt As New clsGenerateOthers(pd, result)
            gt.PrintIt(arl2)


            If Module1.pdlt Is Nothing Then
                MsgBox("Others printed for route " & Me.ListBox1.SelectedValue)
            End If
        Else
            If Module1.pdlt Is Nothing Then
                MsgBox("No orders found for this date range")
            End If
        End If
        If Module1.pdlt Is Nothing Then
            Me.btnSearch.PerformClick()
        End If

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal



        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text


        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "'")
        'Dim o As New clsOrderRoute("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "'")
        If Not o.IsNew Then
            Dim printed As Integer = 0
            Dim pd As PrintDialog
            Dim result As DialogResult
            If Module1.pdtherest Is Nothing Then
                pd = New PrintDialog
                pd.UseEXDialog = True
                pd.Document = New System.Drawing.Printing.PrintDocument


                result = pd.ShowDialog()
            Else
                pd = Module1.pdtherest
                result = Module1.dresult2
            End If
            Dim arl As New ArrayList

            For Each r As DataRow In o.m_dt.Rows
                arl.Add(r.Item("No_"))
                Dim l As New clsSalesLine(" [Document No_] = '" & r.Item("No_") & "'")
                For Each lr As DataRow In l.m_dt.Rows
                    'MsgBox(lr.Item("Line No_"))
                    If Not IsDBNull(lr.Item("Quantity")) Then

                        Dim quantity As Integer = CInt(lr.Item("Quantity"))
                        'quantity = 2
                        'For x As Integer = 1 To quantity
                        '    Dim gt As New clsGenerateTag(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                        '    gt.PrintIt()
                        '    printed += lr.Item("Quantity")
                        'Next
                    End If
                Next

            Next
            Dim count As Integer = 0
            Dim arl2 As New ArrayList
            For Each s As String In arl
                arl2.Add("'" & s & "'")
                '    count += 1
            Next

            Dim gt As New clsGenerateOthers(pd, result)
            gt.PrintIt(arl2)


            If Module1.pdlt Is Nothing Then
                MsgBox("Others printed for all ")
            End If
        Else
            If Module1.pdlt Is Nothing Then
                MsgBox("No orders found for this date range")
            End If
        End If
        If Module1.pdlt Is Nothing Then
            Me.btnSearch.PerformClick()
        End If
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        For Each o As Object In Me.ListBox2.SelectedItems
            Dim s As String = CStr(o)
            If s = "Law Tags" Or s = "All" Then
                Me.Button2.PerformClick()
            End If
            If s = "Other Tags" Or s = "All" Then
                Me.Button6.PerformClick()
            End If
            If s = "Piece Rate Tickets" Or s = "All" Then
                Me.Button14.PerformClick()
            End If
            If s = "Encasement Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Encasement"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "Insulator Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Insulator"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "Quilting Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Quilting Pattern"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()

                Dim f2 As New frmDynamicReports

                f2.report = "ZMachine"
                f2.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f2.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f2.route = "All"
                If Module1.testmode = 0 Then
                    f2.chkStraightToPrinter.Checked = True
                End If
                f2.ShowDialog()



            End If
            If s = "Production Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Production"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "Manpower Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Manpower"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "ManpowerNR Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "ManpowerNR"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "AssemblyFE Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "AssemblyFE"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "AssemblyIS Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "AssemblyIS"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If

            If s = "AssemblySP Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "AssemblySP"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If

            If s = "PickList Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "PickList"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
        Next
        Me.Label2.Text = ""
    End Sub

    Private Sub Button10_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button10.Click
        MsgBox("Select printer for LawTags")
        Dim pd1 As New PrintDialog
        pd1.UseEXDialog = True

        pd1.Document = New System.Drawing.Printing.PrintDocument
        Dim result1 As DialogResult = pd1.ShowDialog()

        MsgBox("Select printer for other reports")
        Dim pd2 As New PrintDialog
        pd2.UseEXDialog = True
        pd2.Document = New System.Drawing.Printing.PrintDocument
        Dim result2 As DialogResult = pd2.ShowDialog()
        If (result1 = DialogResult.OK) And (result2 = DialogResult.OK) Then
            Module1.pdlt = pd1
            Module1.pdtherest = pd2
            Module1.dresult1 = result1
            Module1.dresult2 = result2
            Me.Button7.Enabled = True
            Me.Button8.Enabled = True
            Me.Button9.Enabled = True
        End If

    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        For Each o As Object In Me.ListBox2.SelectedItems
            Dim s As String = CStr(o)
            If s = "Law Tags" Or s = "All" Then
                Me.Button1.PerformClick()
            End If
            If s = "Other Tags" Or s = "All" Then
                Me.Button5.PerformClick()
            End If
            If s = "Piece Rate Tickets" Or s = "All" Then
                Me.Button15.PerformClick()
            End If
            If s = "Encasement Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Encasement"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = Me.ListBox1.SelectedValue
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "Insulator Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Insulator"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = Me.ListBox1.SelectedValue
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "Quilting Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Quilting Pattern"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = Me.ListBox1.SelectedValue
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()



                Dim f2 As New frmDynamicReports

                f2.report = "ZMachine"
                f2.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f2.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f2.route = Me.ListBox1.SelectedValue
                If Module1.testmode = 0 Then
                    f2.chkStraightToPrinter.Checked = True
                End If
                f2.ShowDialog()


            End If
            If s = "Production Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Production"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = Me.ListBox1.SelectedValue
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "Manpower Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Manpower"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = Me.ListBox1.SelectedValue
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "ManpowerNR Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "ManpowerNR"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = Me.ListBox1.SelectedValue
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If

            If s = "AssemblyFE Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Assembly FE"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = Me.ListBox1.SelectedValue
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "AssemblyIS Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "AssemblyIS"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = Me.ListBox1.SelectedValue
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "AssemblySP Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "AssemblySP"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = Me.ListBox1.SelectedValue
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If
            If s = "PickList Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "PickList"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = Me.ListBox1.SelectedValue
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
            End If

        Next
        Me.Label2.Text = ""
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        Dim OrderList As String = ""
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal


        Module1.ticketcounter = 0
        Module1.PrintSet.Clear()

        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text
        Me.Label2.Text = "Working..."


        Dim ord As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "'")
        'Dim ord As New clsOrderRoute("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "'")

        Dim arl As New ArrayList
        For x As Integer = 0 To (ord.m_dt.Rows.Count - 1)
            Try
                If Me.DataGrid1.IsSelected(x) Then
                    Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                    arl.Add("'" & s & "'")
                End If
            Catch
            End Try
        Next
        If arl.Count > 0 Then
            OrderList = "(" & Join(arl.ToArray, ",") & ")"
            For Each s As String In arl
                Dim sl As New clsTagView(" OrderNum=" & s & " ")
                Module1.PrintSetAddRow(sl.m_row)

            Next

        End If
        If OrderList.Length > 0 Then
            OrderList = " and  dbo.[" & Module1.CompanyName & "$BOM Explosion].[Document No_] in" & OrderList
        End If


        For Each o As Object In Me.ListBox2.SelectedItems
            Dim s As String = CStr(o)
            If s = "Law Tags" Or s = "All" Then
                Me.Button4.PerformClick()
            End If
            If s = "Other Tags" Or s = "All" Then
                Me.Button3.PerformClick()
            End If
            If s = "Piece Rate Tickets" Or s = "All" Then
                Me.Button13.PerformClick()
            End If
            If s = "Encasement Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Encasement"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                f.OrderList = OrderList
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
                Me.Label2.Text = "Encasement done"
            End If
            If s = "Insulator Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Insulator"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                f.OrderList = OrderList
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
                Me.Label2.Text = "Insulator Done"
            End If
            If s = "Quilting Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Quilting Pattern"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                f.OrderList = OrderList
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()

                Dim f2 As New frmDynamicReports
                f2.report = "ZMachine"
                f2.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f2.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f2.route = "All"
                f2.OrderList = OrderList
                If Module1.testmode = 0 Then
                    f2.chkStraightToPrinter.Checked = True
                End If
                f2.ShowDialog()



                Me.Label2.Text = "Quilting done"
            End If

            If s = "Production Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Production"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                f.OrderList = OrderList
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
                Me.Label2.Text = "Production done"
            End If
            If s = "Manpower Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "Manpower"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                f.OrderList = OrderList
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
                Me.Label2.Text = "Manpower done"
            End If
            If s = "ManpowerNR Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "ManpowerNR"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                f.OrderList = OrderList
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
                Me.Label2.Text = "Manpower NR done"
            End If

            If s = "AssemblyFE Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "AssemblyFE"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                f.OrderList = OrderList
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
                Me.Label2.Text = "Assembly FE done"
            End If
            If s = "AssemblyIS Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "AssemblyIS"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                f.OrderList = OrderList
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
                Me.Label2.Text = "Assembly IS done"
            End If

            If s = "AssemblySP Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "AssemblySP"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                f.OrderList = OrderList
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
                Me.Label2.Text = "Assembly SP done"
            End If

            If s = "PickList Report" Or s = "All" Then
                Dim f As New frmDynamicReports
                f.report = "PickList"
                f.DateTimePicker1.Value = Date.Parse(Me.cboStartMonth.Text & "/" & Me.cboStartDay.Text & "/" & Me.cboStartYear.Text)
                f.DateTimePicker2.Value = Date.Parse(Me.cboStopMonth.Text & "/" & Me.cboStopDay.Text & "/" & Me.cboStopYear.Text)
                f.route = "All"
                f.OrderList = OrderList
                If Module1.testmode = 0 Then
                    f.chkStraightToPrinter.Checked = True
                End If
                f.ShowDialog()
                Me.Label2.Text = "Picklist done"
            End If

        Next
        If Module1.testmode = 0 Then
            Dim crpt As New crptPrintSet
            Dim Printer As String = Module1.pdtherest.Document.PrinterSettings.PrinterName
            crpt.PrintOptions.PrinterName = Printer

            crpt.SetDataSource(Module1.PrintSet)
            crpt.PrintToPrinter(1, False, 1, 3)
        End If


        Module1.ticketcounter = 0
        Module1.PrintSet.Clear()
        Module1.pshash.Clear()


        Me.Label2.Text = "Finished " & Date.Now.ToShortTimeString
    End Sub

    Private Sub MenuItem12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem12.Click

        If Me.MenuItem12.Checked Then
            Me.MenuItem12.Checked = False
        Else
            Me.MenuItem12.Checked = True
        End If
    End Sub

    Private Sub MenuItem13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem13.Click

        If Me.MenuItem13.Checked Then
            Me.MenuItem13.Checked = False
        Else
            Me.MenuItem13.Checked = True
        End If
    End Sub

    Private Sub MenuItem14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MenuItem14.Click

        If Me.MenuItem14.Checked Then
            Me.MenuItem14.Checked = False
        Else
            Me.MenuItem14.Checked = True
        End If
    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub Button11_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button11.Click
        Dim f As New frmByLines

        For x As Integer = 0 To (Me.rc - 1)
            'Try
            If Me.DataGrid1.IsSelected(x) Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))

                f.ordernum = s
                f.ShowDialog()

            End If
            'Catch
            'End Try

        Next
    End Sub

    Private Sub Button12_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal
        Module1.ticketcounter = 0

        'Module1.PrintSet.WriteXmlSchema("PrintSet.xml")
        'Return

        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text
        Me.Label2.Text = "Working..."


        'Dim o As New clsOrderRoute("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "'")
        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "'")

        'MsgBox("LT " & o.m_dt.Rows.Count)
        Dim printed As Integer = 0
        Dim pd As PrintDialog
        Dim result As DialogResult
        If Module1.pdtherest Is Nothing Then
            pd = New PrintDialog
            pd.UseEXDialog = True
            pd.Document = New System.Drawing.Printing.PrintDocument
            ' pd.Document.DocumentName = "Freedom - Laser Law Label"

            result = pd.ShowDialog()
        Else
            pd = Module1.pdtherest
            result = Module1.dresult2
        End If

        Dim arl As New ArrayList
        For x As Integer = 0 To (o.m_dt.Rows.Count - 1)
            Try
                If Me.DataGrid1.IsSelected(x) Then
                    Dim s As String = CStr(Me.DataGrid1.Item(x, 0))

                    Dim l As New clsSalesLine(" [Document No_] = '" & s & "'")
                    For Each r As DataRow In l.m_dt.Rows
                        Dim i As New clsItem(r.Item("No_"))

                        'If r.Item("FR") Then
                        Dim lw As New clsLawTag("[Document No_] ='" & s & "' and [Line No_]='" & r.Item("Line No_") & "'")
                        If Not lw.IsNew Then
                            For Each lwr As DataRow In lw.m_dt.Rows

                                Dim ds As New DataSet
                                Dim dt As New DataTable
                                dt.Columns.Add("Entry No_", GetType(String))
                                dt.Columns.Add("Lawtag No_", GetType(String))
                                dt.Columns.Add("Item No_", GetType(String))
                                'dt.Columns.Add("Vendor Lot No_", GetType(String))
                                ds.Tables.Add(dt)

                                Dim nr As DataRow = dt.NewRow
                                nr.Item("Item No_") = i.m_row.Item("No_")
                                nr.Item("Entry No_") = s
                                nr.Item("Lawtag No_") = lwr.Item("Tag Number")
                                dt.Rows.Add(nr)
                                'ds.WriteXmlSchema("SubLot.xml")

                                'ds.Tables.Add(dt)
                                Dim crpt As New crptFRSheet
                                crpt.SetDataSource(ds)
                                clsStraightToPrinterDR.Print(crpt, ds, pd, result)


                            Next
                        End If

                    Next

                End If
            Catch
            End Try

        Next
        Me.Label2.Text = ""
        If Module1.pdlt Is Nothing Then
            ' MsgBox(printed & " Items printed")
        End If


    End Sub

    Private Sub Button14_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button14.Click
        'The Print all button

        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal

        Module1.ticketcounter = 0


        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text
        Me.Label2.Text = "Working..."



        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "'")
        'Dim o As New clsOrderRoute("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "'")

        If Not o.IsNew Then
            Dim printed As Integer = 0
            Dim pd As PrintDialog
            Dim result As DialogResult
            If Module1.pdlt Is Nothing Then
                pd = New PrintDialog
                pd.UseEXDialog = True
                pd.Document = New System.Drawing.Printing.PrintDocument


                result = pd.ShowDialog()
            Else
                pd = Module1.pdlt
                result = Module1.dresult1
            End If

            Dim arl As New ArrayList

            For Each r As DataRow In o.m_dt.Rows
                Dim l As New clsSalesLine(" [Document No_] = '" & r.Item("No_") & "'")
                For Each lr As DataRow In l.m_dt.Rows
                    If lr.Item("Type") Then
                        'MsgBox(lr.Item("Line No_"))
                        If Not IsDBNull(lr.Item("Quantity")) Then

                            Dim quantity As Integer = CInt(lr.Item("Quantity"))
                            'quantity = 2
                            arl.Add(r.Item("No_"))
                            printed += lr.Item("Quantity")
                            printed = 0
                            For x As Integer = 1 To quantity
                                Dim itcat As New clsItemCat(lr.Item("Item Category Code"))
                                If Not itcat.IsNew Then
                                    If itcat.m_row.Item("Law Tag Style") <> 2 Then

                                        'jel 20070511 added the call to the item table and this if to send data
                                        'Dim itemno As String = lr.Item("No_")
                                        'Dim j As New clsItem(itemno)
                                        'For Each jr As DataRow In j.m_dt.Rows
                                        'If jr.Item("FR Lawtag") = 1 Then
                                        Dim gt As New clsGeneratePieceRateTickets(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                        printed += gt.PrintIt()
                                        'Else
                                        '   Dim gt As New clsGenerateTag(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                        '    printed += gt.PrintIt()
                                        'End If
                                        'Next

                                        'Dim gt As New clsGenerateTag(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                        'printed += gt.PrintIt()
                                    End If
                                End If
                            Next
                        End If
                    End If
                Next

            Next
            If Module1.pdlt Is Nothing Then
                MsgBox(printed & " Items printed")
            End If
        Else
            If Module1.pdlt Is Nothing Then
                MsgBox("No orders found for this date range")
            End If
        End If
        Me.Label2.Text = ""
        Module1.ticketcounter = 0

        If Module1.pdlt Is Nothing Then
            Me.btnSearch.PerformClick()
        End If

    End Sub

    Private Sub Button15_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button15.Click
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal

        Module1.ticketcounter = 0


        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text

        Me.Label2.Text = "Working..."


        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "' and [Route Code] = '" & Me.ListBox1.SelectedValue & "'")
        'Dim o As New clsOrderRoute("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "' and [Route Code] = '" & Me.ListBox1.SelectedValue & "'")

        If Not o.IsNew Then
            Dim printed As Integer = 0
            Dim pd As PrintDialog
            Dim result As DialogResult
            If Module1.pdlt Is Nothing Then
                pd = New PrintDialog
                pd.UseEXDialog = True
                pd.Document = New System.Drawing.Printing.PrintDocument


                result = pd.ShowDialog()
            Else
                pd = Module1.pdlt
                result = Module1.dresult1
            End If
            Dim arl As New ArrayList

            For Each r As DataRow In o.m_dt.Rows
                Dim l As New clsSalesLine(" [Document No_] = '" & r.Item("No_") & "'")
                For Each lr As DataRow In l.m_dt.Rows
                    If lr.Item("Type") Then
                        'MsgBox(lr.Item("Line No_"))
                        If Not IsDBNull(lr.Item("Quantity")) Then


                            Dim quantity As Integer = CInt(lr.Item("Quantity"))
                            'quantity = 2
                            printed += lr.Item("Quantity")
                            printed = 0
                            arl.Add(r.Item("No_"))
                            For x As Integer = 1 To quantity
                                Dim itcat As New clsItemCat(lr.Item("Item Category Code"))
                                If Not itcat.IsNew Then
                                    If itcat.m_row.Item("Law Tag Style") <> 2 Then

                                        'jel 20070511 added the call to the item table and this if to send data
                                        'Dim itemno As String = lr.Item("No_")
                                        'Dim j As New clsItem(itemno)
                                        'For Each jr As DataRow In j.m_dt.Rows
                                        'If jr.Item("FR Lawtag") = 1 Then
                                        Dim gt As New clsGeneratePieceRateTickets(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                        printed += gt.PrintIt()
                                        'Else
                                        '    Dim gt As New clsGenerateTag(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                                        '    printed += gt.PrintIt()
                                        'End If
                                        'Next
                                    End If
                                End If
                            Next
                        End If
                    End If
                Next

            Next
            If Module1.pdlt Is Nothing Then
                MsgBox(printed & " Items printed")
            End If
        Else
            If Module1.pdlt Is Nothing Then
                MsgBox("No orders found for this date range")
            End If
        End If
        Me.Label2.Text = ""
        Module1.ticketcounter = 0

        If Module1.pdlt Is Nothing Then
            Me.btnSearch.PerformClick()
        End If

    End Sub

    Private Sub Button13_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button13.Click
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal
        Module1.ticketcounter = 0

        'Module1.PrintSet.WriteXmlSchema("PrintSet.xml")
        'Return

        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text
        Me.Label2.Text = "Working..."


        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "'")
        'Dim o As New clsOrderRoute("[Production Date] >= '" & strStartDate & "' and [Production Date] <= '" & strEndDate & "'")

        'MsgBox("LT " & o.m_dt.Rows.Count)
        Dim printed As Integer = 0
        Dim pd As PrintDialog
        Dim result As DialogResult
        If Module1.pdlt Is Nothing Then
            pd = New PrintDialog
            pd.UseEXDialog = True
            pd.Document = New System.Drawing.Printing.PrintDocument
            pd.Document.DocumentName = "Freedom - Laser Law Label"

            result = pd.ShowDialog()
        Else
            pd = Module1.pdlt
            result = Module1.dresult1
        End If

        Dim arl As New ArrayList
        For x As Integer = 0 To (o.m_dt.Rows.Count - 1)
            'Try
            If Me.DataGrid1.IsSelected(x) Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                's represents the order number
                Dim l As New clsSalesLine(" [Document No_] = '" & s & "'")
                For Each lr As DataRow In l.m_dt.Rows
                    If lr.Item("Type") = 2 Then

                        'MsgBox(s)
                        If Not IsDBNull(lr.Item("Quantity")) Then
                            Dim quantity As Integer = CInt(lr.Item("Quantity"))
                            'quantity = 2
                            arl.Add(s)
                            For y As Integer = 1 To quantity
                                Dim itcat As New clsItemCat(lr.Item("Item Category Code"))
                                If Not itcat.IsNew Then
                                    If itcat.m_row.Item("Law Tag Style") <> 2 Then


                                        'jel 20070511 added the call to the item table and this if to send data
                                        'Dim itemno As String = lr.Item("No_")
                                        'Dim j As New clsItem(itemno)
                                        'For Each jr As DataRow In j.m_dt.Rows
                                        'If jr.Item("FR Lawtag") = 1 Then
                                        Dim gt As New clsGeneratePieceRateTickets(s, lr.Item("Line No_"), CStr(y), pd, result, Me.CheckBox1.Checked)
                                        printed += gt.PrintIt()
                                        'Else
                                        '    Dim gt As New clsGenerateTag(s, lr.Item("Line No_"), CStr(y), pd, result, Me.CheckBox1.Checked)
                                        '    printed += gt.PrintIt()
                                        'End If
                                        'Next
                                    End If
                                End If

                            Next
                        End If
                    End If
                Next
            End If
            'Catch
            'End Try

        Next
        Me.Label2.Text = ""
        If Module1.pdlt Is Nothing Then
            MsgBox(printed & " Items printed")
        End If

        'Dim f As New frrmCRVProdAnDealer
        'f.crpt = New crptPrintSet
        'f.ds = Module1.PrintSet
        'f.ShowDialog()



        'Module1.ticketcounter = 0
        'Module1.PrintSet.Clear()
        'Module1.pshash.Clear()
        If Module1.pdlt Is Nothing Then
            Me.btnSearch.PerformClick()
        End If

    End Sub

    Private Sub cmdLawTag_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdLawTag.Click
        'The Print by tag no. button

        Module1.ticketcounter = 0



        Me.Label2.Text = "Working..."


        Dim lt As New clsLawTag("[Tag Number] = " & Me.txtLawTag.Text)

        If lt.IsNew Then
            MsgBox("Law Tag No. not found")
            Me.txtLawTag.Text = ""
            Me.txtLawTag.Focus()
            Return
        End If

        Dim sh As New clsSalesHeader("[No_] = '" & lt.m_row.Item("Document No_") & "'")
        If sh.IsNew Then
            MsgBox("Sales Order has been completely invoiced.  Tag Print Unavailable.")
            Me.txtLawTag.Text = ""
            Me.txtLawTag.Focus()
            Return
        End If

        Dim sl As New clsSalesLine("[Document No_] = '" & lt.m_row.Item("Document No_") & "' AND [Line No_] = " & lt.m_row.Item("Line No_"))

        Dim printed As Integer = 0
        Dim pd As PrintDialog
        Dim result As DialogResult
        If Module1.pdlt Is Nothing Then
            pd = New PrintDialog
            pd.UseEXDialog = True
            pd.Document = New System.Drawing.Printing.PrintDocument


            result = pd.ShowDialog()
        Else
            pd = Module1.pdlt
            result = Module1.dresult1
        End If


        Dim x As Integer = lt.m_row.Item("Instance")
        'quantity = 2
        'jel 20070511 added the call to the item table and this if to send data
        Dim itemno As String = sl.m_row.Item("No_")
        Dim j As New clsItem(itemno)
        For Each jr As DataRow In j.m_dt.Rows
            'If jr.Item("FR Lawtag") = 1 Then
            Dim itcat As New clsItemCat(jr.Item("Item Category Code"))
            If Not itcat.IsNew Then
                If sl.m_row.Item("Sell-to Customer No_") = "9083" And itemno.Trim.Substring(0, 2) = "08" Then
                    Dim gt As New clsGenerateTagFRstylebedinabox(sh.m_row.Item("No_"), lt.m_row.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                    printed += gt.PrintIt()
                Else
                    If itcat.m_row.Item("Law Tag Style") = 1 Then
                        Dim gt As New clsGenerateTagFRstyleBox(sh.m_row.Item("No_"), lt.m_row.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                        printed += gt.PrintIt()
                    End If
                    If itcat.m_row.Item("Law Tag Style") = 0 Then
                        Dim gt As New clsGenerateTagFRstyle(sh.m_row.Item("No_"), lt.m_row.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
                        printed += gt.PrintIt()
                    End If
                End If
            End If
            'Else
            'Dim gt As New clsGenerateTag(sh.m_row.Item("No_"), lt.m_row.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
            'printed += gt.PrintIt()
            'End If
        Next

        'Dim gt As New clsGenerateTag(r.Item("No_"), lr.Item("Line No_"), CStr(x), pd, result, Me.CheckBox1.Checked)
        'printed += gt.PrintIt()
        If Module1.pdlt Is Nothing Then
            MsgBox(printed & " Items printed")
        End If

        'If Module1.pdlt Is Nothing Then
        'MsgBox("No orders found for this date range")
        'End If

        Me.Label2.Text = ""
        Module1.ticketcounter = 0

        'If Module1.pdlt Is Nothing Then
        'Me.btnSearch.PerformClick()
        'End If
        Me.txtLawTag.Text = ""

    End Sub

    Private Sub Button12_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button12.Click
        'The Print all button

        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal

        Module1.ticketcounter = 0


        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text
        Me.Label2.Text = "Working..."



        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "'")
        If Not o.IsNew Then
            Dim printed As Integer = 0
            Dim pd As PrintDialog
            Dim result As DialogResult
            If Module1.pdlt Is Nothing Then
                pd = New PrintDialog
                pd.UseEXDialog = True
                pd.Document = New System.Drawing.Printing.PrintDocument


                result = pd.ShowDialog()
            Else
                pd = Module1.pdlt
                result = Module1.dresult1
            End If

            Dim arl As New ArrayList

            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim ds3 As New DataSet
            Dim dt As New DataTable("FRComp")
            dt.Columns.Add("Model", GetType(String))
            dt.Columns.Add("Document No_", GetType(String))
            dt.Columns.Add("Prototype No_", GetType(String))
            dt.Columns.Add("Description", GetType(String))
            dt.Columns.Add("Test", GetType(String))
            dt.Columns.Add("Test Date", GetType(Date))
            dt.Columns.Add("TF Name", GetType(String))
            dt.Columns.Add("TF Address", GetType(String))
            dt.Columns.Add("TF Address2", GetType(String))
            dt.Columns.Add("TF City", GetType(String))
            dt.Columns.Add("TF State", GetType(String))
            dt.Columns.Add("TF Zip", GetType(String))
            dt.Columns.Add("Man Name", GetType(String))
            dt.Columns.Add("Man Address", GetType(String))
            dt.Columns.Add("Man City", GetType(String))
            dt.Columns.Add("Man State", GetType(String))
            dt.Columns.Add("Man Zip", GetType(String))
            dt.Columns.Add("Man Phone", GetType(String))
            dt.Columns.Add("Man Country", GetType(String))
            dt.Columns.Add("Man Site", GetType(String))
            dt.Columns.Add("RK Name", GetType(String))
            dt.Columns.Add("RK Address", GetType(String))
            dt.Columns.Add("RK City", GetType(String))
            dt.Columns.Add("RK State", GetType(String))
            dt.Columns.Add("RK Zip", GetType(String))
            dt.Columns.Add("RK Country", GetType(String))
            dt.Columns.Add("RK Phone", GetType(String))
            dt.Columns.Add("RK Email", GetType(String))
            dt.Columns.Add("Man Date", GetType(Date))
            ds.Tables.Add(dt)
            'ds.WriteXmlSchema("FrComp.xml") '04.25.07 therse lines are added during changes to the XML structure
            'MsgBox("Done!")
            'Return
            Dim nocert As Boolean = False
            Dim testint As New Integer
            Dim newrec As New Boolean
            newrec = True
            For Each r As DataRow In o.m_dt.Rows
                Dim l As New ClsSLDistinct(r.Item("No_"))
                If l.IsNew Then
                    Dim sr As DataRow = dt.NewRow
                    sr.Item("Document No_") = r.Item("No_")
                    Dim SH As New clsSalesHeader("[No_] = '" & r.Item("No_") & "'")
                    If Not SH.IsNew Then
                        sr.Item("Man Date") = SH.m_row.Item("Production Date")
                    End If
                    Dim frcomp As New clsCompanyInfo
                    sr.Item("Man Name") = frcomp.m_row.Item("Manufacturer Name")
                    sr.Item("Man Address") = frcomp.m_row.Item("Manufacturer Address")
                    sr.Item("Man City") = frcomp.m_row.Item("Manufacturer City")
                    sr.Item("Man State") = frcomp.m_row.Item("Manufacturer State")
                    sr.Item("Man Zip") = frcomp.m_row.Item("Manufacturer Zip")
                    sr.Item("Man Phone") = frcomp.m_row.Item("Manufacturer Phone")
                    sr.Item("Man Country") = frcomp.m_row.Item("Manufacturer Country")
                    sr.Item("Man Site") = frcomp.m_row.Item("Manufacturer Site")
                    sr.Item("RK Name") = frcomp.m_row.Item("Record Keeper Name")
                    sr.Item("RK Address") = frcomp.m_row.Item("Record Keeper Address")
                    sr.Item("RK City") = frcomp.m_row.Item("Record Keeper City")
                    sr.Item("RK State") = frcomp.m_row.Item("Record Keeper State")
                    sr.Item("RK Zip") = frcomp.m_row.Item("Record Keeper Zip")
                    sr.Item("RK Country") = frcomp.m_row.Item("Record Keeper Country")
                    sr.Item("RK Phone") = frcomp.m_row.Item("Record Keeper Phone")
                    sr.Item("RK Email") = frcomp.m_row.Item("Record Keeper Email")
                    dt.Rows.Add(sr)
                    nocert = True
                Else
                    For Each lr As DataRow In l.m_dt.Rows
                        Dim frp As New clsPrototype(lr.Item("Model"))
                        If Not frp.IsNew Then
                            For Each frpr As DataRow In frp.m_dt.Rows
                                newrec = True
                                For Each d As DataRow In dt.Rows
                                    If d.Item("Test") = "16 CFR 1632" Then
                                        testint = 1
                                    End If
                                    If d.Item("Test") = "16 CFR 1633" Then
                                        testint = 2
                                    End If

                                    If (frpr.Item("Prototype No_") = d.Item("Prototype No_")) And (frpr.Item("Test") = testint) And (frpr.Item("Model") = d.Item("Model")) Then
                                        If frpr.Item("Test Date") > d.Item("Test Date") Then
                                            d.Item("Test Date") = Format(frpr.Item("Test Date"), "Short Date")
                                            Dim tf As New clsTestFacility("[Code] = '" & frpr.Item("Test Facility") & "'")
                                            If Not tf.IsNew Then
                                                d.Item("TF Name") = tf.m_row.Item("Name")
                                                d.Item("TF Address") = tf.m_row.Item("Address")
                                                d.Item("TF Address2") = tf.m_row.Item("Address2")
                                                d.Item("TF City") = tf.m_row.Item("City")
                                                d.Item("TF State") = tf.m_row.Item("State")
                                                d.Item("TF Zip") = tf.m_row.Item("Postal Code")
                                            End If
                                            Dim arch As New ClsFRCompArch("[Document No_] ='" & d.Item("Document No_") & "' and [Model]='" & d.Item("Model") & "' and [Prototype No_]='" & d.Item("Prototype No_") & "' and [Test] =" & testint)
                                            If Not arch.IsNew Then
                                                arch.m_cmd.QuotePrefix = "["
                                                arch.m_cmd.QuoteSuffix = "]"
                                                arch.m_row.Item("Test Date") = d.Item("Test Date")
                                                arch.m_row.Item("TF Name") = d.Item("TF Name")
                                                arch.m_row.Item("TF Address") = d.Item("TF Address")
                                                arch.m_row.Item("TF Address2") = d.Item("TF Address2")
                                                arch.m_row.Item("TF City") = d.Item("TF City")
                                                arch.m_row.Item("TF State") = d.Item("TF State")
                                                arch.m_row.Item("TF Zip") = d.Item("TF Zip")
                                                arch.update()
                                            End If
                                        End If
                                        newrec = False
                                    End If
                                Next
                                If newrec = True Then
                                    Dim sr As DataRow = dt.NewRow
                                    sr.Item("Model") = frpr.Item("Model")
                                    sr.Item("Document No_") = r.Item("No_")
                                    sr.Item("Prototype No_") = frpr.Item("Prototype No_")
                                    Dim model As New clsSalesLine("[Document No_] = '" & r.Item("No_") & "' AND [Model] = '" & frpr.Item("Model") & "'")
                                    If Not model.IsNew Then
                                        Try
                                            sr.Item("Description") = Microsoft.VisualBasic.Left(model.m_row.Item("Description"), (InStr(model.m_row.Item("Description"), "/")) - 3)
                                        Catch
                                            sr.Item("Description") = model.m_row.Item("Description")
                                        End Try
                                    End If
                                    If frpr.Item("Test") = 1 Then
                                        sr.Item("Test") = "16 CFR 1632"
                                    End If
                                    If frpr.Item("Test") = 2 Then
                                        sr.Item("Test") = "16 CFR 1633"
                                    End If
                                    sr.Item("Test Date") = Format(frpr.Item("Test Date"), "Short Date")
                                    Dim SH As New clsSalesHeader("[No_] = '" & lr.Item("Document No_") & "'")
                                    If Not SH.IsNew Then
                                        sr.Item("Man Date") = SH.m_row.Item("Production Date")
                                    End If
                                    'sr.Item("Man Date") = lr.Item("Production Date")
                                    Dim tf As New clsTestFacility("[Code] = '" & frpr.Item("Test Facility") & "'")
                                    If Not tf.IsNew Then
                                        sr.Item("TF Name") = tf.m_row.Item("Name")
                                        sr.Item("TF Address") = tf.m_row.Item("Address")
                                        sr.Item("TF Address2") = tf.m_row.Item("Address2")
                                        sr.Item("TF City") = tf.m_row.Item("City")
                                        sr.Item("TF State") = tf.m_row.Item("State")
                                        sr.Item("TF Zip") = tf.m_row.Item("Postal Code")
                                    End If
                                    Dim frcomp As New clsCompanyInfo
                                    sr.Item("Man Name") = frcomp.m_row.Item("Manufacturer Name")
                                    sr.Item("Man Address") = frcomp.m_row.Item("Manufacturer Address")
                                    sr.Item("Man City") = frcomp.m_row.Item("Manufacturer City")
                                    sr.Item("Man State") = frcomp.m_row.Item("Manufacturer State")
                                    sr.Item("Man Zip") = frcomp.m_row.Item("Manufacturer Zip")
                                    sr.Item("Man Phone") = frcomp.m_row.Item("Manufacturer Phone")
                                    sr.Item("Man Country") = frcomp.m_row.Item("Manufacturer Country")
                                    sr.Item("Man Site") = frcomp.m_row.Item("Manufacturer Site")
                                    sr.Item("RK Name") = frcomp.m_row.Item("Record Keeper Name")
                                    sr.Item("RK Address") = frcomp.m_row.Item("Record Keeper Address")
                                    sr.Item("RK City") = frcomp.m_row.Item("Record Keeper City")
                                    sr.Item("RK State") = frcomp.m_row.Item("Record Keeper State")
                                    sr.Item("RK Zip") = frcomp.m_row.Item("Record Keeper Zip")
                                    sr.Item("RK Country") = frcomp.m_row.Item("Record Keeper Country")
                                    sr.Item("RK Phone") = frcomp.m_row.Item("Record Keeper Phone")
                                    sr.Item("RK Email") = frcomp.m_row.Item("Record Keeper Email")
                                    Dim arch As New ClsFRCompArch("[Document No_] ='" & r.Item("No_") & "' and [Model]='" & frpr.Item("Model") & "' and [Prototype No_]='" & frpr.Item("Prototype No_") & "' and [Test] =" & frpr.Item("Test"))
                                    If arch.IsNew Then
                                        arch.m_cmd.QuotePrefix = "["
                                        arch.m_cmd.QuoteSuffix = "]"
                                        arch.m_row.Item("Model") = sr.Item("Model")
                                        arch.m_row.Item("Document No_") = sr.Item("Document No_")
                                        arch.m_row.Item("Prototype No_") = sr.Item("Prototype No_")
                                        arch.m_row.Item("Description") = sr.Item("Description")
                                        arch.m_row.Item("Test") = frpr.Item("Test")
                                        arch.m_row.Item("Test Date") = sr.Item("Test Date")
                                        arch.m_row.Item("Man Date") = sr.Item("Man Date")
                                        arch.m_row.Item("TF Name") = sr.Item("TF Name")
                                        arch.m_row.Item("TF Address") = sr.Item("TF Address")
                                        arch.m_row.Item("TF Address2") = sr.Item("TF Address2")
                                        arch.m_row.Item("TF City") = sr.Item("TF City")
                                        arch.m_row.Item("TF State") = sr.Item("TF State")
                                        arch.m_row.Item("TF Zip") = sr.Item("TF Zip")
                                        arch.m_row.Item("Man Name") = sr.Item("Man Name")
                                        arch.m_row.Item("Man Address") = sr.Item("Man Address")
                                        arch.m_row.Item("Man City") = sr.Item("Man City")
                                        arch.m_row.Item("Man State") = sr.Item("Man State")
                                        arch.m_row.Item("Man Zip") = sr.Item("Man Zip")
                                        arch.m_row.Item("Man Phone") = sr.Item("Man Phone")
                                        arch.m_row.Item("Man Country") = sr.Item("Man Country")
                                        arch.m_row.Item("Man Site") = sr.Item("Man Site")
                                        arch.m_row.Item("RK Name") = sr.Item("RK Name")
                                        arch.m_row.Item("RK Address") = sr.Item("RK Address")
                                        arch.m_row.Item("RK City") = sr.Item("RK City")
                                        arch.m_row.Item("RK State") = sr.Item("RK State")
                                        arch.m_row.Item("RK Zip") = sr.Item("RK Zip")
                                        arch.m_row.Item("RK Country") = sr.Item("RK Country")
                                        arch.m_row.Item("RK Phone") = sr.Item("RK Phone")
                                        arch.m_row.Item("RK Email") = sr.Item("RK Email")
                                        arch.update()
                                    End If
                                    dt.Rows.Add(sr)
                                End If
                            Next
                        End If
                    Next
                End If
                '                ds2 = ds
                '               ds3 = ds
                Dim targetform As New frrmCRVProdAnDealer
                '    Dim xz As New crptSalespersonDealer

                'targetform.crpt = New crptSalespersonDealer
                targetform.crpt = New CrptFRComp
                'targetform.filename = "FRTrack.pdf"
                Dim cr As New CRH(targetform.crpt)

                cr.SetText("txtName", r.Item("Ship-to Name"))
                If nocert = True Then
                    cr.SetText("txtNoCert", "This order does not require a certificate!")
                End If
                nocert = False 'cr.SetText("txtDesc1", "Prototype # " & Me.TxtProto.Text)
                'cr.SetText("txtDesc2", "Date: " & Me.TxtProtoDate.Text)

                targetform.ds = ds
                'clsStraightToPrinter.Print(targetform.crpt, ds, ds2, ds3, pd, result)
                targetform.crpt.SetDataSource(ds)


                Dim Printer As String = pd.Document.PrinterSettings.PrinterName
                Dim Copies As String = pd.Document.PrinterSettings.Copies

                Dim FromPage As Integer = pd.Document.PrinterSettings.FromPage
                Dim ToPage As Integer = pd.Document.PrinterSettings.ToPage




                If (result = DialogResult.OK) Then
                    targetform.crpt.PrintOptions.PrinterName = Printer
                    'crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Manual
                    'The case statement below should not make any difference, but I left it in case we need it sometime
                    Select Case Module1.lawtagtray
                        Case "Manual"
                            ' MsgBox("Manual")
                            targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Manual
                        Case "LargeCapacity"
                            'MsgBox("LargeCapacity")
                            targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.LargeCapacity
                        Case "Lower"
                            'MsgBox("Lower")
                            targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Lower
                        Case "Middle"
                            'MsgBox("Middle")
                            targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Middle
                        Case "Upper"
                            'MsgBox("Upper")
                            targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Upper
                        Case "Auto"
                            'MsgBox("Auto")
                            targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Auto
                    End Select

                    If Module1.testmode = 0 Then
                        targetform.crpt.PrintToPrinter(Copies, False, FromPage, ToPage)

                    End If
                End If
                ds.Clear()
                targetform.Close()
                targetform.Dispose()
                GC.Collect()

            Next
        Else
            If Module1.pdlt Is Nothing Then
                MsgBox("No orders found for this date range")
            End If
        End If
        Me.Label2.Text = ""

    End Sub

    Private Sub Button16_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button16.Click
        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal
        Module1.ticketcounter = 0

        'Module1.PrintSet.WriteXmlSchema("PrintSet.xml")
        'Return

        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text
        Me.Label2.Text = "Working..."

        Dim ds As New DataSet
        Dim ds2 As New DataSet
        Dim ds3 As New DataSet
        Dim dt As New DataTable("FRComp")
        dt.Columns.Add("Model", GetType(String))
        dt.Columns.Add("Document No_", GetType(String))
        dt.Columns.Add("Prototype No_", GetType(String))
        dt.Columns.Add("Description", GetType(String))
        dt.Columns.Add("Test", GetType(String))
        dt.Columns.Add("Test Date", GetType(Date))
        dt.Columns.Add("TF Name", GetType(String))
        dt.Columns.Add("TF Address", GetType(String))
        dt.Columns.Add("TF Address2", GetType(String))
        dt.Columns.Add("TF City", GetType(String))
        dt.Columns.Add("TF State", GetType(String))
        dt.Columns.Add("TF Zip", GetType(String))
        dt.Columns.Add("Man Name", GetType(String))
        dt.Columns.Add("Man Address", GetType(String))
        dt.Columns.Add("Man City", GetType(String))
        dt.Columns.Add("Man State", GetType(String))
        dt.Columns.Add("Man Zip", GetType(String))
        dt.Columns.Add("Man Phone", GetType(String))
        dt.Columns.Add("Man Country", GetType(String))
        dt.Columns.Add("Man Site", GetType(String))
        dt.Columns.Add("RK Name", GetType(String))
        dt.Columns.Add("RK Address", GetType(String))
        dt.Columns.Add("RK City", GetType(String))
        dt.Columns.Add("RK State", GetType(String))
        dt.Columns.Add("RK Zip", GetType(String))
        dt.Columns.Add("RK Country", GetType(String))
        dt.Columns.Add("RK Phone", GetType(String))
        dt.Columns.Add("RK Email", GetType(String))
        dt.Columns.Add("Man Date", GetType(Date))
        ds.Tables.Add(dt)
        'ds.WriteXmlSchema("FrComp.xml") '04.25.07 therse lines are added during changes to the XML structure
        'MsgBox("Done!")
        'Return
        Dim testint As New Integer
        Dim newrec As New Boolean
        newrec = True
        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "'")
        'MsgBox("LT " & o.m_dt.Rows.Count)
        Dim printed As Integer = 0
        Dim pd As PrintDialog
        Dim result As DialogResult
        If Module1.pdlt Is Nothing Then
            pd = New PrintDialog
            pd.UseEXDialog = True
            pd.Document = New System.Drawing.Printing.PrintDocument
            pd.Document.DocumentName = "Freedom - Laser Law Label"

            result = pd.ShowDialog()
        Else
            pd = Module1.pdlt
            result = Module1.dresult1
        End If
        Dim nocert As Boolean = False
        Dim arl As New ArrayList
        For x As Integer = 0 To (o.m_dt.Rows.Count - 1)
            Try
                If Me.DataGrid1.IsSelected(x) Then
                    Dim s As String = CStr(Me.DataGrid1.Item(x, 0))
                    's represents the order number
                    Dim l As New ClsSLDistinct(s)
                    If l.IsNew Then
                        Dim sr As DataRow = dt.NewRow
                        sr.Item("Document No_") = s
                        Dim SH As New clsSalesHeader("[No_] = '" & s & "'")
                        If Not SH.IsNew Then
                            sr.Item("Man Date") = SH.m_row.Item("Production Date")
                        End If
                        Dim frcomp As New clsCompanyInfo
                        sr.Item("Man Name") = frcomp.m_row.Item("Manufacturer Name")
                        sr.Item("Man Address") = frcomp.m_row.Item("Manufacturer Address")
                        sr.Item("Man City") = frcomp.m_row.Item("Manufacturer City")
                        sr.Item("Man State") = frcomp.m_row.Item("Manufacturer State")
                        sr.Item("Man Zip") = frcomp.m_row.Item("Manufacturer Zip")
                        sr.Item("Man Phone") = frcomp.m_row.Item("Manufacturer Phone")
                        sr.Item("Man Country") = frcomp.m_row.Item("Manufacturer Country")
                        sr.Item("Man Site") = frcomp.m_row.Item("Manufacturer Site")
                        sr.Item("RK Name") = frcomp.m_row.Item("Record Keeper Name")
                        sr.Item("RK Address") = frcomp.m_row.Item("Record Keeper Address")
                        sr.Item("RK City") = frcomp.m_row.Item("Record Keeper City")
                        sr.Item("RK State") = frcomp.m_row.Item("Record Keeper State")
                        sr.Item("RK Zip") = frcomp.m_row.Item("Record Keeper Zip")
                        sr.Item("RK Country") = frcomp.m_row.Item("Record Keeper Country")
                        sr.Item("RK Phone") = frcomp.m_row.Item("Record Keeper Phone")
                        sr.Item("RK Email") = frcomp.m_row.Item("Record Keeper Email")
                        dt.Rows.Add(sr)
                        nocert = True
                    Else
                        For Each lr As DataRow In l.m_dt.Rows
                            m = lr.Item("Model")
                            Dim frp As New clsPrototype(lr.Item("Model"))
                            If Not frp.IsNew Then
                                For Each frpr As DataRow In frp.m_dt.Rows
                                    newrec = True
                                    For Each d As DataRow In dt.Rows
                                        If d.Item("Test") = "16 CFR 1632" Then
                                            testint = 1
                                        End If
                                        If d.Item("Test") = "16 CFR 1633" Then
                                            testint = 2
                                        End If

                                        If (frpr.Item("Prototype No_") = d.Item("Prototype No_")) And (frpr.Item("Test") = testint) And (frpr.Item("Model") = d.Item("Model")) Then
                                            If frpr.Item("Test Date") > d.Item("Test Date") Then
                                                d.Item("Test Date") = Format(frpr.Item("Test Date"), "Short Date")
                                                Dim tf As New clsTestFacility("[Code] = '" & frpr.Item("Test Facility") & "'")
                                                If Not tf.IsNew Then
                                                    d.Item("TF Name") = tf.m_row.Item("Name")
                                                    d.Item("TF Address") = tf.m_row.Item("Address")
                                                    d.Item("TF Address2") = tf.m_row.Item("Address2")
                                                    d.Item("TF City") = tf.m_row.Item("City")
                                                    d.Item("TF State") = tf.m_row.Item("State")
                                                    d.Item("TF Zip") = tf.m_row.Item("Postal Code")
                                                End If
                                                Dim arch As New ClsFRCompArch("[Document No_] ='" & d.Item("Document No_") & "' and [Model]='" & d.Item("Model") & "' and [Prototype No_]='" & d.Item("Prototype No_") & "' and [Test] =" & testint)
                                                If Not arch.IsNew Then
                                                    arch.m_cmd.QuotePrefix = "["
                                                    arch.m_cmd.QuoteSuffix = "]"
                                                    arch.m_row.Item("Test Date") = d.Item("Test Date")
                                                    arch.m_row.Item("TF Name") = d.Item("TF Name")
                                                    arch.m_row.Item("TF Address") = d.Item("TF Address")
                                                    arch.m_row.Item("TF Address2") = d.Item("TF Address2")
                                                    arch.m_row.Item("TF City") = d.Item("TF City")
                                                    arch.m_row.Item("TF State") = d.Item("TF State")
                                                    arch.m_row.Item("TF Zip") = d.Item("TF Zip")
                                                    arch.update()
                                                End If
                                            End If
                                            newrec = False
                                        End If
                                    Next
                                    If newrec = True Then
                                        Dim sr As DataRow = dt.NewRow
                                        sr.Item("Model") = frpr.Item("Model")
                                        sr.Item("Document No_") = s
                                        sr.Item("Prototype No_") = frpr.Item("Prototype No_")
                                        Dim model As New clsSalesLine("[Document No_] = '" & s & "' AND [Model] = '" & frpr.Item("Model") & "'")
                                        If Not model.IsNew Then
                                            Try
                                                sr.Item("Description") = Microsoft.VisualBasic.Left(model.m_row.Item("Description"), (InStr(model.m_row.Item("Description"), "/")) - 3)
                                            Catch
                                                sr.Item("Description") = model.m_row.Item("Description")
                                            End Try
                                        End If
                                        If frpr.Item("Test") = 1 Then
                                            sr.Item("Test") = "16 CFR 1632"
                                        End If
                                        If frpr.Item("Test") = 2 Then
                                            sr.Item("Test") = "16 CFR 1633"
                                        End If
                                        sr.Item("Test Date") = Format(frpr.Item("Test Date"), "Short Date")
                                        Dim SH As New clsSalesHeader("[No_] = '" & lr.Item("Document No_") & "'")
                                        If Not SH.IsNew Then
                                            sr.Item("Man Date") = SH.m_row.Item("Production Date")
                                        End If
                                        Dim tf As New clsTestFacility("[Code] = '" & frpr.Item("Test Facility") & "'")
                                        If Not tf.IsNew Then
                                            sr.Item("TF Name") = tf.m_row.Item("Name")
                                            sr.Item("TF Address") = tf.m_row.Item("Address")
                                            sr.Item("TF Address2") = tf.m_row.Item("Address2")
                                            sr.Item("TF City") = tf.m_row.Item("City")
                                            sr.Item("TF State") = tf.m_row.Item("State")
                                            sr.Item("TF Zip") = tf.m_row.Item("Postal Code")
                                        End If
                                        Dim frcomp As New clsCompanyInfo
                                        sr.Item("Man Name") = frcomp.m_row.Item("Manufacturer Name")
                                        sr.Item("Man Address") = frcomp.m_row.Item("Manufacturer Address")
                                        sr.Item("Man City") = frcomp.m_row.Item("Manufacturer City")
                                        sr.Item("Man State") = frcomp.m_row.Item("Manufacturer State")
                                        sr.Item("Man Zip") = frcomp.m_row.Item("Manufacturer Zip")
                                        sr.Item("Man Phone") = frcomp.m_row.Item("Manufacturer Phone")
                                        sr.Item("Man Country") = frcomp.m_row.Item("Manufacturer Country")
                                        sr.Item("Man Site") = frcomp.m_row.Item("Manufacturer Site")
                                        sr.Item("RK Name") = frcomp.m_row.Item("Record Keeper Name")
                                        sr.Item("RK Address") = frcomp.m_row.Item("Record Keeper Address")
                                        sr.Item("RK City") = frcomp.m_row.Item("Record Keeper City")
                                        sr.Item("RK State") = frcomp.m_row.Item("Record Keeper State")
                                        sr.Item("RK Zip") = frcomp.m_row.Item("Record Keeper Zip")
                                        sr.Item("RK Country") = frcomp.m_row.Item("Record Keeper Country")
                                        sr.Item("RK Phone") = frcomp.m_row.Item("Record Keeper Phone")
                                        sr.Item("RK Email") = frcomp.m_row.Item("Record Keeper Email")
                                        Dim arch As New ClsFRCompArch("[Document No_] ='" & s & "' and [Model]='" & frpr.Item("Model") & "' and [Prototype No_]='" & frpr.Item("Prototype No_") & "' and [Test] =" & frpr.Item("Test"))
                                        If arch.IsNew Then
                                            arch.m_cmd.QuotePrefix = "["
                                            arch.m_cmd.QuoteSuffix = "]"
                                            arch.m_row.Item("Model") = sr.Item("Model")
                                            arch.m_row.Item("Document No_") = sr.Item("Document No_")
                                            arch.m_row.Item("Prototype No_") = sr.Item("Prototype No_")
                                            arch.m_row.Item("Description") = sr.Item("Description")
                                            arch.m_row.Item("Test") = frpr.Item("Test")
                                            arch.m_row.Item("Test Date") = sr.Item("Test Date")
                                            arch.m_row.Item("Man Date") = sr.Item("Man Date")
                                            arch.m_row.Item("TF Name") = sr.Item("TF Name")
                                            arch.m_row.Item("TF Address") = sr.Item("TF Address")
                                            arch.m_row.Item("TF Address2") = sr.Item("TF Address2")
                                            arch.m_row.Item("TF City") = sr.Item("TF City")
                                            arch.m_row.Item("TF State") = sr.Item("TF State")
                                            arch.m_row.Item("TF Zip") = sr.Item("TF Zip")
                                            arch.m_row.Item("Man Name") = sr.Item("Man Name")
                                            arch.m_row.Item("Man Address") = sr.Item("Man Address")
                                            arch.m_row.Item("Man City") = sr.Item("Man City")
                                            arch.m_row.Item("Man State") = sr.Item("Man State")
                                            arch.m_row.Item("Man Zip") = sr.Item("Man Zip")
                                            arch.m_row.Item("Man Phone") = sr.Item("Man Phone")
                                            arch.m_row.Item("Man Country") = sr.Item("Man Country")
                                            arch.m_row.Item("Man Site") = sr.Item("Man Site")
                                            arch.m_row.Item("RK Name") = sr.Item("RK Name")
                                            arch.m_row.Item("RK Address") = sr.Item("RK Address")
                                            arch.m_row.Item("RK City") = sr.Item("RK City")
                                            arch.m_row.Item("RK State") = sr.Item("RK State")
                                            arch.m_row.Item("RK Zip") = sr.Item("RK Zip")
                                            arch.m_row.Item("RK Country") = sr.Item("RK Country")
                                            arch.m_row.Item("RK Phone") = sr.Item("RK Phone")
                                            arch.m_row.Item("RK Email") = sr.Item("RK Email")
                                            arch.update()
                                        End If
                                        dt.Rows.Add(sr)
                                    End If
                                Next
                            End If
                        Next
                    End If
                    '                ds2 = ds
                    '               ds3 = ds
                    Dim targetform As New frrmCRVProdAnDealer
                    '    Dim xz As New crptSalespersonDealer

                    'targetform.crpt = New crptSalespersonDealer
                    targetform.crpt = New CrptFRComp
                    'targetform.filename = "FRTrack.pdf"
                    Dim cr As New CRH(targetform.crpt)
                    Dim cust As String = CStr(Me.DataGrid1.Item(x, 1))

                    cr.SetText("txtName", cust)
                    If nocert = True Then
                        cr.SetText("txtNoCert", "This order does not require a certificate!")
                    End If
                    nocert = False
                    'cr.SetText("txtDesc2", "Date: " & Me.TxtProtoDate.Text)

                    targetform.ds = ds
                    'clsStraightToPrinter.Print(targetform.crpt, ds, ds2, ds3, pd, result)
                    targetform.crpt.SetDataSource(ds)


                    Dim Printer As String = pd.Document.PrinterSettings.PrinterName
                    Dim Copies As String = pd.Document.PrinterSettings.Copies

                    Dim FromPage As Integer = pd.Document.PrinterSettings.FromPage
                    Dim ToPage As Integer = pd.Document.PrinterSettings.ToPage




                    If (result = DialogResult.OK) Then
                        targetform.crpt.PrintOptions.PrinterName = Printer
                        'crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Manual
                        'The case statement below should not make any difference, but I left it in case we need it sometime
                        Select Case Module1.lawtagtray
                            Case "Manual"
                                ' MsgBox("Manual")
                                targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Manual
                            Case "LargeCapacity"
                                'MsgBox("LargeCapacity")
                                targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.LargeCapacity
                            Case "Lower"
                                'MsgBox("Lower")
                                targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Lower
                            Case "Middle"
                                'MsgBox("Middle")
                                targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Middle
                            Case "Upper"
                                'MsgBox("Upper")
                                targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Upper
                            Case "Auto"
                                'MsgBox("Auto")
                                targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Auto
                        End Select

                        If Module1.testmode = 0 Then
                            targetform.crpt.PrintToPrinter(Copies, False, FromPage, ToPage)

                        End If
                    End If
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                MsgBox("Order No: " & CStr(Me.DataGrid1.Item(x, 0)) & " does not have all prototypes set up! Model - " & m)
            End Try
            ds.Clear()
        Next
        Me.Label2.Text = ""
        'If Module1.pdlt Is Nothing Then
        'MsgBox(printed & " Items printed")
        'End If

        'Dim f As New frrmCRVProdAnDealer
        'f.crpt = New crptPrintSet
        'f.ds = Module1.PrintSet
        'f.ShowDialog()



        'Module1.ticketcounter = 0
        'Module1.PrintSet.Clear()
        'Module1.pshash.Clear()
        If Module1.pdlt Is Nothing Then
            Me.btnSearch.PerformClick()
        End If
    End Sub

    Private Sub Button17_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button17.Click
        Dim f As New FrmFRCompArch
        f.ShowDialog()
    End Sub

    Private Sub Button18_Click(sender As System.Object, e As System.EventArgs) Handles Button18.Click
        'The Print all button

        Dim strStartDate As String
        Dim strEndDate As String
        Dim strNumRows As String

        Dim decTotal2 As Decimal
        Dim decTotal As Decimal

        Module1.ticketcounter = 0


        strStartDate = cboStartMonth.Text & "/" & cboStartDay.Text & "/" & cboStartYear.Text
        strEndDate = cboStopMonth.Text & "/" & cboStopDay.Text & "/" & cboStopYear.Text
        Me.Label2.Text = "Working..."



        Dim o As New clsOrderRoute("[Shipment Date] >= '" & strStartDate & "' and [Shipment Date] <= '" & strEndDate & "' and [Route Code] = '" & Me.ListBox1.SelectedValue & "'")
        If Not o.IsNew Then
            Dim printed As Integer = 0
            Dim pd As PrintDialog
            Dim result As DialogResult
            If Module1.pdlt Is Nothing Then
                pd = New PrintDialog
                pd.UseEXDialog = True
                pd.Document = New System.Drawing.Printing.PrintDocument


                result = pd.ShowDialog()
            Else
                pd = Module1.pdlt
                result = Module1.dresult1
            End If

            Dim arl As New ArrayList

            Dim ds As New DataSet
            Dim ds2 As New DataSet
            Dim ds3 As New DataSet
            Dim dt As New DataTable("FRComp")
            dt.Columns.Add("Model", GetType(String))
            dt.Columns.Add("Document No_", GetType(String))
            dt.Columns.Add("Prototype No_", GetType(String))
            dt.Columns.Add("Description", GetType(String))
            dt.Columns.Add("Test", GetType(String))
            dt.Columns.Add("Test Date", GetType(Date))
            dt.Columns.Add("TF Name", GetType(String))
            dt.Columns.Add("TF Address", GetType(String))
            dt.Columns.Add("TF Address2", GetType(String))
            dt.Columns.Add("TF City", GetType(String))
            dt.Columns.Add("TF State", GetType(String))
            dt.Columns.Add("TF Zip", GetType(String))
            dt.Columns.Add("Man Name", GetType(String))
            dt.Columns.Add("Man Address", GetType(String))
            dt.Columns.Add("Man City", GetType(String))
            dt.Columns.Add("Man State", GetType(String))
            dt.Columns.Add("Man Zip", GetType(String))
            dt.Columns.Add("Man Phone", GetType(String))
            dt.Columns.Add("Man Country", GetType(String))
            dt.Columns.Add("Man Site", GetType(String))
            dt.Columns.Add("RK Name", GetType(String))
            dt.Columns.Add("RK Address", GetType(String))
            dt.Columns.Add("RK City", GetType(String))
            dt.Columns.Add("RK State", GetType(String))
            dt.Columns.Add("RK Zip", GetType(String))
            dt.Columns.Add("RK Country", GetType(String))
            dt.Columns.Add("RK Phone", GetType(String))
            dt.Columns.Add("RK Email", GetType(String))
            dt.Columns.Add("Man Date", GetType(Date))
            ds.Tables.Add(dt)
            'ds.WriteXmlSchema("FrComp.xml") '04.25.07 therse lines are added during changes to the XML structure
            'MsgBox("Done!")
            'Return
            Dim nocert As Boolean = False
            Dim testint As New Integer
            Dim newrec As New Boolean
            newrec = True
            For Each r As DataRow In o.m_dt.Rows
                Dim l As New ClsSLDistinct(r.Item("No_"))
                If l.IsNew Then
                    Dim sr As DataRow = dt.NewRow
                    sr.Item("Document No_") = r.Item("No_")
                    Dim SH As New clsSalesHeader("[No_] = '" & r.Item("No_") & "'")
                    If Not SH.IsNew Then
                        sr.Item("Man Date") = SH.m_row.Item("Production Date")
                    End If
                    Dim frcomp As New clsCompanyInfo
                    sr.Item("Man Name") = frcomp.m_row.Item("Manufacturer Name")
                    sr.Item("Man Address") = frcomp.m_row.Item("Manufacturer Address")
                    sr.Item("Man City") = frcomp.m_row.Item("Manufacturer City")
                    sr.Item("Man State") = frcomp.m_row.Item("Manufacturer State")
                    sr.Item("Man Zip") = frcomp.m_row.Item("Manufacturer Zip")
                    sr.Item("Man Phone") = frcomp.m_row.Item("Manufacturer Phone")
                    sr.Item("Man Country") = frcomp.m_row.Item("Manufacturer Country")
                    sr.Item("Man Site") = frcomp.m_row.Item("Manufacturer Site")
                    sr.Item("RK Name") = frcomp.m_row.Item("Record Keeper Name")
                    sr.Item("RK Address") = frcomp.m_row.Item("Record Keeper Address")
                    sr.Item("RK City") = frcomp.m_row.Item("Record Keeper City")
                    sr.Item("RK State") = frcomp.m_row.Item("Record Keeper State")
                    sr.Item("RK Zip") = frcomp.m_row.Item("Record Keeper Zip")
                    sr.Item("RK Country") = frcomp.m_row.Item("Record Keeper Country")
                    sr.Item("RK Phone") = frcomp.m_row.Item("Record Keeper Phone")
                    sr.Item("RK Email") = frcomp.m_row.Item("Record Keeper Email")
                    dt.Rows.Add(sr)
                    nocert = True
                Else
                    For Each lr As DataRow In l.m_dt.Rows
                        Dim frp As New clsPrototype(lr.Item("Model"))
                        If Not frp.IsNew Then
                            For Each frpr As DataRow In frp.m_dt.Rows
                                newrec = True
                                For Each d As DataRow In dt.Rows
                                    If d.Item("Test") = "16 CFR 1632" Then
                                        testint = 1
                                    End If
                                    If d.Item("Test") = "16 CFR 1633" Then
                                        testint = 2
                                    End If

                                    If (frpr.Item("Prototype No_") = d.Item("Prototype No_")) And (frpr.Item("Test") = testint) And (frpr.Item("Model") = d.Item("Model")) Then
                                        If frpr.Item("Test Date") > d.Item("Test Date") Then
                                            d.Item("Test Date") = Format(frpr.Item("Test Date"), "Short Date")
                                            Dim tf As New clsTestFacility("[Code] = '" & frpr.Item("Test Facility") & "'")
                                            If Not tf.IsNew Then
                                                d.Item("TF Name") = tf.m_row.Item("Name")
                                                d.Item("TF Address") = tf.m_row.Item("Address")
                                                d.Item("TF Address2") = tf.m_row.Item("Address2")
                                                d.Item("TF City") = tf.m_row.Item("City")
                                                d.Item("TF State") = tf.m_row.Item("State")
                                                d.Item("TF Zip") = tf.m_row.Item("Postal Code")
                                            End If
                                            Dim arch As New ClsFRCompArch("[Document No_] ='" & d.Item("Document No_") & "' and [Model]='" & d.Item("Model") & "' and [Prototype No_]='" & d.Item("Prototype No_") & "' and [Test] =" & testint)
                                            If Not arch.IsNew Then
                                                arch.m_cmd.QuotePrefix = "["
                                                arch.m_cmd.QuoteSuffix = "]"
                                                arch.m_row.Item("Test Date") = d.Item("Test Date")
                                                arch.m_row.Item("TF Name") = d.Item("TF Name")
                                                arch.m_row.Item("TF Address") = d.Item("TF Address")
                                                arch.m_row.Item("TF Address2") = d.Item("TF Address2")
                                                arch.m_row.Item("TF City") = d.Item("TF City")
                                                arch.m_row.Item("TF State") = d.Item("TF State")
                                                arch.m_row.Item("TF Zip") = d.Item("TF Zip")
                                                arch.update()
                                            End If
                                        End If
                                        newrec = False
                                    End If
                                Next
                                If newrec = True Then
                                    Dim sr As DataRow = dt.NewRow
                                    sr.Item("Model") = frpr.Item("Model")
                                    sr.Item("Document No_") = r.Item("No_")
                                    sr.Item("Prototype No_") = frpr.Item("Prototype No_")
                                    Dim model As New clsSalesLine("[Document No_] = '" & r.Item("No_") & "' AND [Model] = '" & frpr.Item("Model") & "'")
                                    If Not model.IsNew Then
                                        Try
                                            sr.Item("Description") = Microsoft.VisualBasic.Left(model.m_row.Item("Description"), (InStr(model.m_row.Item("Description"), "/")) - 3)
                                        Catch
                                            sr.Item("Description") = model.m_row.Item("Description")
                                        End Try
                                    End If
                                    If frpr.Item("Test") = 1 Then
                                        sr.Item("Test") = "16 CFR 1632"
                                    End If
                                    If frpr.Item("Test") = 2 Then
                                        sr.Item("Test") = "16 CFR 1633"
                                    End If
                                    sr.Item("Test Date") = Format(frpr.Item("Test Date"), "Short Date")
                                    Dim SH As New clsSalesHeader("[No_] = '" & lr.Item("Document No_") & "'")
                                    If Not SH.IsNew Then
                                        sr.Item("Man Date") = SH.m_row.Item("Production Date")
                                    End If
                                    'sr.Item("Man Date") = lr.Item("Production Date")
                                    Dim tf As New clsTestFacility("[Code] = '" & frpr.Item("Test Facility") & "'")
                                    If Not tf.IsNew Then
                                        sr.Item("TF Name") = tf.m_row.Item("Name")
                                        sr.Item("TF Address") = tf.m_row.Item("Address")
                                        sr.Item("TF Address2") = tf.m_row.Item("Address2")
                                        sr.Item("TF City") = tf.m_row.Item("City")
                                        sr.Item("TF State") = tf.m_row.Item("State")
                                        sr.Item("TF Zip") = tf.m_row.Item("Postal Code")
                                    End If
                                    Dim frcomp As New clsCompanyInfo
                                    sr.Item("Man Name") = frcomp.m_row.Item("Manufacturer Name")
                                    sr.Item("Man Address") = frcomp.m_row.Item("Manufacturer Address")
                                    sr.Item("Man City") = frcomp.m_row.Item("Manufacturer City")
                                    sr.Item("Man State") = frcomp.m_row.Item("Manufacturer State")
                                    sr.Item("Man Zip") = frcomp.m_row.Item("Manufacturer Zip")
                                    sr.Item("Man Phone") = frcomp.m_row.Item("Manufacturer Phone")
                                    sr.Item("Man Country") = frcomp.m_row.Item("Manufacturer Country")
                                    sr.Item("Man Site") = frcomp.m_row.Item("Manufacturer Site")
                                    sr.Item("RK Name") = frcomp.m_row.Item("Record Keeper Name")
                                    sr.Item("RK Address") = frcomp.m_row.Item("Record Keeper Address")
                                    sr.Item("RK City") = frcomp.m_row.Item("Record Keeper City")
                                    sr.Item("RK State") = frcomp.m_row.Item("Record Keeper State")
                                    sr.Item("RK Zip") = frcomp.m_row.Item("Record Keeper Zip")
                                    sr.Item("RK Country") = frcomp.m_row.Item("Record Keeper Country")
                                    sr.Item("RK Phone") = frcomp.m_row.Item("Record Keeper Phone")
                                    sr.Item("RK Email") = frcomp.m_row.Item("Record Keeper Email")
                                    Dim arch As New ClsFRCompArch("[Document No_] ='" & r.Item("No_") & "' and [Model]='" & frpr.Item("Model") & "' and [Prototype No_]='" & frpr.Item("Prototype No_") & "' and [Test] =" & frpr.Item("Test"))
                                    If arch.IsNew Then
                                        arch.m_cmd.QuotePrefix = "["
                                        arch.m_cmd.QuoteSuffix = "]"
                                        arch.m_row.Item("Model") = sr.Item("Model")
                                        arch.m_row.Item("Document No_") = sr.Item("Document No_")
                                        arch.m_row.Item("Prototype No_") = sr.Item("Prototype No_")
                                        arch.m_row.Item("Description") = sr.Item("Description")
                                        arch.m_row.Item("Test") = frpr.Item("Test")
                                        arch.m_row.Item("Test Date") = sr.Item("Test Date")
                                        arch.m_row.Item("Man Date") = sr.Item("Man Date")
                                        arch.m_row.Item("TF Name") = sr.Item("TF Name")
                                        arch.m_row.Item("TF Address") = sr.Item("TF Address")
                                        arch.m_row.Item("TF Address2") = sr.Item("TF Address2")
                                        arch.m_row.Item("TF City") = sr.Item("TF City")
                                        arch.m_row.Item("TF State") = sr.Item("TF State")
                                        arch.m_row.Item("TF Zip") = sr.Item("TF Zip")
                                        arch.m_row.Item("Man Name") = sr.Item("Man Name")
                                        arch.m_row.Item("Man Address") = sr.Item("Man Address")
                                        arch.m_row.Item("Man City") = sr.Item("Man City")
                                        arch.m_row.Item("Man State") = sr.Item("Man State")
                                        arch.m_row.Item("Man Zip") = sr.Item("Man Zip")
                                        arch.m_row.Item("Man Phone") = sr.Item("Man Phone")
                                        arch.m_row.Item("Man Country") = sr.Item("Man Country")
                                        arch.m_row.Item("Man Site") = sr.Item("Man Site")
                                        arch.m_row.Item("RK Name") = sr.Item("RK Name")
                                        arch.m_row.Item("RK Address") = sr.Item("RK Address")
                                        arch.m_row.Item("RK City") = sr.Item("RK City")
                                        arch.m_row.Item("RK State") = sr.Item("RK State")
                                        arch.m_row.Item("RK Zip") = sr.Item("RK Zip")
                                        arch.m_row.Item("RK Country") = sr.Item("RK Country")
                                        arch.m_row.Item("RK Phone") = sr.Item("RK Phone")
                                        arch.m_row.Item("RK Email") = sr.Item("RK Email")
                                        arch.update()
                                    End If
                                    dt.Rows.Add(sr)
                                End If
                            Next
                        End If
                    Next
                End If
                '                ds2 = ds
                '               ds3 = ds
                Dim targetform As New frrmCRVProdAnDealer
                '    Dim xz As New crptSalespersonDealer

                'targetform.crpt = New crptSalespersonDealer
                targetform.crpt = New CrptFRComp
                'targetform.filename = "FRTrack.pdf"
                Dim cr As New CRH(targetform.crpt)

                cr.SetText("txtName", r.Item("Ship-to Name"))
                If nocert = True Then
                    cr.SetText("txtNoCert", "This order does not require a certificate!")
                End If
                nocert = False 'cr.SetText("txtDesc1", "Prototype # " & Me.TxtProto.Text)
                'cr.SetText("txtDesc2", "Date: " & Me.TxtProtoDate.Text)

                targetform.ds = ds
                'clsStraightToPrinter.Print(targetform.crpt, ds, ds2, ds3, pd, result)
                targetform.crpt.SetDataSource(ds)


                Dim Printer As String = pd.Document.PrinterSettings.PrinterName
                Dim Copies As String = pd.Document.PrinterSettings.Copies

                Dim FromPage As Integer = pd.Document.PrinterSettings.FromPage
                Dim ToPage As Integer = pd.Document.PrinterSettings.ToPage




                If (result = DialogResult.OK) Then
                    targetform.crpt.PrintOptions.PrinterName = Printer
                    'crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Manual
                    'The case statement below should not make any difference, but I left it in case we need it sometime
                    Select Case Module1.lawtagtray
                        Case "Manual"
                            ' MsgBox("Manual")
                            targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Manual
                        Case "LargeCapacity"
                            'MsgBox("LargeCapacity")
                            targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.LargeCapacity
                        Case "Lower"
                            'MsgBox("Lower")
                            targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Lower
                        Case "Middle"
                            'MsgBox("Middle")
                            targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Middle
                        Case "Upper"
                            'MsgBox("Upper")
                            targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Upper
                        Case "Auto"
                            'MsgBox("Auto")
                            targetform.crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Auto
                    End Select

                    If Module1.testmode = 0 Then
                        targetform.crpt.PrintToPrinter(Copies, False, FromPage, ToPage)

                    End If
                End If
                ds.Clear()
                targetform.Close()
                targetform.Dispose()
                GC.Collect()

            Next
        Else
            If Module1.pdlt Is Nothing Then
                MsgBox("No orders found for this date range")
            End If
        End If
        Me.Label2.Text = ""


    End Sub

    Private Sub Button19_Click(sender As System.Object, e As System.EventArgs) Handles Button19.Click
        Dim pd As PrintDialog
        Dim result As DialogResult
        If Module1.pdlt Is Nothing Then
            pd = New PrintDialog
            pd.UseEXDialog = True
            pd.Document = New System.Drawing.Printing.PrintDocument


            result = pd.ShowDialog()
        Else
            pd = Module1.pdlt
            result = Module1.dresult1
        End If

        For x As Integer = 1 To 10
            Dim crpt As New crptBedinaBoxtest
            'End If
            'Else
            '    crpt = New crptTicket
            'End If
            'Dim subrpt As CrystalDecisions.CrystalReports.Engine.ReportDocument = crpt.OpenSubreport("TearOffs")


            'Dim targetform As New frrmCRVProdAnDealer

            'targetform.crpt = crpt
            'Dim fp As String = crpt.FilePath
            'MsgBox(fp)
            'File.Copy(fp, "Lawtag", True)

            'Try
            'System.IO.File.Copy(fp, "LawtagBB", True)
            'Catch ex As Exception
            'MsgBox(ex.Message)
            'End Try
            'MsgBox("Post")

            'Dim crpt2 As New CrystalDecisions.CrystalReports.Engine.ReportDocument
            'crpt2.Load("LawtagBB")
            Dim Printer As String = pd.Document.PrinterSettings.PrinterName
            Dim Copies As String = pd.Document.PrinterSettings.Copies

            Dim FromPage As Integer = 1 'pd.Document.PrinterSettings.FromPage
            Dim ToPage As Integer = 1 'pd.Document.PrinterSettings.ToPage




            If (result = DialogResult.OK) Then
                crpt.PrintOptions.PrinterName = Printer
                'crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Manual
                'The case statement below should not make any difference, but I left it in case we need it sometime
                Select Case Module1.lawtagtray
                    Case "Manual"
                        ' MsgBox("Manual")
                        crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Manual
                    Case "LargeCapacity"
                        'MsgBox("LargeCapacity")
                        crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.LargeCapacity
                    Case "Lower"
                        'MsgBox("Lower")
                        crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Lower
                    Case "Middle"
                        'MsgBox("Middle")
                        crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Middle
                    Case "Upper"
                        'MsgBox("Upper")
                        crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Upper
                    Case "Auto"
                        'MsgBox("Auto")
                        crpt.PrintOptions.PaperSource = CrystalDecisions.[Shared].PaperSource.Auto
                End Select

                If Module1.testmode = 0 Then
                    crpt.PrintToPrinter(Copies, False, FromPage, ToPage)

                End If
            End If

        Next
        MsgBox("Done.")
    End Sub

    Private Sub btnSingle_Click(sender As Object, e As EventArgs) Handles btnSingle.Click

        Module1.ticketcounter = 0
        If txtSingle.Text = "" Then
            MsgBox("You must specify a tag number")
            Return
        End If

        Me.Label2.Text = "Working..."

        Dim printed As Integer = 0
        Dim pd As PrintDialog
        Dim result As DialogResult
        If Module1.pdlt Is Nothing Then
            'MsgBox("Print Dialog")
            pd = New PrintDialog
            pd.UseEXDialog = True
            pd.Document = New System.Drawing.Printing.PrintDocument
            pd.Document.DocumentName = "Freedom - Laser Law Label"

            result = pd.ShowDialog()
        Else
            'MsgBox("No Print Dialog")
            pd = Module1.pdlt
            result = Module1.dresult1
        End If

        Try
            Dim ltnum As String = txtSingle.Text
            Dim lw As New clsLawTag("[Tag Number] = '" & ltnum & "'")
            Dim gt As New clsGeneratePieceRateTickets(lw.m_row.Item("Document No_"), lw.m_row.Item("Line No_"), lw.m_row.Item("Instance").ToString(), pd, result, Me.CheckBox1.Checked)
            gt.PrintIt()
        Catch

        End Try



        txtSingle.Text = ""
    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click
        Dim pd As PrintDialog
        Dim result As DialogResult
        If Module1.pdlt Is Nothing Then
            'MsgBox("Print Dialog")
            pd = New PrintDialog
            pd.UseEXDialog = True
            pd.Document = New System.Drawing.Printing.PrintDocument
            pd.Document.DocumentName = "Freedom - Laser Law Label"

            result = pd.ShowDialog()
        Else
            'MsgBox("No Print Dialog")
            pd = Module1.pdlt
            result = Module1.dresult1
        End If
        For x As Integer = 0 To (Me.DataGrid1.VisibleRowCount - 1)
            'Try
            If Me.DataGrid1.IsSelected(x) Then
                Dim s As String = CStr(Me.DataGrid1.Item(x, 0))


                Dim SH As New clsSpringHill(s)
                SH.PrintIt(pd, result)
            End If
        Next

    End Sub
End Class
