﻿
Imports GQClass

Public Class clsSpringHill
    Public connstr As String
    Public ordernum As String
    Public mfrmordernum As String
    Public dt As DataTable
    Public dtLine As DataTable
    Public shipdate As DateTime
    Public gqhead As clsGenericQuery

    Sub New(ByVal OrderNumber As String)
        Dim colValues As System.Collections.Specialized.NameValueCollection
        colValues = System.Configuration.ConfigurationSettings.AppSettings()
        'Console.WriteLine("YOU ARE HERE")
        'F:\SpringAir\NavSummary - JCBedding - origin\clsSpringHill.vb
        Me.connstr = colValues.Get("connstr")
        Me.ordernum = OrderNumber

        Dim gq As New GQClass.clsGenericQuery("select *   FROM .[" & CompanyName() & "$Sales Header] where No_ ='" & Me.ordernum & "'", connstr)
        Me.gqhead = gq
        If Not gq.IsNew Then
            Me.dt = gq.MyDataTable
            Me.shipdate = gq.MyRow.Item("Shipment Date")
            Me.mfrmordernum = gq.MyRow.Item("External Document No_")
        Else
            Me.dt = New DataTable
        End If

    End Sub

    Public Function PrintIt(ByVal pd As PrintDialog, ByVal result As DialogResult) As Integer
        'Return 1
        Dim gq As New GQClass.clsGenericQuery("select *   FROM .[" & CompanyName() & "$Sales Line] where [Document No_] ='" & Me.ordernum & "'", connstr)
        If Not gq.IsNew Then
            Me.dtLine = gq.MyDataTable
        Else
            Me.dtLine = New DataTable
        End If
        For Each r As DataRow In dtLine.Rows
            Dim gqVar As New GQClass.clsGenericQuery("select * from .[" & CompanyName() & "$SAVariant] where VariantNumber='" & r.Item("SAVariant").ToString() & "'", connstr)

            Dim dtBuilt As New DataTable
            dtBuilt.Columns.Add("MFRMPONum", GetType(String))
            dtBuilt.Columns.Add("MFRMVariant", GetType(String))
            dtBuilt.Columns.Add("Description", GetType(String))
            dtBuilt.Columns.Add("MMFRMItemNum", GetType(String))
            dtBuilt.Columns.Add("MMFRMSize", GetType(String))
            dtBuilt.Columns.Add("MMFRMType", GetType(String))
            dtBuilt.Columns.Add("ShipDate", GetType(Date))
            dtBuilt.Columns.Add("TFName", GetType(String))
            dtBuilt.Columns.Add("TFAddress", GetType(String))
            dtBuilt.Columns.Add("TFAddress2", GetType(String))
            dtBuilt.Columns.Add("TFCity", GetType(String))
            dtBuilt.Columns.Add("TFState", GetType(String))
            dtBuilt.Columns.Add("TFZip", GetType(String))
            dtBuilt.Columns.Add("Qty", GetType(Integer))
            dtBuilt.Columns.Add("Man Address", GetType(String))
            dtBuilt.Columns.Add("Man City", GetType(String))
            dtBuilt.TableName = "SpringAir"

            ' dtBuilt.WriteXmlSchema("SpringAir.xml")
            Dim printed As Integer = 0
            Dim desc As String = r.Item("Description").ToString()

            Dim qty As Integer = Decimal.ToInt32(Decimal.Parse(r.Item("Quantity").ToString()))
            Dim dr As DataRow = dtBuilt.NewRow()
            dr.Item("MFRMPONum") = Me.mfrmordernum
            dr.Item("MFRMVariant") = r.Item("SAVariant").ToString()
            dr.Item("Description") = r.Item("Description")
            dr.Item("ShipDate") = Me.shipdate
            dr.Item("Qty") = 1 ' Decimal.ToInt32(Decimal.Parse(r.Item("Quantity").ToString()))
            dtBuilt.Rows.Add(dr)
            Dim ds As New DataSet
            ds.Tables.Add(dtBuilt)

            dr.Item("MMFRMSize") = gqVar.MyRow.Item("Size")
            dr.Item("MMFRMType") = gqVar.MyRow.Item("Profile")

            dr.Item("MMFRMItemNum") = gqVar.MyRow.Item("SAItemNumber")

            dr.Item("TFName") = gqhead.MyRow.Item("Ship-to Name")
            dr.Item("TFAddress") = gqhead.MyRow.Item("Ship-to Address")
            dr.Item("TFAddress2") = gqhead.MyRow.Item("Ship-to Address 2")
            dr.Item("TFCity") = gqhead.MyRow.Item("Ship-to City")
            dr.Item("TFState") = gqhead.MyRow.Item("Ship-to County")
            dr.Item("TFZip") = gqhead.MyRow.Item("Ship-to Post Code")

            Dim crpt As New crptSpringHill
            pd.PrinterSettings.Copies = 2
            For x As Integer = 1 To qty
                clsStraightToPrinter.Print(crpt, dtBuilt.DataSet, dtBuilt.DataSet, dtBuilt.DataSet, pd, result)
            Next
        Next
    End Function


End Class
